<script>

//FOR DATA TABLE
// $(document).ready(function() {
//     var tab = $('#table_dash').DataTable({"scrollX": true});

// } );

$(function () {
//FOR SELECT 2
  $('.select_campaign').select2({
                                    placeholder: "Pilih",
                                    allowClear: true
                                });

//FOR DATE RANGE PICKER
  $('#reservation').daterangepicker();

});

function selectCampaign() {
    var selectBox = document.getElementById("brand");
    var id_brand = selectBox.options[selectBox.selectedIndex].value;
    // console.log(id_brand);//DEBUG
    $.ajax({
            type: "POST",
            url: "<?php echo site_url('admin/dashboard_admin/get_campaign');?>",
            data: { id_brand : id_brand},
            dataType: "json",
            timeout: 6000, // in milliseconds
            success: function(result) {
                // console.log( JSON.stringify(result) );//debug

                var $campaign = $("#campaign");

                $campaign.empty();

                $.each(result, function (i, item) {
                    $('#campaign').append($('<option>', {
                        value: item.id_campaign,
                        text : item.name
                    }));
                });
            },
            error: function(request, status, err) {
                if(status == "timeout") {
                    alert('gagal');
                }
            }
        });
}

function selectPlatform() {
    var selectBox = document.getElementById("brand");
    var id_brand = selectBox.options[selectBox.selectedIndex].value;
    var selectBox = document.getElementById("campaign");
    var id_campaign = selectBox.options[selectBox.selectedIndex].value;
    // console.log(id_campaign+' '+id_brand);//DEBUG
    $.ajax({
            type: "POST",
            url: "<?php echo site_url('admin/dashboard_admin/get_platform');?>",
            data: {
                    id_brand    : id_brand,
                    id_campaign : id_campaign
                },
            dataType: "json",
            timeout: 6000, // in milliseconds
            success: function(result) {
                var id_platform = JSON.stringify(result[0].id_platform);
                $.ajax({
                        type: "POST",
                        url: "<?php echo site_url('admin/dashboard_admin/get_platform2');?>",
                        data: {
                                id_platform  : id_platform
                            },
                        dataType: "json",
                        timeout: 6000, // in milliseconds
                        success: function(platform) {
                            // console.log( JSON.stringify(platform) );//debug
                            var $platform = $("#platform");

                            $platform.empty();

                            $.each(platform, function (i, item) {
                                $('#platform').append($('<option>', {
                                    value: item.id_platform,
                                    text : item.platform
                                }));
                            });
                        },
                        error: function(request, status, err) {
                            if(status == "timeout") {
                                alert('Gagal');
                            }
                        }
                    })
            },
            error: function(request, status, err) {
                if(status == "timeout") {
                    alert('Gagal');
                }
            }
    });
}

function showDashboard(){
  var params_1 = document.getElementById("params_1");
  var params_2 = document.getElementById("params_2");

    var brand       = document.getElementById("brand");
    var id_brand    = brand.options[brand.selectedIndex].value;
    var campaign    = document.getElementById("campaign");
    var id_campaign = campaign.options[campaign.selectedIndex].value;
    var platform    = document.getElementById("platform");
    var id_platform = platform.options[platform.selectedIndex].value;
    var date        = document.getElementById("reservation").value;
    // console.log("id_brand : "+id_brand+" id_campaign : "+id_campaign+" id_platform : "+id_platform+" date : "+date);//DEBUG

    $.ajax({
        type: "POST",
        url: "<?php echo site_url('admin/dashboard_admin/open_dashboard_chart');?>",
        data:{
                id_brand        : id_brand
                ,id_campaign    : id_campaign
                ,id_platform    : id_platform
                ,date           : date
        },
        dataType: "json",
        timeout: 6000, // in milliseconds
        success: function(result) {
            var campaign    = result.ket[0].campaign;
            var platform    = result.ket[0].platform;
            var from        = result.ket[0].from;
            var to          = result.ket[0].to;
            var ket_dash_1  = result.ket[0].ket_dash_1;
            var ket_dash_2  = result.ket[0].ket_dash_2;

            var cpc = [], impr = [], period = [];
            var dashboar_dt  = result.dash;

            //ARRAY PUSD DENGAN LOOPING DI JS
                // console.log(chart_1);//DEBUG
            dashboar_dt.forEach(function (i) {
                cpc.push(i.CPC);
                impr.push(i.Impression);
                period.push(i.date);
            });
            // console.log(chart_1);//DEBUG

            document.getElementById('head_chart').innerHTML = "Chart "+campaign+" "+platform;
            document.getElementById('chart_date').innerHTML = "Placement Date : "+from+" "+to;

//FOR LINE CHART ===========================================================================================================================================>
            var ctx = document.getElementById("dashboard").getContext("2d");
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: period,
                    datasets: [
                      {
                        label: 'CPC',
                        fill: false,
                        fillColor: "#FF6384",
                        lineTension: 0.1,
                        backgroundColor: "#FF6384",
                        borderColor: "#FF6384",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "#FF6384",
                        pointBackgroundColor: "#FF6384",
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "#FF6384",
                        pointHoverBorderColor: "#FF6384",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        yAxisID: "y-axis-1",
                        data: cpc
                        // data: ['10%','20%', '21%']
                      },
                      {
                        label: 'IMPRESSION',
                        fill: false,
                        fillColor: "#36A2EB",
                        lineTension: 0.1,
                        backgroundColor: "#36A2EB",
                        borderColor: "#36A2EB",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "#36A2EB",
                        pointBackgroundColor: "#36A2EB",
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "#36A2EB",
                        pointHoverBorderColor: "#36A2EB",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        yAxisID: "y-axis-2",
                        data: impr
                        // data: ['700,000', '713,000', '698,000']
                      }
                    ]
                },
                options: {
                  scales: {
                        yAxes: [
                            {"id":"y-axis-1",display:true, position: "left"},
                            {"id":"y-axis-2",display:true, position: "right"}
                        ]
                    }
                }
            });
//END LINE CHART ===========================================================================================================================================>
            var content = '', select = [];

            for (var i = 0; i < ket_dash_1.length; i++) {
                // console.log(ket_dash_1[i]+' '+ket_dash_2[i]);
                content += "<div class='col-sm-1 col-xs-3'>"
                            +   "<div class='description-block border-right'>"
                            +   "<h5 class='description-header'>"
                            +    ket_dash_2[i]
                            +    "</h5>"
                            +   "<span class='description-text'>"
                            +    ket_dash_1[i]
                            +    "</span>"
                            +    "</div>"
                            +"</div>";


            };
            document.getElementById('ket_dash').innerHTML = content;

            $('.params_1').select2(
                                    {
                                        data: ket_dash_1
                                    }
                                );

            $('.params_2').select2(
                                    {
                                        data : ket_dash_1
                                    }
                                );

            document.getElementById('chart').style.display='block';

//FOR TABLE ================================================================================================================================================>
            document.getElementById('head_table').innerHTML = "Table "+campaign+" "+platform;

            var table = [], title = [], thead = '', tbody = '', show_table = '';
            title = result.table[0].ket;
            table = result.table[0].value;

            for (var i = 0; i < title.length; i++) {
                // console.log(title[i]);
                thead += '<th>'+title[i]+'</th>';
            };
            document.getElementById('thead').innerHTML = '<tr>'+thead+'</tr>';
            console.log(table);//DEBUG

            $(document).ready(function() {
                var t = $('#table_dash').DataTable();
                // var counter = 1;
                t.clear();
                t.rows.add( table ).draw(true);
            } );

            document.getElementById('table').style.display='block';
//END TABLE ================================================================================================================================================>
        },
        error: function(request, status, err) {
            if(status == "timeout") {
                alert('gagal');
            }
        }
    });

    // document.getElementById('table').style.display='block';
}

function line_1(){

    var params_1 = document.getElementById("params_1");
    var line_1 = params_1.options[params_1.selectedIndex].value;
    var params_2 = document.getElementById("params_2");
    var line_2 = params_2.options[params_2.selectedIndex].value;

    var brand       = document.getElementById("brand");
    var id_brand    = brand.options[brand.selectedIndex].value;
    var campaign    = document.getElementById("campaign");
    var id_campaign = campaign.options[campaign.selectedIndex].value;
    var platform    = document.getElementById("platform");
    var id_platform = platform.options[platform.selectedIndex].value;
    var date        = document.getElementById("reservation").value;
    // console.log("id_brand : "+id_brand+" id_campaign : "+id_campaign+" id_platform : "+id_platform+" date : "+date);//DEBUG

    $.ajax({
        type: "POST",
        url: "<?php echo site_url('admin/dashboard_admin/open_dashboard_chart');?>",
        data:{
                id_brand        : id_brand
                ,id_campaign    : id_campaign
                ,id_platform    : id_platform
                ,date           : date
        },
        dataType: "json",
        timeout: 6000, // in milliseconds
        success: function(result) {
            var campaign    = result.ket[0].campaign;
            var platform    = result.ket[0].platform;
            var from        = result.ket[0].from;
            var to          = result.ket[0].to;
            var ket_dash_1  = result.ket[0].ket_dash_1;
            var ket_dash_2  = result.ket[0].ket_dash_2;

            var data_ln_1 = [], data_ln_2 = [], period = [];
            var chart_1  = result.dash;

            //ARRAY PUSH DENGAN LOOPING DI JS
            chart_1.forEach(function (i) {
                data_ln_1.push(i[line_1]);
                data_ln_2.push(i[line_2]);
                period.push(i.date);
            });
            // console.log(data_ln_1);

            var ctx = document.getElementById("dashboard").getContext("2d");
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: period,
                    datasets: [
                      {
                        label: line_1,
                        fill: false,
                        fillColor: "#FF6384",
                        lineTension: 0.1,
                        backgroundColor: "#FF6384",
                        borderColor: "#FF6384",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "#FF6384",
                        pointBackgroundColor: "#FF6384",
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "#FF6384",
                        pointHoverBorderColor: "#FF6384",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        yAxisID: "y-axis-1",
                        data: data_ln_1
                      },
                      {
                        label: line_2,
                        fill: false,
                        fillColor: "#36A2EB",
                        lineTension: 0.1,
                        backgroundColor: "#36A2EB",
                        borderColor: "#36A2EB",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "#36A2EB",
                        pointBackgroundColor: "#36A2EB",
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "#36A2EB",
                        pointHoverBorderColor: "#36A2EB",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        yAxisID: "y-axis-2",
                        data: data_ln_2
                      }
                    ]
                },
                options: {

                    scales: {
                        yAxes: [
                            {
                                ticks: {
                                            beginAtZero:false
                                        }
                            },
                            {"id":"y-axis-1",display:true, position: "left"},
                            {"id":"y-axis-2",display:true, position: "right"}
                        ]
                    }
                }
            });
        },
        error: function(request, status, err) {
            if(status == "timeout") {
                alert('gagal');
            }
        }
    });

    $('#dashboard').remove(); // this is my <canvas> element
    $('#position_dash').append('<canvas id="dashboard" height="65"><canvas>');//Munculkan Kembali Chart
}

function line_2(){

    var params_1 = document.getElementById("params_1");
    var line_1 = params_1.options[params_1.selectedIndex].value;
    var params_2 = document.getElementById("params_2");
    var line_2 = params_2.options[params_2.selectedIndex].value;

    var brand       = document.getElementById("brand");
    var id_brand    = brand.options[brand.selectedIndex].value;
    var campaign    = document.getElementById("campaign");
    var id_campaign = campaign.options[campaign.selectedIndex].value;
    var platform    = document.getElementById("platform");
    var id_platform = platform.options[platform.selectedIndex].value;
    var date        = document.getElementById("reservation").value;
    // console.log("id_brand : "+id_brand+" id_campaign : "+id_campaign+" id_platform : "+id_platform+" date : "+date);//DEBUG

    $.ajax({
        type: "POST",
        url: "<?php echo site_url('admin/dashboard_admin/open_dashboard_chart');?>",
        data:{
                id_brand        : id_brand
                ,id_campaign    : id_campaign
                ,id_platform    : id_platform
                ,date           : date
        },
        dataType: "json",
        timeout: 6000, // in milliseconds
        success: function(result) {
            var campaign    = result.ket[0].campaign;
            var platform    = result.ket[0].platform;
            var from        = result.ket[0].from;
            var to          = result.ket[0].to;
            var ket_dash_1  = result.ket[0].ket_dash_1;
            var ket_dash_2  = result.ket[0].ket_dash_2;

            var data_ln_1 = [], data_ln_2 = [], period = [];
            var chart_1  = result.dash;

            //ARRAY PUSH DENGAN LOOPING DI JS
            chart_1.forEach(function (i) {
                data_ln_1.push(i[line_1]);
                data_ln_2.push(i[line_2]);
                period.push(i.date);
            });

            // console.log(data_ln_1);
            var ctx = document.getElementById("dashboard").getContext("2d");
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: period,
                    datasets: [
                      {
                        label: line_1,
                        fill: false,
                        fillColor: "#FF6384",
                        lineTension: 0.1,
                        backgroundColor: "#FF6384",
                        borderColor: "#FF6384",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "#FF6384",
                        pointBackgroundColor: "#FF6384",
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "#FF6384",
                        pointHoverBorderColor: "#FF6384",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        yAxisID: "y-axis-1",
                        data: data_ln_1
                      },
                      {
                        label: line_2,
                        fill: false,
                        fillColor: "#36A2EB",
                        lineTension: 0.1,
                        backgroundColor: "#36A2EB",
                        borderColor: "#36A2EB",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "#36A2EB",
                        pointBackgroundColor: "#36A2EB",
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "#36A2EB",
                        pointHoverBorderColor: "#36A2EB",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 10,
                        yAxisID: "y-axis-2",
                        data: data_ln_2
                      }
                    ]
                },
                options: {
                    scales: {
                        yAxes: [
                            {
                                ticks: {
                                            beginAtZero:false
                                        }
                            },
                            {"id":"y-axis-1",display:true, position: "left"},
                            {"id":"y-axis-2",display:true, position: "right"}
                        ]
                    }
                }
            });
        },
        error: function(request, status, err) {
            if(status == "timeout") {
                alert('gagal');
            }
        }
    });

    $('#dashboard').remove(); // this is my <canvas> element
    $('#position_dash').append('<canvas id="dashboard" height="65"><canvas>');
}

</script>

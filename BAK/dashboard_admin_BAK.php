<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_admin extends CI_Controller {

	function __construct() {
        parent::__construct();

        $this->load->model(array(
                                  'm_campaign'
                                  ,'m_dashboard'
                                  ,'m_upload'
                                )
                            );        
    }


    public function index()
	{

		$data = array(
						'chart'			=> 'tidak',
						'brand'			=> $this->select_brand(),
						'campaign'		=> $this->select_campaign(),
						'platform'		=> $this->select_platform(),
						'view' 			=> 'open_dashboard',
						'js'   			=> 'script_admin_dash'
					);
		// echo "<pre/>"; print_r($data); die();//DEBUG
        $this->load->view('admin/dashboard_admin', $data);
	}

	function select_brand(){
		$dashboard = $this->m_dashboard->name_brand();
		return $dashboard;
	}

	function select_campaign(){
		$dashboard = $this->m_dashboard->name_campaign();
		return $dashboard;
	}

	function select_platform(){
		$platform = $this->m_dashboard->platform();
		return $platform;
	}

	function campaign($id_campaign){
        $campaign = $this->m_dashboard->campaign($id_campaign);
        return $campaign;
    }

	function open_dashboard(){

		$id_brand    	= $_POST['id_brand'];
		$id_campaign    = $_POST['id_campaign'];
		$id_platform    = $_POST['id_platform'];
		$date         	= $_POST['date'];//05/11/2016 - 05/11/2016
		
        $date_from    = date('Y-m-d',strtotime(substr($date, 0,10)));
        $date_to      = date('Y-m-d',strtotime(substr($date, 13, 22)));

        $tanggal = $this->period($date_from, $date_to); 

		$dt_dashboard = $this->m_dashboard->data_dashboard($id_brand, $id_campaign, $id_platform, $date_from, $date_to);
		$platform_dt = call_user_func_array('array_merge', $this->m_upload->platform($id_platform));
		$campaign_nm = call_user_func_array('array_merge', $this->m_upload->name_campaign($id_campaign));
		// echo '<pre/>'; print_r($dt_dashboard); die();//DEBUG

		if ($platform_dt['platform'] == 'Programatic') {
            $table_th = array('id_tmp_dash','Date','Device','Targeting','Cost','Impression','Clicks','CTR','Conversions','CVR','CPC','CPM','CPA');
        }
        elseif ($platform_dt['platform'] == 'Facebook') {
            $table_th = array('id_tmp_dash','Date','Cost','Impression','Clicks','CTR','Engagement','Eng Rate','Page Likes','Likes Rate','Views (Video)', 'Views Rate','CPC','CPM','CPE','CPL','CPV');
        }
        elseif ($platform_dt['platform'] == 'Instagram') {
            $table_th = array('id_tmp_dash','Date','Cost','Impression','Clicks','CTR','Engagement','Eng Rate','Views (Video)','View Rate','CPC','CPM','CPE','CPV');
        }
        elseif ($platform_dt['platform'] == 'Twitter') {
            $table_th = array('id_tmp_dash','Date','Cost','Impression','Clicks','CTR','Engagement', 'Eng Rate', 'Followers', 'Follow Rate','Views (Video)', 'View Rate','CPC','CPM','CPL');
        }
        elseif ($platform_dt['platform'] == 'BBM') {
            $table_th = array('');
        }

        if (empty($dt_dashboard)) {
        	$message = array( 
                            'valid'     => 'kosong', 
                            'message'   => 'Data Tidak Ditemukan !'
                        );

            $this->session->set_flashdata('message',$message);
            
            redirect('admin/dashboard_admin'); 
        }
        else{
			$data = array(
	                        // 'chart'      	=> 'ada',
	                        'table_th' 		=> $table_th,
	                        'dt_dashboard'  => $dt_dashboard,
	                        'platform_dt'   => $platform_dt,
	                        'campaign_nm'   => $campaign_nm,
	                        'from'   		=> $tanggal['period_f'],
	                        'to'   			=> $tanggal['period_t'],
							'campaign'		=> $this->select_campaign(),
							'platform'		=> $this->select_platform(),
	                        // 'view' 			=> 'open_dashboard',
							// 'js'   			=> 'script_admin_dash'
	                    );
	        // echo '<pre/>'; print_r($data); die();//DEBUG

	        // $this->load->view('admin/dashboard_admin', $data);
	        echo json_encode($data);
        }

	}

	function open_dashboard_chart(){

		$id_brand    	= $_POST['id_brand'];
		$id_campaign    = $_POST['id_campaign'];
		$id_platform    = $_POST['id_platform'];
		$date         	= $_POST['date'];//05/11/2016 - 05/11/2016
		
        $date_from    = date('Y-m-d',strtotime(substr($date, 0,10)));
        $date_to      = date('Y-m-d',strtotime(substr($date, 13, 22)));

        $tanggal = $this->period($date_from, $date_to); 

		$dt_dashboard = $this->m_dashboard->data_dashboard($id_brand, $id_campaign, $id_platform, $date_from, $date_to);
		$platform_dt = call_user_func_array('array_merge', $this->m_upload->platform($id_platform));
		$campaign_nm = call_user_func_array('array_merge', $this->m_upload->name_campaign($id_campaign));

		echo '<pre/>'; print_r($dt_dashboard); die();//DEBUG
		

        if (empty($dt_dashboard)) {
        	$message = array( 
                            'valid'     => 'kosong', 
                            'message'   => 'Data Tidak Ditemukan !'
                        );

            $this->session->set_flashdata('message',$message);
            
            redirect('admin/dashboard_admin'); 
        }
        else{
			$data = array(
	                        // 'chart'      	=> 'ada',
	                        'table_th' 		=> $table_th,
	                        'dt_dashboard'  => $dt_dashboard,
	                        'platform_dt'   => $platform_dt,
	                        'campaign_nm'   => $campaign_nm,
	                        'from'   		=> $tanggal['period_f'],
	                        'to'   			=> $tanggal['period_t'],
							'campaign'		=> $this->select_campaign(),
							'platform'		=> $this->select_platform(),
	                        // 'view' 			=> 'open_dashboard',
							// 'js'   			=> 'script_admin_dash'
	                    );
	        // echo '<pre/>'; print_r($data); die();//DEBUG

	        // $this->load->view('admin/dashboard_admin', $data);
	        echo json_encode($data);
        }

	}

	function period($period_from, $period_to){
		$day_f   = substr($period_from, 8,2);
		$day_t   = substr($period_to, 8,2);
		$month_f = substr($period_from, 5,2);
		$month_t = substr($period_to, 5,2);
		$year_f  = substr($period_from, 0,4);
		$year_t  = substr($period_to, 0,4);

		$dateObj_f   = DateTime::createFromFormat('!m', $month_f);
		$dateObj_t   = DateTime::createFromFormat('!m', $month_t);
		$month_f = $dateObj_f->format('F'); // March
		$month_t = $dateObj_t->format('F'); // March

		$period = array(
						'period_f'	=> $day_f.' '.$month_f.', '.$year_f
						,'period_t'	=> $day_t.' '.$month_t.', '.$year_t
						);

		// echo '<pre/>'; print_r($period); die();//DEBUG
		return $period;
	}

	function get_campaign(){
        $id_brand = $_POST['id_brand'];

        $get_brand = $this->m_dashboard->get_campagin($id_brand);


        $campaign = array();

        foreach ($get_brand as $key => $get_brand) {
            array_push($campaign,  array(
                                           'id_campaign'   => $get_brand['id_campaign'],
                                           'name'          => $get_brand['name']       
                                        )
                        );     
        }
        // print_r($campaign); die();

        echo json_encode($campaign);
    }

	function get_platform(){
        $id_brand 	 = $_POST['id_brand'];
        $id_campaign = $_POST['id_campaign'];

        $id_platform = $this->m_dashboard->get_platform($id_brand, $id_campaign);


        $id = array();

        foreach ($id_platform as $key => $id_platform) {
            array_push($id,  array(
                                           'id_platform'   => $id_platform['id_platform']      
                                        )
                        );     
        }
        // print_r($id); die();//DEBUG

        echo json_encode($id);
    }

	function get_platform2(){
        $id_platform = str_replace('"', '', $_POST['id_platform']);

        $platform = $this->m_dashboard->get_platform2($id_platform);

        $name = array();

        foreach ($platform as $key => $platform) {
            array_push($name,  array(
            								'id_platform' => $id_platform
                                        	,'platform'   => $platform['platform']      
                                        )
                        );     
        }
        // print_r($name); die();//DEBUG

        echo json_encode($name);
    }


	function logout() {
        $this->session->sess_destroy();
        redirect('login');
    }
}


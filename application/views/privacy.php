<!DOCTYPE html>
<html lang="id" xml:lang="id">
  <head>
    <meta charset="utf-8">
    <title>Privacy Policy</title>
    <link rel="icon" href="http://blade.co.id/new/assets/img/Logo-Circle.png">
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,600,700" rel="stylesheet">
    <style>
      body {
        font-family: 'Poppins', sans-serif;
        width: 70%;
        height: auto;
        margin: auto;
        background-color: #222222;
        color: #fff;
      }

      h1 {
        padding-bottom: 35px;
        font-size: 16pt;
        font-style: normal;
        font-weight: 700;
        text-align: center;
        color: #fdc00f;
      }

      h2, h3 {
        font-size: 16pt;
        text-align: left;
        font-weight: 600;
      }

      p {
        text-align: justify;
        text-justify: inter-word;
        line-height: 23pt;
        font-size: 12pt;
      }

      i, strong, h2, h3 {
        color: #fdc00f;
      }

      footer {
        margin: 50px 0 0 0;
        margin: 50px  0 0 0;
      }

      footer p {
        text-align: center;
      }

      footer p a {
        padding-left: 5px;
      }

      footer span {
        font-size: 9pt;
        padding-top: 15px;
      }

      footer span a {
        text-decoration: none;
        color: #fdc00f;
      }

    </style>
  </head>
  <body>
    <h1>Privacy Policy</h1>
    <p>
      <i>Microad Trading Desk</i> atau MTD adalah Dashboard yang kami, <strong>PT. MicroAd Blade Indonesia</strong> sediakan untuk Klien kami agar mereka dapat dengan mudah melihat dan mengotrol layanan yang telah mereka sewa/beli dari kami sebagai perusahaan penyedia jasa. Tentunya banyak fasilitas yang kami tawarkan untuk Klien atau calon Klien kami seperti :
    </p>

    <ul>
      <li>Blade Programmatic</li>
      <li>Facebook Ads</li>
      <li>Google Ads</li>
    </ul>

    <p>
      Yang didalamnya terdapat banyak fasilitas untuk keperluan bisnis yang kami jalankan. Jika Anda adalah Klien kami atau Anda tertarik untuk menggunakan jasa kami sebagai salah satu perusahaan <i>Digital Agency</i> di Indonesia, berikut adalah poin yang harus anda ketahui tentang bagaimana kami menggunakan informasi data dari dari fasilitas yang telah kami sebutkan diatas sebagai keperluan dashboard internal kami yaitu :
    </p>

    <ul>
      <li>Informasi apa dan kenapa kami mengambil informasi tersebut.</li>
      <li>Bagaimana kami menggunakan informasi tersebut.</li>
      <li>Ketentuan Penggunaan dashboard MTD</li>
    </ul>

    <h2>Informasi yang Kami Gunakan</h2>
    <p>
      Kami mengumpulkan informasi untuk menyediakan layanan kepada Klien kami agar Klien kami dapat mengetahui pencapaian dari layanan yang mereka sewa/beli. Informasi yang kami kumpulkan kami dapatkan dari berbagai mesin penyedia layanan iklan seperti Facebook Ads, Google Ads, dan Blade Programmatic. Adapun informasi yang kami dapatkan akan kami gunakan sebagai acuan apakah permitaan dari Klien (KPI) sudah terpenuhi atau belum. Dari sisi Klien, Klien akan memonitor kinerja dari layanan yang mereka sewa/beli melalui <i>dashboard</i> MTD.
    </p>
    <p>
      Informasi yang kami gunakan contohnya adalah :
      <ul>
        <li><i>Impressions</i></li>
        <li><i>Cost Per Click (CPC)</i></li>
        <li><i>Engagement</i></li>
        <li><i>Page Like</i></li>
        <li><i>View Video</i></li>
        <li><i>Click</i></li>
        <li><i>Cost</i></li>
      </ul>
    </p>

    <p>
      Informasi yang kami dapatkan akan menjadi acuan bagi kami apakah target dari Klien sudah terpenuhi atau belum, sehingga kami dapat mengambil tindakan dari hasil pencapain yang kami dapatkan.
    </p>

    <h2>Penggunaan Informasi</h2>
    <p>
      Informasi yang kami dapatkan hanya akan dapat dilihat dan gunakan dalam aplikasi ini, yaitu Microad Trading Desk (MTD). Informasi seperti impression, engagement, page like, video view, akan kami gunakan sebagai acuan berjalannya sebuah iklan yang Klien kami beli/sewa.
    </p>

    <p>
      Informasi yang didapatkan dari dashboard kami bersifat tertutup, sehingga pihak yang tidak bekerjasama dengan kami tidak akan mengetahui informasi apapun didalamnya. Klien akan diberikan akses untuk masuk kedalam dashboard setelah Klien mentandatangani kontrak dengan kami untuk menyewa salah satu atau lebih produk kami.
    </p>

    <h3>Ketentuan Penggunaan MTD</h3>
    <p>
      <i>Microad Trading Desk</i> (MTD) hanya dapat digunakan oleh karyawan dari <strong>PT. MicroAd Blade Indonesia</strong> dan Klien yang telah bekerjasama sebagai media untuk mengukur jalannya layanan yang disediakan.
    </p>

    <hr style="border: 1px solid #fdc00f;">

    <footer>
      <p>
        <a href="https://www.facebook.com/microadbladeindonesia/" target="_blank"><img src="https://blade.co.id/new/assets/img/fb_full.png" alt="Facebook"></a>
        <a href="https://www.instagram.com/microadbladeid/" target="_blank"><img src="https://blade.co.id/new/assets/img/ig_full.png" alt="Instagram"></a>
        <a href="https://twitter.com/MicroAdBladeID" target="_blank"><img src="https://blade.co.id/new/assets/img/tw_col.png" alt="Twitter"></a>
        <a href="https://www.linkedin.com/company/microad-blade-indonesia" target="_blank"><img src="https://blade.co.id/new/assets/img/lk_col.png" alt="Linkedin"></a>
        <a href="https://www.youtube.com/channel/UCRaV1HjedMV5Mw_idMqrvAQ" target="_blank"><img src="https://blade.co.id/new/assets/img/yt_col.png" alt="Youtube"></a>
        <a href="https://www.google.com/partners/?hl=en-US#a_profile;idtf=7176635733;qury=microad%20blade%20indonesia" target="_blank"><img src="https://blade.co.id/new/assets/img/pt_col.png" alt="Google Partner"></a>
        <br/>
        <span><a href="https://blade.co.id" target="_blank"> &copy; PT. Microad Blade Indonesia <?php echo date('Y');?></a></span>
      </p>
    </footer>
  </body>
</html>

<script>

$(function() {
    // For Select 2 JS
    $(".select2").select2();

//START CAMPAIGN =========================================================================================================================================>
    //FOR TABLE
    var table_campaign = $("#table_campaign").DataTable({
                                                    "columnDefs": [
                                                            {
                                                                "targets": [0],
                                                                "visible": false,
                                                                "searchable": false
                                                            },
                                                            {
                                                                "targets": [1],
                                                                "visible": false,
                                                                "searchable": false
                                                            },
                                                            {
                                                                "targets": [4],
                                                                "visible": false,
                                                                "searchable": false
                                                            },
                                                            {
                                                                "targets": [6],
                                                                "visible": false,
                                                                "searchable": false
                                                            },
                                                            {
                                                                "targets": [9],
                                                                "visible": false,
                                                                "searchable": false
                                                            }
                                                        ]
                                                    });

    $('#table_campaign tbody').on( 'click', 'tr', function () {//Untuk Select table
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table_campaign.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    $('#edit_cmpgn').click( function () {//Untuk Edit
        // console.log(val);//DEBUG
        var campaign = $.map(table_campaign.rows('.selected').data(), function (item) {
            var id_campaign = item[0];
            var id_brand    = item[1];
            var brand_name  = item[2];
            var name        = item[3];
            var id_agency   = item[4];
            var agency_name = item[5];
            var id_platform = item[6];
            var platform    = item[7];
            var budget      = item[8].match(/\d/g);
            var u_id        = item[9];
            var client      = item[10];
            var campaign    = [id_campaign, id_brand, brand_name, name, id_agency, agency_name, id_platform, platform, budget, u_id, client];
            return campaign
        });

        // console.log( campaign );//debug
        $("#id_campaign").val(campaign[0]);

        $("#name").val(campaign[3]);

        $("#id_brand_old").val(campaign[1]);
        $("#id_agency_old").val(campaign[4]);
        $("#id_platform_old").val(campaign[6]);

        $("#u_id").val(campaign[9]);
        $("#budget_edt").val(campaign[8].join(""));

    });

    $('#dlt_campaign').click( function () {//Untuk Menghapus

        var id_campaign = $.map(table_campaign.rows('.selected').data(), function (item) {
            return item[0]
        });

        console.log( id_campaign );//debug

        table_campaign.row('.selected').remove().draw( false );

        // Sending Data to Controller
        $.post("<?php echo site_url('admin/campaign/delete_campaign');?>",
                    {
                       //TODO: key untuk delete
                       id_campaign : id_campaign
                    }
            );
        //with href
        // window.location.href = "<?php echo site_url('admin/agency_sales/delete_agency/"+data+"'); ?>";
    });
//END CAMPAIGN ===========================================================================================================================================>

//START BRAND ============================================================================================================================================>
    //FOR TABLE
    var table_brand = $("#table_brand").DataTable({
                                                    "columnDefs": [
                                                            {
                                                                "targets": [0],
                                                                "visible": false,
                                                                "searchable": false
                                                            }
                                                        ]
                                                    ,"info":  false
                                                    ,"bPaginate": true
                                                    });

    $('#table_brand tbody').on( 'click', 'tr', function () {//Untuk Select table
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table_brand.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    $('#brand_edit').click( function () {//Untuk Edit
        // console.log(val);//DEBUG
        var brand = $.map(table_brand.rows('.selected').data(), function (item) {
            var id_brand    = item[0];
            var brand_name  = item[1];
            var brand    = [id_brand, brand_name];
            return brand
        });

        // console.log(brand);//debug
        //FOR MODALS
        $("#id_brand").val(brand[0]);

        $("#brand_nm").val(brand[1]);
    });

    $('#brand_dlt').click( function () {//Untuk Menghapus

        var id_brand = $.map(table_brand.rows('.selected').data(), function (item) {
            return item[0]
        });

        console.log( id_brand );//debug

        // Sending Data to Controller
        $.post("<?php echo site_url('admin/campaign/delete_brand');?>",
                    {
                       //TODO: key untuk delete
                       id_brand : id_brand
                    }
            );

        table_brand.row('.selected').remove().draw( false );

        //with href
        // window.location.href = "<?php echo site_url('admin/agency_sales/delete_agency/"+data+"'); ?>";
    });
//END BRAND ==============================================================================================================================================>
});

</script>

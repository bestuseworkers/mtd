<script>
//FOR DATA TABLE
var t = $("#table_dash").DataTable({"scrollX": true});


$(function () {
//FOR SELECT 2
  $('.select_campaign').select2({
                                    placeholder: "Choose",
                                    allowClear: true
                                });

  $('.select_type').select2({
                                placeholder: "Choose",
                                // allowClear: true
                            });
  $('.select_targeting').select2({
                                    placeholder: "Choose",
                                    // allowClear: true
                                });

//FOR DATE RANGE PICKER
  $('#reservation').daterangepicker();

// Hide Publisher Date Selector
document.getElementById('publisher').style.display='none';

});

function selectCampaign() {
    $(function () {
        $(".select_campaign").select2('data', null);
        $("#platform").select2("data", null);
    });

    var selectBox   = document.getElementById("brand");
    var id_brand    = selectBox.options[selectBox.selectedIndex].value;
    var u_id        = document.getElementById("u_id").value;
    // console.log();//DEBUG
    $.ajax({
            type: "POST",
            url: "<?php echo site_url('dashboard/get_campaign');?>",
            data: {
                    id_brand : id_brand,
                    u_id     : u_id
                },
            dataType: "json",
            timeout: 6000, // in milliseconds
            success: function(result) {
                console.log( JSON.stringify(result) );//debug

                var $campaign = $("#campaign");

                $campaign.empty();

                $.each(result, function(i, value) {
                    $campaign.append("<option value=''></option>"
                                      +"<option value='"+value.id_campaign+"'>"
                                      + value.name
                                      + "</option>"
                                    );
                });

                // $.each(result, function (i, item) {
                //     $('#campaign').append($('<option>', {
                //         value: item.id_campaign,
                //         text : item.name
                //     }));
                // });
            },
            error: function(request, status, err) {
                if(status == "timeout") {
                    alert('Failed, Please Reload !');
                }
            }
        });
}

function selectPlatform() {
    $(function () {
        $(".select_platform").select2('data', null);
    });

    var selectBox   = document.getElementById("brand");
    var id_brand    = selectBox.options[selectBox.selectedIndex].value;
    var selectBox   = document.getElementById("campaign");
    var id_campaign = selectBox.options[selectBox.selectedIndex].value;
    var nm_campaign = selectBox.options[selectBox.selectedIndex].text;
    // console.log(nm_campaign+' '+id_brand);//DEBUG
    $.ajax({
            type: "POST",
            url: "<?php echo site_url('dashboard/get_platform');?>",
            data: {
                    id_brand    : id_brand,
                    id_campaign : id_campaign
                },
            dataType: "json",
            timeout: 6000, // in milliseconds
            success: function(result) {
                var id_brand = JSON.stringify(result[0].id_brand);
                var name     = JSON.stringify(result[0].name);
                $.ajax({
                        type: "POST",
                        url: "<?php echo site_url('dashboard/get_platform2');?>",
                        data: {
                                id_brand  : id_brand,
                                name      : name
                            },
                        dataType: "json",
                        timeout: 6000, // in milliseconds
                        success: function(platform) {
                            // console.log( JSON.stringify(platform) );//debug
                            var $platform = $("#platform");

                            $platform.empty();

                            $.each(platform, function(i, value) {
                                $platform.append("<option value=''></option>"
                                                  +"<option value='"+value.id_platform+"'>"
                                                  + value.platform
                                                  + "</option>"
                                                );
                            });

                            // $.each(platform, function (i, item) {
                            //     $('#platform').append($('<option>', {
                            //         value: item.id_platform,
                            //         text : item.platform
                            //     }));
                            // });
                        },
                        error: function(request, status, err) {
                            if(status == "timeout") {
                                alert('Gagal');
                            }
                        }
                    })
            },
            error: function(request, status, err) {
                if(status == "timeout") {
                    alert('Failed, Please Reload !');
                }
            }
    });
}

function selectType(){
    var $targeting = $("#targeting");
    $targeting.empty();

    var brand       = document.getElementById("brand");
    var id_brand    = brand.options[brand.selectedIndex].value;
    var campaign    = document.getElementById("campaign");
    var id_campaign = campaign.options[campaign.selectedIndex].value;
    var platform    = document.getElementById("platform");
    var id_platform = platform.options[platform.selectedIndex].value;
    // console.log(id_platform);
   if (id_platform == 1) {
        document.getElementById('type_targeting').style.display='block';
        document.getElementById('type_targeting_2').style.display='block';
    }else{
        document.getElementById('type_targeting').style.display='none';
        document.getElementById('type_targeting_2').style.display='none';
    }

    $.ajax({
            type: "POST",
            url: "<?php echo site_url('dashboard/select_type');?>",
            data: {
                    id_brand   : id_brand,
                    id_campaign: id_campaign,
                    id_platform: id_platform
                },
            dataType: "json",
            timeout: 6000, // in milliseconds
            success: function(type_val) {
                // console.log(type_val);//DEBUG
                // var targeting  = JSON.stringify(result[0].targeting);
                var $type = $("#type");

                $type.empty();

                $.each(type_val, function(i, value) {
                    console.log(value);

                    $type.append("<option value=''></option>"
                                          +"<option value='"+value.device+"'>"
                                          + value.device
                                          + "</option>"
                                        );
                });
            },
            error: function(request, status, err) {
                if(status == "timeout") {
                    alert('Failed, Please Reload !');
                }
            }
    });
}

function selectTargeting(){
    var brand       = document.getElementById("brand");
    var id_brand    = brand.options[brand.selectedIndex].value;
    var campaign    = document.getElementById("campaign");
    var id_campaign = campaign.options[campaign.selectedIndex].value;
    var platform    = document.getElementById("platform");
    var id_platform = platform.options[platform.selectedIndex].value;
    var type        = document.getElementById("type");
    var id_type     = type.options[type.selectedIndex].value;

    $.ajax({
            type: "POST",
            url: "<?php echo site_url('dashboard/get_targeting');?>",
            data: {
                    id_brand   : id_brand,
                    id_campaign: id_campaign,
                    id_platform: id_platform,
                    id_type    : id_type
                },
            dataType: "json",
            timeout: 6000, // in milliseconds
            success: function(targeting_val) {
                // console.log(targeting_val);//DEBUG
                // var targeting  = JSON.stringify(result[0].targeting);

                var $targeting = $("#targeting");

                $targeting.empty();

                $.each(targeting_val, function(i, value) {
                    $targeting.append("<option value=''></option>"
                                      +"<option value='"+value.targeting+"'>"
                                      + value.targeting
                                      + "</option>"
                                    );
                });
            },
            error: function(request, status, err) {
                if(status == "timeout") {
                    alert('Failed, Please Reload !');
                }
            }
    });
}

function showDashboard(){
     // Untuk Mendapatkan Parameter
    var brand       = document.getElementById("brand");
    var id_brand    = brand.options[brand.selectedIndex].value;
    var campaign    = document.getElementById("campaign");
    var id_campaign = campaign.options[campaign.selectedIndex].value;
    var platform    = document.getElementById("platform");
    var id_platform = platform.options[platform.selectedIndex].value;
    var date        = document.getElementById("reservation").value;
    var type        = document.getElementById("type");

    var typ;
     for ( var i = 0, len = type.options.length; i < len; i++ ) {
        typ = type.options[i];
        if ( typ.selected === true ) {
            // break;
            var id_type     = type.options[type.selectedIndex].value;
        }
    }

    var target      = document.getElementById("targeting");

    var opt;
    for ( var i = 0, len = target.options.length; i < len; i++ ) {
        opt = target.options[i];
        if ( opt.selected === true ) {
            // break;
            var targeting   = target.options[target.selectedIndex].value;
        }
    }
    // console.log(targeting);//DEBUG

    $.ajax({
        type: "POST",
        url: "<?php echo site_url('dashboard/open_dashboard_chart');?>",
        data:{
                id_brand        : id_brand
                ,id_campaign    : id_campaign
                ,id_platform    : id_platform
                ,id_type        : id_type
                ,targeting      : targeting
                ,date           : date
        },
        dataType: "json",
        timeout: 6000, // in milliseconds
        success: function(result) {
            var campaign    = result.ket[0].campaign;
            var platform    = result.ket[0].platform;
            var from        = result.ket[0].from;
            var to          = result.ket[0].to;
            var ket_dash_1  = result.ket[0].ket_dash_1;
            var ket_dash_2  = result.ket[0].ket_dash_2;

            var data_ln_1 = [], data_ln_2 = [], period = [];
            var dashboar_dt  = result.dash;

            //KALAU DATA LINE 1 & 2 KOSONG
            //ARRAY PUSD DENGAN LOOPING DI JS
            dashboar_dt.forEach(function (i) {
              data_ln_1.push(i.CPC);
              data_ln_2.push(i.Impression);
              period.push(i.date);
            });
            // console.log(chart_1);//DEBUG

            document.getElementById('head_chart').innerHTML = "Chart "+campaign+" "+platform;
            document.getElementById('chart_date').innerHTML = "Placement Date : "+from+" "+to;
            document.getElementById('chart').style.display='block';
//FOR LINE CHART ===========================================================================================================================================>
            var ctx = document.getElementById("dashboard").getContext("2d");
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: period,
                    datasets: [
                      {
                        label: 'CPC',
                        fill: false,
                        fillColor: "#FF6384",
                        lineTension: 0.1,
                        backgroundColor: "#FF6384",
                        borderColor: "#FF6384",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "#FF6384",
                        pointBackgroundColor: "#FF6384",
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "#FF6384",
                        pointHoverBorderColor: "#FF6384",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 7,
                        yAxisID: "y-axis-1",
                        data: data_ln_1
                        // data: ['10%','20%', '21%']
                      },
                      {
                        label: 'IMPRESSION',
                        fill: false,
                        fillColor: "#36A2EB",
                        lineTension: 0.1,
                        backgroundColor: "#36A2EB",
                        borderColor: "#36A2EB",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "#36A2EB",
                        pointBackgroundColor: "#36A2EB",
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "#36A2EB",
                        pointHoverBorderColor: "#36A2EB",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 7,
                        yAxisID: "y-axis-2",
                        data: data_ln_2
                        // data: ['700,000', '713,000', '698,000']
                      }
                    ]
                },
                options: {
                  scales: {
                        yAxes: [
                            { "id":"y-axis-1",
                              display:true,
                              position: "left"
                            },
                            { "id":"y-axis-2",
                              scalePositionLeft: false,
                              display:true,
                              position: "right"
                            }
                        ]
                    }
                }
            });

            $('#search').click(function() {
                var count = 0;
                count += 1;
                if (count == 1) {
                    // console.log('Masuk');
                    myChart.destroy();
                };
            });
//END LINE CHART ===========================================================================================================================================>
            var content = '', select = [];

            for (var i = 0; i < ket_dash_1.length; i++) {
                // console.log(ket_dash_1[i]+' '+ket_dash_2[i]);
                content += "<div class='col-sm-1 col-xs-3'>"
                            +   "<div class='description-block'>"
                            +   "<h5 class='description-header'>"
                            +    ket_dash_2[i]
                            +    "</h5>"
                            +   "<span class='description-text'>"
                            +    ket_dash_1[i]
                            +    "</span>"
                            +    "</div>"
                            +"</div>";
            };

            document.getElementById('ket_dash').innerHTML = content;

            $('.params_1').select2(
                                    {
                                        data: ket_dash_1
                                    }
                                );

            $('.params_2').select2(
                                    {
                                        data : ket_dash_1
                                    }
                                );
//FOR TABLE ================================================================================================================================================>
            document.getElementById('head_table').innerHTML = "Table "+campaign+" "+platform;

            var table = [], title = [], thead = '', tbody = '', show_table = '';
            title = result.table[0].ket;
            table = result.table[0].value;
            // console.log(title);

            // using Post method for table
            var brand       = document.getElementById("brand");
            var id_brand    = brand.options[brand.selectedIndex].value;
            var campaign    = document.getElementById("campaign");
            var id_campaign = campaign.options[campaign.selectedIndex].value;
            var platform    = document.getElementById("platform");
            var id_platform = platform.options[platform.selectedIndex].value;
            var date        = document.getElementById("reservation").value;
            var type        = document.getElementById("type");

            var typ;
             for ( var i = 0, len = type.options.length; i < len; i++ ) {
                typ = type.options[i];
                if ( typ.selected === true ) {
                    // break;
                    var id_type     = type.options[type.selectedIndex].value;
                }
            }

            var target      = document.getElementById("targeting");

            var opt;
            for ( var i = 0, len = target.options.length; i < len; i++ ) {
                opt = target.options[i];
                if ( opt.selected === true ) {
                    // break;
                    var targeting   = target.options[target.selectedIndex].value;
                }
            }

            $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('dashboard/table_dashboard');?>",
                    data: {
                            id_brand        : id_brand
                            ,id_campaign    : id_campaign
                            ,id_platform    : id_platform
                            ,date           : date
                            ,id_type        : id_type
                            ,targeting      : targeting
                        },
                    dataType: "json",
                    timeout: 6000, // in milliseconds
                    success: function(result) {
                        // console.log(result);
                        if(result)
                        $("#for-table").html(result);

                        $(document).ready( function () {
                            var table = $('#table_dash').DataTable({
                                "scrollX": true,
                                "columnDefs": [
                                    {"className": "dt-center", "targets": "_all"}
                                  ]
                            });
                        } );

                        // $('#table_dash').DataTable({
                        //                             "columnDefs": [
                        //                                 {"className": "dt-center", "targets": "_all"}
                        //                             ],
                        //                             "scrollX": true
                        //                           });
                    },
                    error: function(request, status, err) {
                        if(status == "timeout") {
                            alert('Failed Load Table Dashboard, Please Reload !');
                        }
                    }
            });

            document.getElementById('table').style.display='block';
//END TABLE ================================================================================================================================================>
        },
        error: function(request, status, err) {
            if(status == "timeout") {
                alert('Failed to Load Dashboard, Please Reload !');
            }
        }
    });
}

function line_1(){
    $("#params_1").select2("val", "");
    $('#dashboard').remove(); // this is my <canvas> element

    var params_1 = document.getElementById("params_1");
    var line_1 = params_1.options[params_1.selectedIndex].value;
    var params_2 = document.getElementById("params_2");
    var line_2 = params_2.options[params_2.selectedIndex].value;

    var brand       = document.getElementById("brand");
    var id_brand    = brand.options[brand.selectedIndex].value;
    var campaign    = document.getElementById("campaign");
    var id_campaign = campaign.options[campaign.selectedIndex].value;
    var platform    = document.getElementById("platform");
    var id_platform = platform.options[platform.selectedIndex].value;
    var date        = document.getElementById("reservation").value;
    var type        = document.getElementById("type");
    // var id_type     = type.options[type.selectedIndex].value;

     var typ;
     for ( var i = 0, len = type.options.length; i < len; i++ ) {
        typ = type.options[i];
        if ( typ.selected === true ) {
            // break;
            var id_type     = type.options[type.selectedIndex].value;
        }
    }

    var target      = document.getElementById("targeting");

    var opt;
    for ( var i = 0, len = target.options.length; i < len; i++ ) {
        opt = target.options[i];
        if ( opt.selected === true ) {
            // break;
            var targeting   = target.options[target.selectedIndex].value;
        }
    }

    // console.log("id_brand : "+id_brand+" id_campaign : "+id_campaign+" id_platform : "+id_platform+" date : "+date);//DEBUG

    $.ajax({
        type: "POST",
        url: "<?php echo site_url('dashboard/open_dashboard_chart');?>",
        data:{
                id_brand        : id_brand
                ,id_campaign    : id_campaign
                ,id_platform    : id_platform
                ,date           : date
                ,targeting      : targeting
                ,id_type        : id_type
        },
        dataType: "json",
        timeout: 6000, // in milliseconds
        success: function(result) {
            var campaign    = result.ket[0].campaign;
            var platform    = result.ket[0].platform;
            var from        = result.ket[0].from;
            var to          = result.ket[0].to;
            var ket_dash_1  = result.ket[0].ket_dash_1;
            var ket_dash_2  = result.ket[0].ket_dash_2;

            var data_ln_1 = [], data_ln_2 = [], period = [];
            var chart_1  = result.dash;

            //ARRAY PUSH DENGAN LOOPING DI JS
            chart_1.forEach(function (i) {
                if (line_1 == 'Revenue') {
                    line_1 = 'Cost';

                    data_ln_1.push(i[line_1]);
                    data_ln_2.push(i[line_2]);
                    period.push(i.date);
                }
                else if (line_2 == 'Revenue') {
                    line_2 = 'Cost';

                    data_ln_1.push(i[line_1]);
                    data_ln_2.push(i[line_2]);
                    period.push(i.date);
                }
                else {
                    data_ln_1.push(i[line_1]);
                    data_ln_2.push(i[line_2]);
                    period.push(i.date);
                }
            });

            var ctx = document.getElementById("dashboard").getContext("2d");
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: period,
                    datasets: [
                      {
                        label: line_1,
                        fill: false,
                        fillColor: "#FF6384",
                        lineTension: 0.1,
                        backgroundColor: "#FF6384",
                        borderColor: "#FF6384",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "#FF6384",
                        pointBackgroundColor: "#FF6384",
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "#FF6384",
                        pointHoverBorderColor: "#FF6384",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 7,
                        yAxisID: "y-axis-1",
                        data: data_ln_1
                      },
                      {
                        label: line_2,
                        fill: false,
                        fillColor: "#36A2EB",
                        lineTension: 0.1,
                        backgroundColor: "#36A2EB",
                        borderColor: "#36A2EB",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "#36A2EB",
                        pointBackgroundColor: "#36A2EB",
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "#36A2EB",
                        pointHoverBorderColor: "#36A2EB",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 7,
                        yAxisID: "y-axis-2",
                        data: data_ln_2
                      }
                    ]
                },
                options: {

                    scales: {
                        yAxes: [
                            { "id":"y-axis-1",
                              display:true,
                              position: "left"
                            },
                            { "id":"y-axis-2",
                              scalePositionLeft: false,
                              display:true,
                              position: "right"
                            }
                        ]
                    }
                }
            });
        },
        error: function(request, status, err) {
            if(status == "timeout") {
                alert('gagal');
            }
        }
    });

    $('#position_dash').append('<canvas id="dashboard" height="65"><canvas>');//Munculkan Kembali Chart
}

function line_2(){
    $("#params_2").select2("val", "");
    $('#dashboard').remove(); // this is my <canvas> element

    var params_1 = document.getElementById("params_1");
    var line_1 = params_1.options[params_1.selectedIndex].value;
    var params_2 = document.getElementById("params_2");
    var line_2 = params_2.options[params_2.selectedIndex].value;

    var brand       = document.getElementById("brand");
    var id_brand    = brand.options[brand.selectedIndex].value;
    var campaign    = document.getElementById("campaign");
    var id_campaign = campaign.options[campaign.selectedIndex].value;
    var platform    = document.getElementById("platform");
    var id_platform = platform.options[platform.selectedIndex].value;
    var date        = document.getElementById("reservation").value;
    var type        = document.getElementById("type");
    // var id_type     = type.options[type.selectedIndex].value;

    var typ;
     for ( var i = 0, len = type.options.length; i < len; i++ ) {
        typ = type.options[i];
        if ( typ.selected === true ) {
            // break;
            var id_type     = type.options[type.selectedIndex].value;
        }
    }

    var target      = document.getElementById("targeting");

    var opt;
    for ( var i = 0, len = target.options.length; i < len; i++ ) {
        opt = target.options[i];
        if ( opt.selected === true ) {
            // break;
            var targeting   = target.options[target.selectedIndex].value;
        }
    }

    // console.log("id_brand : "+id_brand+" id_campaign : "+id_campaign+" id_platform : "+id_platform+" date : "+date);//DEBUG

    $.ajax({
        type: "POST",
        url: "<?php echo site_url('dashboard/open_dashboard_chart');?>",
        data:{
                id_brand        : id_brand
                ,id_campaign    : id_campaign
                ,id_platform    : id_platform
                ,date           : date
                ,targeting      : targeting
                ,id_type        : id_type
        },
        dataType: "json",
        timeout: 6000, // in milliseconds
        success: function(result) {
            var campaign    = result.ket[0].campaign;
            var platform    = result.ket[0].platform;
            var from        = result.ket[0].from;
            var to          = result.ket[0].to;
            var ket_dash_1  = result.ket[0].ket_dash_1;
            var ket_dash_2  = result.ket[0].ket_dash_2;

            var data_ln_1 = [], data_ln_2 = [], period = [];
            var chart_1  = result.dash;

            //ARRAY PUSH DENGAN LOOPING DI JS
            chart_1.forEach(function (i) {
                if (line_1 == 'Revenue') {
                    line_1 = 'Cost';

                    data_ln_1.push(i[line_1]);
                    data_ln_2.push(i[line_2]);
                    period.push(i.date);
                }
                else if (line_2 == 'Revenue') {
                    line_2 = 'Cost';

                    data_ln_1.push(i[line_1]);
                    data_ln_2.push(i[line_2]);
                    period.push(i.date);
                }
                else {
                    data_ln_1.push(i[line_1]);
                    data_ln_2.push(i[line_2]);
                    period.push(i.date);
                }
            });

            // console.log(data_ln_1);
            var ctx = document.getElementById("dashboard").getContext("2d");
            var myChart = new Chart(ctx, {
                type: 'line',
                data: {
                    labels: period,
                    datasets: [
                      {
                        label: line_1,
                        fill: false,
                        fillColor: "#FF6384",
                        lineTension: 0.1,
                        backgroundColor: "#FF6384",
                        borderColor: "#FF6384",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "#FF6384",
                        pointBackgroundColor: "#FF6384",
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "#FF6384",
                        pointHoverBorderColor: "#FF6384",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 7,
                        yAxisID: "y-axis-1",
                        data: data_ln_1
                      },
                      {
                        label: line_2,
                        fill: false,
                        fillColor: "#36A2EB",
                        lineTension: 0.1,
                        backgroundColor: "#36A2EB",
                        borderColor: "#36A2EB",
                        borderCapStyle: 'butt',
                        borderDash: [],
                        borderDashOffset: 0.0,
                        borderJoinStyle: 'miter',
                        pointBorderColor: "#36A2EB",
                        pointBackgroundColor: "#36A2EB",
                        pointBorderWidth: 5,
                        pointHoverRadius: 5,
                        pointHoverBackgroundColor: "#36A2EB",
                        pointHoverBorderColor: "#36A2EB",
                        pointHoverBorderWidth: 2,
                        pointRadius: 1,
                        pointHitRadius: 7,
                        yAxisID: "y-axis-2",
                        data: data_ln_2
                      }
                    ]
                },
                options: {
                    scales: {
                      yAxes: [
                            { "id":"y-axis-1",
                              display:true,
                              position: "left"
                            },
                            { "id":"y-axis-2",
                              scalePositionLeft: false,
                              display:true,
                              position: "right"
                            }
                        ]
                    }
                }
            });
        },
        error: function(request, status, err) {
            if(status == "timeout") {
                alert('gagal');
            }
        }
    });

    $('#position_dash').append('<canvas id="dashboard" height="65"><canvas>');
}

function download(){
  var brand       = document.getElementById("brand");
  var id_brand    = brand.options[brand.selectedIndex].value;
  var campaign    = document.getElementById("campaign");
  var id_campaign = campaign.options[campaign.selectedIndex].value;
  var platform    = document.getElementById("platform");
  var id_platform = platform.options[platform.selectedIndex].value;
  var date        = document.getElementById("reservation").value;
  var download    = document.getElementById("download");

  // var url = 'http://172.16.163.5/php/PP_AW/download/Download_Stock.php?t='+text_search+'&s='+status;
  var url = "<?php echo site_url('dashboard/download');?>?id_brand="+id_brand+"&id_campaign="+id_campaign+"&id_platform="+id_platform+"&date="+date;
  // alert(url);
  // window.location.href = url;
  // window.open(url,'Download','height=100, width=100, top=50, left=10');
  $('#ifreport').attr('src', url);
}

</script>

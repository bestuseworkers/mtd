<script>
var t = $("#table_dash").DataTable({"scrollX": true});

$(function () {
//FOR SELECT 2
  $('.select_campaign').select2({
                                    placeholder: "Choose",
                                    allowClear: true
                                });

  $('.select_type').select2({
                                placeholder: "Choose",
                                // allowClear: true
                            });
  $('.select_targeting').select2({
                                    placeholder: "Choose",
                                    // allowClear: true
                                });

//FOR DATE RANGE PICKER
  $('#pub-date').daterangepicker();
});

// Hiding Selector
$(document).ready(function() {
    // console.log("coba katakan");
    document.getElementById('non-publisher').style.display='none';
    document.getElementById('publisher').style.display='block';
});

// Get Data Dashboard
function showDashboard(){
    // Untuk Mendapatkan data
    var id_campaign   = document.getElementById("campaign_pub").value;
    var date          = document.getElementById("pub-date").value;
    var name          = document.getElementById("pub-nama").value;

    // console.log(id_campaign);//DEBUG
    $.ajax({ //Untuk Mendapatkan Data Dashboard yang dibutuhkan.
            type: "POST",
            url: "<?php echo site_url('dashboard/get_uid');?>",
            data: {
                    id_campaign : id_campaign,
                    name        : name,
                    date        : date
                },
            dataType: "json",
            timeout: 6000, // in milliseconds
            success: function(result) {
                if(result)

                var name_1         = result.name;
                var id_brand       = result.id_brand;
                var id_campaign    = result.id_campaign;
                var id_platform    = result.id_platform;
                var u_id           = result.u_id;
                var date_from      = result.date_from;
                var date_to        = result.date_to;

                //Proses Menampilkan Data Dashboard Publisher
                    $.ajax({
                            type: "POST",
                            url: "<?php echo site_url('dashboard/open_dashboard_pub');?>",
                            data: {
                                    name        : name_1,
                                    id_brand    : id_brand,
                                    id_campaign : id_campaign,
                                    id_platform : id_platform,
                                    u_id        : u_id,
                                    date_from   : date_from,
                                    date_to     : date_to,
                                },
                            dataType: "json",
                            timeout: 6000, // in milliseconds
                            success: function(result) {
                                var campaign    = result.ket[0].campaign;
                                var platform    = result.ket[0].platform;
                                var from        = result.ket[0].from;
                                var to          = result.ket[0].to;
                                var ket_dash_1  = result.ket[0].ket_dash_1;
                                var ket_dash_2  = result.ket[0].ket_dash_2;

                                var data_ln_1 = [], data_ln_2 = [], period = [];
                                var dashboar_dt  = result.dash;

                                //KALAU DATA LINE 1 & 2 KOSONG
                                //ARRAY PUSD DENGAN LOOPING DI JS
                                dashboar_dt.forEach(function (i) {
                                    data_ln_1.push(i.CPC);
                                    data_ln_2.push(i.Impression);
                                    period.push(i.date);
                                });

                                document.getElementById('head_chart').innerHTML = "Chart "+campaign+" "+platform;
                                document.getElementById('chart_date').innerHTML = "Placement Date : "+from+" "+to;
                                document.getElementById('chart').style.display='block';
                    //FOR LINE CHART ===========================================================================================================================================>
                                var ctx = document.getElementById("dashboard").getContext("2d");
                                var myChart = new Chart(ctx, {
                                    type: 'line',
                                    data: {
                                        labels: period,
                                        datasets: [
                                        {
                                            label: 'CPC',
                                            fill: false,
                                            fillColor: "#FF6384",
                                            lineTension: 0.1,
                                            backgroundColor: "#FF6384",
                                            borderColor: "#FF6384",
                                            borderCapStyle: 'butt',
                                            borderDash: [],
                                            borderDashOffset: 0.0,
                                            borderJoinStyle: 'miter',
                                            pointBorderColor: "#FF6384",
                                            pointBackgroundColor: "#FF6384",
                                            pointBorderWidth: 5,
                                            pointHoverRadius: 5,
                                            pointHoverBackgroundColor: "#FF6384",
                                            pointHoverBorderColor: "#FF6384",
                                            pointHoverBorderWidth: 2,
                                            pointRadius: 1,
                                            pointHitRadius: 7,
                                            yAxisID: "y-axis-1",
                                            data: data_ln_1
                                            // data: ['10%','20%', '21%']
                                        },
                                        {
                                            label: 'IMPRESSION',
                                            fill: false,
                                            fillColor: "#36A2EB",
                                            lineTension: 0.1,
                                            backgroundColor: "#36A2EB",
                                            borderColor: "#36A2EB",
                                            borderCapStyle: 'butt',
                                            borderDash: [],
                                            borderDashOffset: 0.0,
                                            borderJoinStyle: 'miter',
                                            pointBorderColor: "#36A2EB",
                                            pointBackgroundColor: "#36A2EB",
                                            pointBorderWidth: 5,
                                            pointHoverRadius: 5,
                                            pointHoverBackgroundColor: "#36A2EB",
                                            pointHoverBorderColor: "#36A2EB",
                                            pointHoverBorderWidth: 2,
                                            pointRadius: 1,
                                            pointHitRadius: 7,
                                            yAxisID: "y-axis-2",
                                            data: data_ln_2
                                            // data: ['700,000', '713,000', '698,000']
                                        }
                                        ]
                                    },
                                    options: {
                                    scales: {
                                            yAxes: [
                                                { "id":"y-axis-1",
                                                display:true,
                                                position: "left"
                                                },
                                                { "id":"y-axis-2",
                                                scalePositionLeft: false,
                                                display:true,
                                                position: "right"
                                                }
                                            ]
                                        }
                                    }
                                });

                                $('#search').click(function() {
                                    var count = 0;
                                    count += 1;
                                    if (count == 1) {
                                        // console.log('Masuk');
                                        myChart.destroy();
                                    };
                                });
                    //END LINE CHART ===========================================================================================================================================>
                                var content='',content1='',content2='', select = [];

                                // console.log(ket_dash_1.length);
                                if (ket_dash_1.length > 12) {
                                  for (var i = 1; i <= 12; i++) {
                                    content1 +="<div class='col-sm-1 col-xs-3'>"
                                    +   "<div class='description-block'>"
                                    +   "<h5 class='description-header'>"
                                    +    ket_dash_2[i]
                                    +    "</h5>"
                                    +   "<span class='description-text'>"
                                    +    ket_dash_1[i]
                                    +    "</span>"
                                    +    "</div>"
                                    +"</div>";
                                  };
                                  for (var i = 13; i < ket_dash_1.length; i++) {
                                    content2 +="<div class='col-sm-1 col-xs-3'>"
                                    +   "<div class='description-block'>"
                                    +   "<h5 class='description-header'>"
                                    +    ket_dash_2[i]
                                    +    "</h5>"
                                    +   "<span class='description-text'>"
                                    +    ket_dash_1[i]
                                    +    "</span>"
                                    +    "</div>"
                                    +"</div>";
                                  }

                                  document.getElementById('ket_dash').innerHTML = '<div class="row" style="margin:auto;">'+content1+'</div><div class="row" style="margin:auto;">'+content2+'</div>';
                                }
                                else {
                                  for (var i = 0; i < ket_dash_1.length; i++) {
                                    content += "<div class='col-sm-1 col-xs-3'>"
                                    +   "<div class='description-block'>"
                                    +   "<h5 class='description-header'>"
                                    +    ket_dash_2[i]
                                    +    "</h5>"
                                    +   "<span class='description-text'>"
                                    +    ket_dash_1[i]
                                    +    "</span>"
                                    +    "</div>"
                                    +"</div>";
                                  };

                                  document.getElementById('ket_dash').innerHTML = content;
                                }

                                $('.params_1').select2(
                                                        {
                                                            data: ket_dash_1
                                                        }
                                                    );

                                $('.params_2').select2(
                                                        {
                                                            data : ket_dash_1
                                                        }
                                                    );
                    //FOR TABLE ================================================================================================================================================>
                                document.getElementById('head_table').innerHTML = "Table "+campaign+" "+platform;

                                var table = [], title = [], thead = '', tbody = '', show_table = '';
                                title = result.table[0].ket;
                                table = result.table[0].value;

                                $.ajax({
                                        type: "POST",
                                        url: "<?php echo site_url('dashboard/get_table_publisher');?>",
                                        data: {
                                                id_brand       : id_brand,
                                                id_campaign    : id_campaign,
                                                id_platform    : id_platform,
                                                date           : date,
                                                name           : name,
                                            },
                                        dataType: "json",
                                        timeout: 6000, // in milliseconds
                                        success: function(result) {
                                            if(result)
                                            $("#for-table").html(result);

                                            $(document).ready( function () {
                                                var table = $('#table_dash').DataTable({
                                                    "scrollX": true,
                                                    "columnDefs": [
                                                        {"className": "dt-center", "targets": "_all"}
                                                    ]
                                                });
                                            } );
                                        },
                                        error: function(request, status, err) {
                                            if(status == "timeout") {
                                                alert('Failed Load Table Dashboard, Please Reload !');
                                            }
                                        }
                                });

                                document.getElementById('table').style.display='block';

                            },
                            error: function(request, status, err) {
                                if(status == "timeout") {
                                    alert('Failed To Get Data Publisher !');
                                }
                            }
                    });
            },
            error: function(request, status, err) {
                if(status == "timeout") {
                    alert('Failed To Get Data Publisher !');
                }
            }
    });
}


// Choose Different Parameter for Dashboard
function line_1(){
    $("#params_1").select2("val", "");
    $('#dashboard').remove(); // this is my <canvas> element

    // Untuk Mendapatkan session data
    var date  = document.getElementById("pub-date").value;
    var name  = document.getElementById("pub-nama").value;
    // Untuk Mendapatkan Parameter dari Chart
    var params_1 = document.getElementById("params_1");
    var line_1 = params_1.options[params_1.selectedIndex].value;
    var params_2 = document.getElementById("params_2");
    var line_2 = params_2.options[params_2.selectedIndex].value;

    $.ajax({ //Untuk Mendapatkan Data Dashboard yang dibutuhkan.
        type: "POST",
        url: "<?php echo site_url('dashboard/get_uid');?>",
        data: {
                name : name,
                date : date
            },
        dataType: "json",
        timeout: 6000, // in milliseconds
        success: function(result) {
            if(result)

            var name_1         = result.name;
            var id_brand       = result.id_brand;
            var id_campaign    = result.id_campaign;
            var id_platform    = result.id_platform;
            var u_id           = result.u_id;
            var date_from      = result.date_from;
            var date_to        = result.date_to;

            $.ajax({ //Untuk Mendapatkan Data Dashboard yang dibutuhkan.
                type: "POST",
                url: "<?php echo site_url('dashboard/open_dashboard_pub');?>",
                data: {
                        name        : name_1,
                        u_id        : u_id,
                        id_brand    : id_brand,
                        id_campaign : id_campaign,
                        id_platform : id_platform,
                        date_from   : date_from,
                        date_to     : date_to,
                    },
                dataType: "json",
                timeout: 6000, // in milliseconds
                success: function(result) {
                    if(result)

                    var data_ln_1 = [], data_ln_2 = [], period = [];
                    var chart_1  = result.dash;

                    //ARRAY PUSH DENGAN LOOPING DI JS
                    chart_1.forEach(function (i) {
                        if (line_1 == 'Revenue') {
                            line_1 = 'Cost';

                            data_ln_1.push(i[line_1]);
                            data_ln_2.push(i[line_2]);
                            period.push(i.date);
                        }
                        else if (line_2 == 'Revenue') {
                            line_2 = 'Cost';

                            data_ln_1.push(i[line_1]);
                            data_ln_2.push(i[line_2]);
                            period.push(i.date);
                        }
                        else {
                            data_ln_1.push(i[line_1]);
                            data_ln_2.push(i[line_2]);
                            period.push(i.date);
                        }
                    });
                    console.log(chart_1);

                    var ctx = document.getElementById("dashboard").getContext("2d");
                    var myChart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: period,
                            datasets: [
                            {
                                label: line_1,
                                fill: false,
                                fillColor: "#FF6384",
                                lineTension: 0.1,
                                backgroundColor: "#FF6384",
                                borderColor: "#FF6384",
                                borderCapStyle: 'butt',
                                borderDash: [],
                                borderDashOffset: 0.0,
                                borderJoinStyle: 'miter',
                                pointBorderColor: "#FF6384",
                                pointBackgroundColor: "#FF6384",
                                pointBorderWidth: 5,
                                pointHoverRadius: 5,
                                pointHoverBackgroundColor: "#FF6384",
                                pointHoverBorderColor: "#FF6384",
                                pointHoverBorderWidth: 2,
                                pointRadius: 1,
                                pointHitRadius: 7,
                                yAxisID: "y-axis-1",
                                data: data_ln_1
                            },
                            {
                                label: line_2,
                                fill: false,
                                fillColor: "#36A2EB",
                                lineTension: 0.1,
                                backgroundColor: "#36A2EB",
                                borderColor: "#36A2EB",
                                borderCapStyle: 'butt',
                                borderDash: [],
                                borderDashOffset: 0.0,
                                borderJoinStyle: 'miter',
                                pointBorderColor: "#36A2EB",
                                pointBackgroundColor: "#36A2EB",
                                pointBorderWidth: 5,
                                pointHoverRadius: 5,
                                pointHoverBackgroundColor: "#36A2EB",
                                pointHoverBorderColor: "#36A2EB",
                                pointHoverBorderWidth: 2,
                                pointRadius: 1,
                                pointHitRadius: 7,
                                yAxisID: "y-axis-2",
                                data: data_ln_2
                            }
                            ]
                        },
                        options: {

                            scales: {
                                yAxes: [
                                    { "id":"y-axis-1",
                                    display:true,
                                    position: "left"
                                    },
                                    { "id":"y-axis-2",
                                    scalePositionLeft: false,
                                    display:true,
                                    position: "right"
                                    }
                                ]
                            }
                        }
                    });
                },
                error: function(request, status, err) {
                    if(status == "timeout") {
                        alert('Failed To Get Data Publisher !');
                    }
                }
            });

        },
        error: function(request, status, err) {
            if(status == "timeout") {
                alert('Failed To Get Data Publisher !');
            }
        }
    });

    $('#position_dash').append('<canvas id="dashboard" height="65"><canvas>');//Munculkan Kembali Chart
}

// Choose Different Parameter for Dashboard
function line_2(){
    $("#params_2").select2("val", "");
    $('#dashboard').remove(); // this is my <canvas> element

    // Untuk Mendapatkan session data
    var date  = document.getElementById("pub-date").value;
    var name  = document.getElementById("pub-nama").value;
    // Untuk Mendapatkan Parameter dari Chart
    var params_1 = document.getElementById("params_1");
    var line_1 = params_1.options[params_1.selectedIndex].value;
    var params_2 = document.getElementById("params_2");
    var line_2 = params_2.options[params_2.selectedIndex].value;

    $.ajax({ //Untuk Mendapatkan Data Dashboard yang dibutuhkan.
        type: "POST",
        url: "<?php echo site_url('dashboard/get_uid');?>",
        data: {
                name : name,
                date : date
            },
        dataType: "json",
        timeout: 6000, // in milliseconds
        success: function(result) {
            if(result)

            var name_1         = result.name;
            var id_brand       = result.id_brand;
            var id_campaign    = result.id_campaign;
            var id_platform    = result.id_platform;
            var u_id           = result.u_id;
            var date_from      = result.date_from;
            var date_to        = result.date_to;

            $.ajax({ //Untuk Mendapatkan Data Dashboard yang dibutuhkan.
                type: "POST",
                url: "<?php echo site_url('dashboard/open_dashboard_pub');?>",
                data: {
                        name        : name_1,
                        u_id        : u_id,
                        id_brand    : id_brand,
                        id_campaign : id_campaign,
                        id_platform : id_platform,
                        date_from   : date_from,
                        date_to     : date_to,
                    },
                dataType: "json",
                timeout: 6000, // in milliseconds
                success: function(result) {
                    if(result)
                    console.log(result);//DEBUG

                    var data_ln_1 = [], data_ln_2 = [], period = [];
                    var chart_1  = result.dash;

                    //ARRAY PUSH DENGAN LOOPING DI JS
                    chart_1.forEach(function (i) {
                        if (line_1 == 'Revenue') {
                            line_1 = 'Cost';

                            data_ln_1.push(i[line_1]);
                            data_ln_2.push(i[line_2]);
                            period.push(i.date);
                        }
                        else if (line_2 == 'Revenue') {
                            line_2 = 'Cost';

                            data_ln_1.push(i[line_1]);
                            data_ln_2.push(i[line_2]);
                            period.push(i.date);
                        }
                        else {
                            data_ln_1.push(i[line_1]);
                            data_ln_2.push(i[line_2]);
                            period.push(i.date);
                        }
                    });
                    // console.log(data_ln_1);

                    var ctx = document.getElementById("dashboard").getContext("2d");
                    var myChart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: period,
                            datasets: [
                            {
                                label: line_1,
                                fill: false,
                                fillColor: "#FF6384",
                                lineTension: 0.1,
                                backgroundColor: "#FF6384",
                                borderColor: "#FF6384",
                                borderCapStyle: 'butt',
                                borderDash: [],
                                borderDashOffset: 0.0,
                                borderJoinStyle: 'miter',
                                pointBorderColor: "#FF6384",
                                pointBackgroundColor: "#FF6384",
                                pointBorderWidth: 5,
                                pointHoverRadius: 5,
                                pointHoverBackgroundColor: "#FF6384",
                                pointHoverBorderColor: "#FF6384",
                                pointHoverBorderWidth: 2,
                                pointRadius: 1,
                                pointHitRadius: 7,
                                yAxisID: "y-axis-1",
                                data: data_ln_1
                            },
                            {
                                label: line_2,
                                fill: false,
                                fillColor: "#36A2EB",
                                lineTension: 0.1,
                                backgroundColor: "#36A2EB",
                                borderColor: "#36A2EB",
                                borderCapStyle: 'butt',
                                borderDash: [],
                                borderDashOffset: 0.0,
                                borderJoinStyle: 'miter',
                                pointBorderColor: "#36A2EB",
                                pointBackgroundColor: "#36A2EB",
                                pointBorderWidth: 5,
                                pointHoverRadius: 5,
                                pointHoverBackgroundColor: "#36A2EB",
                                pointHoverBorderColor: "#36A2EB",
                                pointHoverBorderWidth: 2,
                                pointRadius: 1,
                                pointHitRadius: 7,
                                yAxisID: "y-axis-2",
                                data: data_ln_2
                            }
                            ]
                        },
                        options: {

                            scales: {
                                yAxes: [
                                    { "id":"y-axis-1",
                                    display:true,
                                    position: "left"
                                    },
                                    { "id":"y-axis-2",
                                    scalePositionLeft: false,
                                    display:true,
                                    position: "right"
                                    }
                                ]
                            }
                        }
                    });
                },
                error: function(request, status, err) {
                    if(status == "timeout") {
                        alert('Failed To Get Data Publisher !');
                    }
                }
            });

        },
        error: function(request, status, err) {
            if(status == "timeout") {
                alert('Failed To Get Data Publisher !');
            }
        }
    });

    $('#position_dash').append('<canvas id="dashboard" height="65"><canvas>');//Munculkan Kembali Chart
}

</script>

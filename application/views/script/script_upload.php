<script>

$(function () {

//START SELECT 2 ================================================================================================================>

$('.select_brand').select2({
                                placeholder: "Select a Brand",
                                allowClear: true
                            });
$('.select_campaign').select2({
                                placeholder: "Select a Campaign",
                                allowClear: true
                            });
$('.select_platform').select2({
                                placeholder: "Select a Platform",
                                allowClear: true
                            });

//END SELECT 2 ==================================================================================================================>

//START Date range picker =======================================================================================================>
$('#reservation').daterangepicker();
//END Date range picker =========================================================================================================>

var platform = document.getElementById("platform").value;

//FOR TABLE
if (platform == 'Facebook') {
    var table_campaign = $("#table_campaign").DataTable({
                                                    "columnDefs": [
                                                            {
                                                                "targets": [0],
                                                                "visible": false,
                                                                "searchable": false
                                                            }
                                                        ]
                                                     ,"bFilter": false
                                                     ,"bLengthChange": false
                                                     ,"bPaginate": false
                                                     ,"info":     false
                                                     ,"scrollX": true
                                                    });
}
else if (platform == 'Instagram') {
  var table_campaign = $("#table_campaign").DataTable({
                                                  "columnDefs": [
                                                          {
                                                              "targets": [0],
                                                              "visible": false,
                                                              "searchable": false
                                                          }
                                                      ]
                                                   ,"bFilter": false
                                                   ,"bLengthChange": false
                                                   ,"bPaginate": false
                                                   ,"info":     false
                                                   ,"scrollX": true
                                                  });
}
else {
    var table_campaign = $("#table_campaign").DataTable({
                                                    "columnDefs": [
                                                            {
                                                                "targets": [0],
                                                                "visible": false,
                                                                "searchable": false
                                                            }
                                                        ]
                                                     ,"bFilter": false
                                                     ,"bLengthChange": false
                                                     ,"bPaginate": false
                                                     ,"info":     false
                                                    });
}


});

//START SELECT METHOD =======================================================================================================>
function changeFunc() {
    var selectBox = document.getElementById("get_brand");
    var id_brand = selectBox.options[selectBox.selectedIndex].value;

    $.ajax({
            type: "POST",
            url: "<?php echo site_url('admin/upload/get_campaign');?>",
            data: { id_brand : id_brand},
            dataType: "json",
            timeout: 6000, // in milliseconds
            success: function(result) {
                // console.log( JSON.stringify(result) );//debug

                var $campaign = $("#campaign");

                $campaign.empty();

                $.each(result, function(i, value) {
                    $campaign.append("<option value=''></option>"
                                      +"<option value='"+value.id_campaign+"'>"
                                      + value.name
                                      + "</option>"
                                    );
                });

                // $.each(result, function (i, item) {
                //     $('#campaign').append($('<option>', {
                //         value: item.id_campaign,
                //         text : item.name
                //     }));
                // });
            },
            error: function(request, status, err) {
                if(status == "timeout") {
                    alert('gagal');
                }
            }
        });

}
//END SELECT METHOD =======================================================================================================>

</script>

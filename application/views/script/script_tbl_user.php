<script>
$(function () {
    var table_user = $("#table_user").DataTable({
                                                    "columnDefs": [
                                                            {
                                                                "targets": [0],
                                                                "visible": false,
                                                                "searchable": false
                                                            },
                                                            {
                                                                "targets": [1],
                                                                "visible": false,
                                                                "searchable": false
                                                            },
                                                            {
                                                                "targets": [2],
                                                                "visible": false,
                                                                "searchable": false
                                                            }
                                                        ]
                                                    });

    $('#table_user tbody').on( 'click', 'tr', function () {//Untuk Select table
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table_user.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    $('#edit_user').click( function () {//Untuk Edit
        // console.log(val);//DEBUG
        var user = $.map(table_user.rows('.selected').data(), function (item) {
            var id_user     = item[0];
            var id_agency   = item[1];
            var id_sales    = item[2];
            var agency_name = item[3];
            var sales_name  = item[4];
            var name        = item[5];
            var user_name   = item[6];
            var active      = item[7];
            var role        = item[8];
            var user       = [id_user, id_agency, id_sales, agency_name, sales_name, name, user_name, active, role];
            return user
        });

        // console.log( user );//debug
        //FOR MODALS
        $("#id_user").val(user[0]);

        $("#agency_old").val(user[1]);
        $("#sales_old").val(user[2]);
        
        $("#agency_name").val(user[3]);
        $("#sales_name").val(user[4]);
        $(".select_agency").select2();
        $(".select_sales").select2();
        
        $("#name").val(user[5]);
        $("#user_name").val(user[6]);
        
        $("#active").val(user[7]);
        $(".select_active").select2();

        $("#role").val(user[8]);
        $(".select_role").select2();
    
    });

    $('#dlt_agency').click( function () {//Untuk Menghapus

        var id_user = $.map(table_user.rows('.selected').data(), function (item) {
            return item[0]
        });

        // console.log( id_user );//debug

        table_user.row('.selected').remove().draw( false );

        // Sending Data to Controller
        $.post("<?php echo site_url('admin/user_clients/delete_user');?>",
                    {
                       //TODO: key untuk delete
                       id_user : id_user
                    }
            );
        //with href
        // window.location.href = "<?php echo site_url('admin/agency_sales/delete_agency/"+data+"'); ?>";
    });

//START CAMPAIGN =====================================================================================================================================>
var table_campaign = $("#table_campaign").DataTable({
                                                    "columnDefs": [
                                                            {
                                                                "targets": [1],
                                                                "visible": false,
                                                                "searchable": false
                                                            }
                                                        ]
                                                    });

    $('#table_campaign tbody').on( 'click', 'tr', function () {//Untuk Select table
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
        }
        else {
            table_campaign.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });

    
//END=================>
  });

//START SELECT========================================================================================================================================>

$(document).ready(function() {
    $(".select2").select2();
    
    document.getElementById('agency').style.display='none';
    document.getElementById('sales').style.display='none';
    document.getElementById('non-user').style.display='inline';
    document.getElementById('user').style.display='none';
});

//FOR HIDE AND SHOW SELECT AREA
function Hide(val){
    console.log(val);//DEBUG
    if(val=='superadmin' || val == 'user')
    {
        document.getElementById('agency').style.display='none';
        document.getElementById('sales').style.display='none';
        document.getElementById('non-user').style.display='inline';
        document.getElementById('user').style.display='none';
    }
    if(val=='client')
    { 
        document.getElementById('agency').style.display='inline';
        document.getElementById('sales').style.display='inline';
        document.getElementById('non-user').style.display='inline';
        document.getElementById('user').style.display='none';
    }
    if (val=='user') {
        document.getElementById('non-user').style.display='none';
        document.getElementById('user').style.display='inline';
    }
    if (val=='publisher') {
        document.getElementById('non-user').style.display='none';
        document.getElementById('user').style.display='none';
        document.getElementById('publisher').style.display='inline';
    }
}

//FOR CAMPAIGN ========================================================================================================================================>

</script>

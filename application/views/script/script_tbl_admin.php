<script>

$(function () {

// For Select 2 JS

$(".select_sales").select2();

$(".select_position").select2();



//Start JS

    var table_sales = $("#table_sales").DataTable({

                                                    "columnDefs": [

                                                            {

                                                                "targets": [0],

                                                                "visible": false,

                                                                "searchable": false

                                                            },

                                                            {

                                                                "targets": [1],

                                                                "visible": false,

                                                                "searchable": false

                                                            }

                                                        ]

                                                    });



    $('#table_sales tbody').on( 'click', 'tr', function () {//Untuk Select table

        if ( $(this).hasClass('selected') ) {

            $(this).removeClass('selected');

        }

        else {

            table_sales.$('tr.selected').removeClass('selected');

            $(this).addClass('selected');

        }

    });



    $('#edit_sales_js').click( function () {//Untuk Edit
        $("#head_div").hide();

        var json;

        var sales = $.map(table_sales.rows('.selected').data(), function (item) {
            // console.log(item);
            var id_sales    = item[0];

            $.ajax({
                type: "POST",
                url: "<?php echo site_url('admin/Agency_sales/get_position');?>",
                data: { id_sales : id_sales},
                dataType: "json",
                timeout: 6000, // in milliseconds
                success: function(result) {
                    // console.log( JSON.stringify(result) );//debug
                    var result = JSON.stringify(result);
                    json   = JSON.parse(result); 
                            
                    var id_sales   = json[0].id_sales;
                    var id_head    = json[0].id_head;
                    var sales_name = json[0].sales_name;
                    var email      = json[0].email;
                    var position   = json[0].position;

                    console.log(json);

                    $('#edt_id_sales').val(id_sales);
                    $('#edt_id_head').val(id_head);
                    $('#edt_sales_name').val(sales_name);
                    $('#edt_email').val(email);
                    $('#edt_sales_position').val(position);

                    //Position Before
                    console.log(position);//Account Executive
                    if (position == "Account Executive") {
                        $("#head_div").show();
                    }

                },
                error: function(request, status, err) {
                    if(status == "timeout") {
                        alert('Please Reload Page !');
                    }
                }
            });
            

        });

        // $("#edt_id_sales").json[0].id_sales;
        // $("#edt_id_head").json[0].id_head;
        // $("#edt_sales_name").json[0].sales_name;
        // $("#edt_email").json[0].email;
        // $("#position").json[0].position;  

    });



    $('#delete_sales').click( function () {//Untuk Menghapus



        var id_sales = $.map(table_sales.rows('.selected').data(), function (item) {

            return item[0]

        });



        // console.log( id_sales );//debug



        table_sales.row('.selected').remove().draw( false );

        // Sending Data to Controller

        $.post("<?php echo site_url('admin/agency_sales/delete_sales');?>",

                    {

                       //TODO: key untuk delete

                       id_sales : id_sales

                    }

            );

        //with href

        // window.location.href = "<?php echo site_url('admin/agency_sales/delete_agency/"+data+"'); ?>";

    
    });

//BEGIN TABLE AGENCY===========================================================================================================================>



    var table_agency = $('#table_agency').DataTable({

                                                    "columnDefs": [

                                                            {

                                                                "targets": [0],

                                                                "visible": false,

                                                                "searchable": false

                                                            },

                                                            {

                                                                "targets": [5],

                                                                "visible": false,

                                                                "searchable": false

                                                            }

                                                        ]

                                                    });



        $('#table_agency tbody').on( 'click', 'tr', function () {

            if ( $(this).hasClass('selected') ) {

                $(this).removeClass('selected');

            }

            else {

                table_agency.$('tr.selected').removeClass('selected');

                $(this).addClass('selected');

            }

        });



        $('#dlt_agency').click( function () {//Untuk Menghapus



            var id_agency = $.map(table_agency.rows('.selected').data(), function (item) {

                return item[0]

            });



            // console.log( id_agency );//debug



            table_agency.row('.selected').remove().draw( false );

            // Sending Data to Controller

            $.post("<?php echo site_url('admin/agency_sales/delete_agency');?>",

                        {

                           //TODO: key untuk delete

                           id_agency : id_agency

                        }

                );

            //with href

            // window.location.href = "<?php echo site_url('admin/agency_sales/delete_agency/"+data+"'); ?>";

        });



        $('#edit').click( function () {//Untuk Edit



            var agency = $.map(table_agency.rows('.selected').data(), function (item) {

                var id        = item[0];

                var pt        = item[1];

                var name      = item[2];

                var email     = item[3];

                var sales     = item[4];

                var id_sales  = item[5];

                var agency    = [id, pt, name, email, sales, id_sales];

                return agency

            });


            // console.log( agency );//debug


            $("#agency_id").val(agency[0]);

            $("#pt_edit").val(agency[1]);

            $("#name").val(agency[2]);

            $("#email_edit").val(agency[3]);

            $("#sales_edit").val(agency[4]);

            $(".sales_edit").select2();

            $("#sales_old").val(agency[5]);

        });

  });

// For Hiding SGH Option
document.getElementById('sgh').style.display='none';

//FOR HIDE AND SHOW SELECT AREA ADD SALES
function Show(val){
    // console.log(val);//DEBUG
    if(val=='Account Executive')
    {   
        document.getElementById('sgh').style.display='inline';
    }
}

//FOR HIDE AND SHOW SELECT AREA EDIT SALES
function lihat(val){
    console.log(val);
    if (val == 'Account Executive') {
        document.getElementById('head_div').style.display='inline';
    }
    else{
        document.getElementById('head_div').style.display='none';
    }
}

</script>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; MicroAd BLADE Indonesia <?php echo date('Y'); ?></strong> All rights reserved.
</footer>
<table id="table_dash" class="table table-bordered table-striped">
    <thead id="thead">
      <tr>
        <?php
            foreach ($th as $key => $th) {
        ?>
            <th><?php echo $th; ?></th>
        <?php
            }
        ?>
      </tr>
    </thead>
    <tbody>
        <?php
        // echo '<pre/>'; print_r($temp_table); die;
          foreach ($temp_table as $key => $temp_table) {
            if ($platform_name == 'Facebook') {
        ?>
              <tr>
                <td><?php echo $temp_table['date']; ?></td>
                <td><?php echo $temp_table['cost']; ?></td>
                <td><?php echo $temp_table['impression']; ?></td>
                <td><?php echo $temp_table['clicks']; ?></td>
                <td><?php echo $temp_table['ctr']; ?></td>
                <td><?php echo $temp_table['engagement']; ?></td>
                <td><?php echo $temp_table['eng_rate']; ?></td>
                <td><?php echo $temp_table['page_likes']; ?></td>
                <td><?php echo $temp_table['like_rates']; ?></td>
                <td><?php echo $temp_table['views_video']; ?></td>
                <td><?php echo $temp_table['view_rates']; ?></td>
                <td><?php echo $temp_table['conversions']; ?></td>
                <td><?php echo $temp_table['leads']; ?></td>
                <td><?php echo $temp_table['reach']; ?></td>
                <td><?php echo $temp_table['cvr']; ?></td>
                <td><?php echo $temp_table['cpc']; ?></td>
                <td><?php echo $temp_table['cpm']; ?></td>
                <td><?php echo $temp_table['cpe']; ?></td>
                <td><?php echo $temp_table['cpl']; ?></td>
                <td><?php echo $temp_table['cpv']; ?></td>
                <td><?php echo $temp_table['cpa']; ?></td>
                <td><?php echo $temp_table['cpleads']; ?></td>
                <td><?php echo $temp_table['cpreach']; ?></td>
                <td><?php echo $temp_table['adRecall']; ?></td>
                <td><?php echo $temp_table['session']; ?></td>
              </tr>
        <?php
            }
            elseif ($platform_name == 'Programmatic') {
        ?>
              <tr>
                <td><?php echo $temp_table['date']; ?></td>
                <td><?php echo $temp_table['device']; ?></td>
                <td><?php echo $temp_table['targeting']; ?></td>
                <td><?php echo $temp_table['cost']; ?></td>
                <td><?php echo $temp_table['impression']; ?></td>
                <td><?php echo $temp_table['clicks']; ?></td>
                <td><?php echo $temp_table['ctr']; ?></td>
                <td><?php echo $temp_table['engagement']; ?></td>
                <td><?php echo $temp_table['eng_rate']; ?></td>
                <td><?php echo $temp_table['conversions']; ?></td>
                <td><?php echo $temp_table['views_video']; ?></td>
                <td><?php echo $temp_table['view_rates']; ?></td>
                <td><?php echo $temp_table['cpv']; ?></td>
                <td><?php echo $temp_table['cvr']; ?></td>
                <td><?php echo $temp_table['cpc']; ?></td>
                <td><?php echo $temp_table['cpm']; ?></td>
                <td><?php echo $temp_table['cpa']; ?></td>
                <td><?php echo $temp_table['cpe']; ?></td>
                <td><?php echo $temp_table['session']; ?></td>
              </tr>
        <?php
            }
            elseif ($platform_name == 'Twitter') {
        ?>
              <tr>
                <td><?php echo $temp_table['date']; ?></td>
                <td><?php echo $temp_table['cost']; ?></td>
                <td><?php echo $temp_table['impression']; ?></td>
                <td><?php echo $temp_table['clicks']; ?></td>
                <td><?php echo $temp_table['ctr']; ?></td>
                <td><?php echo $temp_table['engagement']; ?></td>
                <td><?php echo $temp_table['eng_rate']; ?></td>
                <td><?php echo $temp_table['followers']; ?></td>
                <td><?php echo $temp_table['follow_rates']; ?></td>
                <td><?php echo $temp_table['views_video']; ?></td>
                <td><?php echo $temp_table['view_rates']; ?></td>
                <td><?php echo $temp_table['cpc']; ?></td>
                <td><?php echo $temp_table['cpm']; ?></td>
                <td><?php echo $temp_table['cpf']; ?></td>
                <td><?php echo $temp_table['session']; ?></td>
              </tr>
        <?php
            }
            elseif ($platform_name == 'Instagram') {
        ?>
              <tr>
                <td><?php echo $temp_table['date']; ?></td>
                <td><?php echo $temp_table['cost']; ?></td>
                <td><?php echo $temp_table['impression']; ?></td>
                <td><?php echo $temp_table['clicks']; ?></td>
                <td><?php echo $temp_table['ctr']; ?></td>
                <td><?php echo $temp_table['engagement']; ?></td>
                <td><?php echo $temp_table['leads']; ?></td>
                <td><?php echo $temp_table['reach']; ?></td>
                <td><?php echo $temp_table['eng_rate']; ?></td>
                <td><?php echo $temp_table['views_video']; ?></td>
                <td><?php echo $temp_table['view_rates']; ?></td>
                <td><?php echo $temp_table['cpc']; ?></td>
                <td><?php echo $temp_table['cpm']; ?></td>
                <td><?php echo $temp_table['cpe']; ?></td>
                <td><?php echo $temp_table['cpv']; ?></td>
                <td><?php echo $temp_table['cpleads']; ?></td>
                <td><?php echo $temp_table['cpreach']; ?></td>
                <td><?php echo $temp_table['adRecall']; ?></td>
                <td><?php echo $temp_table['session']; ?></td>
              </tr>
        <?php
            } elseif ($platform_name == 'Native') {
        ?>
              <tr>
                <td><?php echo $temp_table['date']; ?></td>
                <td><?php echo $temp_table['cost']; ?></td>
                <td><?php echo $temp_table['impression']; ?></td>
                <td><?php echo $temp_table['clicks']; ?></td>
                <td><?php echo $temp_table['ctr']; ?></td>
                <td><?php echo $temp_table['cpc']; ?></td>
                <td><?php echo $temp_table['cpm']; ?></td>
                <td><?php echo $temp_table['session']; ?></td>
              </tr>
        <?php
            }
            elseif ($platform_name == 'YouTube' OR $platform_name == 'Video Network') {
        ?>
            <tr>
                <td><?php echo $temp_table['date']; ?></td>
                <td><?php echo $temp_table['device']; ?></td>
                <td><?php echo $temp_table['targeting']; ?></td>
                <td><?php echo $temp_table['cost']; ?></td>
                <td><?php echo $temp_table['impression']; ?></td>
                <td><?php echo $temp_table['clicks']; ?></td>
                <td><?php echo $temp_table['views_video']; ?></td>
                <td><?php echo $temp_table['view_rates']; ?></td>
                <td><?php echo $temp_table['ctr']; ?></td>
                <td><?php echo $temp_table['cpc']; ?></td>
                <td><?php echo $temp_table['cpm']; ?></td>
                <td><?php echo $temp_table['cpv']; ?></td>
                <td><?php echo $temp_table['session']; ?></td>
              </tr>
        <?php
            }
            elseif ($platform_name == 'CPI') {
        ?>
            <tr>
                <td><?php echo $temp_table['date']; ?></td>
                <td><?php echo $temp_table['device']; ?></td>
                <td><?php echo $temp_table['targeting']; ?></td>
                <td><?php echo $temp_table['cost']; ?></td>
                <td><?php echo $temp_table['impression']; ?></td>
                <td><?php echo $temp_table['clicks']; ?></td>
                <td><?php echo $temp_table['install']; ?></td>
                <td><?php echo $temp_table['install_rate']; ?></td>
                <td><?php echo $temp_table['ctr']; ?></td>
                <td><?php echo $temp_table['cpi']; ?></td>
                <td><?php echo $temp_table['session']; ?></td>
            </tr>
        <?php
            }
            elseif ($platform_name == 'Publisher') {
        ?>
            <tr>
                <td><?php echo $temp_table['date']; ?></td>
                <td><?php echo $temp_table['cost']; ?></td>
                <td><?php echo $temp_table['ad_request']; ?></td>
                <td><?php echo $temp_table['impression']; ?></td>
                <td><?php echo $temp_table['fill_rate']; ?></td>
                <td><?php echo $temp_table['clicks']; ?></td>
                <td><?php echo $temp_table['ctr']; ?></td>
                <td><?php echo $temp_table['cpm']; ?></td>
                <td><?php echo $temp_table['cpc']; ?></td>
                <td><?php echo $temp_table['session']; ?></td>
            </tr>
        <?php
            }
            elseif ($platform_name == 'SEM') {
        ?>
            <tr>
              <td><?php echo $temp_table['date']; ?></td>
              <td><?php echo $temp_table['device']; ?></td>
              <td><?php echo $temp_table['targeting']; ?></td>
              <td><?php echo $temp_table['cost']; ?></td>
              <td><?php echo $temp_table['impression']; ?></td>
              <td><?php echo $temp_table['clicks']; ?></td>
              <td><?php echo $temp_table['conversions']; ?></td>
              <td><?php echo $temp_table['cpc']; ?></td>
              <td><?php echo $temp_table['cpm']; ?></td>
              <td><?php echo $temp_table['cpv']; ?></td>
              <td><?php echo $temp_table['cpa']; ?></td>
              <td><?php echo $temp_table['cvr']; ?></td>
              <td><?php echo $temp_table['session']; ?></td>
            </tr>
        <?php
            }
            elseif ($platform_name == 'Tiktok') {
        ?>
            <tr>
              <td><?php echo $temp_table['date']; ?></td>
              <td><?php echo $temp_table['cost']; ?></td>
              <td><?php echo $temp_table['impression']; ?></td>
              <td><?php echo $temp_table['clicks']; ?></td>
              <td><?php echo $temp_table['ctr']; ?></td>
              <td><?php echo $temp_table['engagement']; ?></td>
              <td><?php echo $temp_table['conversions']; ?></td>
              <td><?php echo $temp_table['reach']; ?></td>
              <td><?php echo $temp_table['followers']; ?></td>
              <td><?php echo $temp_table['viewSecond']; ?></td>
              <td><?php echo $temp_table['viewSecondRate']; ?></td>
              <td><?php echo $temp_table['videoLikes']; ?></td>
              <td><?php echo $temp_table['comment']; ?></td>
              <td><?php echo $temp_table['shares']; ?></td>
              <td><?php echo $temp_table['profileVisits']; ?></td>
              <td><?php echo $temp_table['session']; ?></td>
            </tr>
        <?php
            }
          }
        ?>
    </tbody>

</table>

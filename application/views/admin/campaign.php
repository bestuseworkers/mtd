<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Microad Trading Desk
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-tag"></i> Campaign & Brand</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- START MESSAGE -->
            <?php //FOR MESSAGE INPUT DATA
            if (!empty($message) && $valid == "sukses") {
                ?>
                <div style="padding : 10px;">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <i class="icon fa fa-check"></i><strong>SUCCESS !</strong>
                        <?php echo $message; ?>
                    </div>
                </div>
            <?php
                }
                elseif (!empty($message) && $valid == "gagal") {
            ?>
                <div style="padding : 10px;">
                    <div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <i class="icon fa fa-warning"></i><strong>FAILED !</strong>
                        <?php echo $message; ?>
                    </div>
                </div>
            <?php
                }
                elseif (!empty($message) && $valid == "kosong") {
            ?>
                <div style="padding : 10px;">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <i class="icon fa fa-ban"></i><strong>WARNING !</strong>
                        <?php echo $message; ?>
                    </div>
                </div>
            <?php
                }//END FOR INPUT DATA

                //START FOR DELETE DATA
                elseif (!empty($message) && $valid == "dihapus") {
            ?>
                <div style="padding : 10px;">
                    <div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <i class="icon fa fa-ban"></i><strong>SUCCESS !</strong>
                        <?php echo $message; ?>
                    </div>
                </div>
            <?php
                }//END FOR DELETE DATA

                //START FOR EDIT DATA
                elseif (!empty($message) && $valid == "sukses_edit") {
             ?>
                <div style="padding : 10px;">
                    <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <i class="icon fa fa-check"></i><strong>SUCCESS !</strong>
                        <?php echo $message; ?>
                    </div>
                </div>
             <?php
                }
                elseif (!empty($message) && $valid == "gagal_edit") {
             ?>
                <div style="padding : 10px;">
                    <div class="alert alert-warning alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <i class="icon fa fa-warning"></i><strong>FAILED !</strong>
                        <?php echo $message; ?>
                    </div>
                </div>
             <?php
                }//END FOR EDIT DATA
             ?>
            <!-- END MESSAGE -->

            <!-- left column -->
            <div class="col-md-12">
              <!-- Start Table Agency -->
              <div class="box">

                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Data Brand</h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <div style="padding-bottom:10px">
                      <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#add_brand">
                        Add
                      </button>
                      <button id="brand_edit" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#edit_brand">
                        Edit
                      </button>
                      <button id='dlt_brand' type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#delete_brand">
                        Delete
                      </button>
                    </div>
                    <table id="table_brand" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th>ID Brand</th>
                          <th>Brand Name</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          // echo '<pre/>'; print_r($table_brand); die();//DEBUG
                          foreach ($table_brand as $key => $brand) {
                        ?>
                        <tr>
                          <td><?php echo $brand['id_brand']; ?></td>
                          <td><?php echo $brand['brand_name']; ?></td>
                        </tr>
                        <?php
                          }
                        ?>
                      </tbody>
                    </table>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->

                <!-- Hidden Form -->
                  <!-- Modal Add -->
                  <div class="modal fade" id="add_brand" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                      <form class="form-horizontal" action="<?php echo site_url('admin/campaign/insert_brand'); ?>" method="post">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel">Add Brand</h4>
                        </div>
                        <div class="modal-body">
                            <div class="box-body">
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Brand Name</label>
                                <div class="col-xs-4">
                                  <input name="brand_name" type="text" class="form-control" placeholder="Brand Name">
                                </div>
                              </div>
                            </div><!-- /.box-body -->
                        </div>
                        <div class="modal-footer">
                          <button id="send_edit" type="submit" class="btn-success btn">Save changes</button>
                        </div>
                      </form>
                      </div>
                    </div>
                  </div>
                  <!-- Modal Edit -->
                  <div class="modal fade" id="edit_brand" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                      <form class="form-horizontal" action="<?php echo site_url('admin/campaign/edit_brand'); ?>" method="post">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel">Edit Campaign</h4>
                        </div>
                        <div class="modal-body">
                            <div class="box-body">
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Brand Name</label>
                                <div class="col-xs-4">
                                  <input id="id_brand" name="id_brand" type="hidden" class="form-control" placeholder="ID Brand">
                                  <input id="brand_nm" name="brand_name" type="text" class="form-control" placeholder="Brand Name">
                                </div>
                              </div>
                            </div><!-- /.box-body -->
                        </div>
                        <div class="modal-footer">
                          <button id="send_edit" type="submit" class="btn-success btn">Save changes</button>
                        </div>
                      </form>
                      </div>
                    </div>
                  </div>
                  <!-- Modal Delete -->
                  <div class="modal fade" id="delete_brand" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel">Delete Brand</h4>
                        </div>
                        <div class="modal-body">
                            <div class="box-body">
                              <div class="form-group">
                                <label for="inputEmail3">Do You Want Delete This Data ?</label>
                              </div>
                            </div><!-- /.box-body -->
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
                          <button id="brand_dlt" type="submit" data-dismiss="modal" class="btn btn-success">Yes</button>
                        </div>
                      </div>
                    </div>
                  </div>
                <!-- Hidden Form -->
              </div><!-- End Table Agency -->
            </div><!--/.col (left) -->

            <!-- Right Coloumn -->
            <div class="col-md-12">
              <!-- Start Table Agency -->
              <div class="box">

                <div class="box">
                  <div class="box-header">
                    <h3 class="box-title">Data Campaign</h3>
                  </div><!-- /.box-header -->
                  <div class="box-body">
                    <div style="padding-bottom:10px">
                      <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#add_campaign">
                        Add
                      </button>
                      <button id="edit_cmpgn" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#edit_campaign">
                        Edit
                      </button>
                      <button id='campaign_dlt' type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#delete_campaign">
                        Delete
                      </button>
                    </div>
                    <table id="table_campaign" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th>ID Campaign</th>
                          <th>ID Brand</th>
                          <th>Brand Name</th>
                          <th>Campaign Name</th>
                          <th>ID Agency</th>
                          <th>Agency</th>
                          <th>ID Platform</th>
                          <th>Platform</th>
                          <th>Budget in $</th>
                          <th>User ID</th>
                          <th>Client</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          // echo '<pre/>'; print_r($table_campaign); die();//DEBUG
                          foreach ($table_campaign as $key => $campaign) {
                        ?>
                          <tr>
                            <td><?php echo $campaign['id_campaign']; ?></td>
                            <td><?php echo $campaign['id_brand']; ?></td>
                            <td><?php echo $campaign['brand_name']; ?></td>
                            <td><?php echo $campaign['name']; ?></td>
                            <td><?php echo $campaign['id_agency']; ?></td>
                            <td><?php echo $campaign['agency_name']; ?></td>
                            <td><?php echo $campaign['id_platform']; ?></td>
                            <td><?php echo $campaign['platform']; ?></td>
                            <td><?php echo "USD ".number_format($campaign['budget'], 0, ",", "."); ?></td>
                            <td><?php echo $campaign['u_id']; ?></td>
                            <td><?php echo $campaign['client']; ?></td>
                          </tr>
                        <?php
                          }
                        ?>
                      </tbody>
                    </table>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->

                <!-- Hidden Form -->
                  <!-- Modal Add -->
                  <div class="modal fade" id="add_campaign" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                      <form class="form-horizontal" action="<?php echo site_url('admin/campaign/insert_campaign'); ?>" method="post">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel">Add Campaign</h4>
                        </div>
                        <div class="modal-body">
                            <div class="box-body">
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Brand</label>
                                <select name="brand" class="form-control select2" style="width: 30%;">
                                  <option selected="selected"></option>
                                  <?php
                                    foreach ($table_brand as $key => $brand) {
                                  ?>
                                    <option value="<?php echo $brand['id_brand'];?>"><?php echo $brand['brand_name'];?></option>
                                  <?php
                                    }
                                  ?>
                                </select>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Campagin Name</label>
                                <div class="col-xs-4">
                                  <input name="cmpgn_name" type="text" class="form-control" placeholder="Campagin Name">
                                </div>
                              </div>
                              <div id="agency" class="form-group">
                                <label class="col-sm-3 control-label">Agency Name</label>
                                <select name="agency" class="form-control select2" style="width: 30%;">
                                  <option selected="selected"></option>
                                  <?php
                                    foreach ($select_agency as $key => $agency) {
                                  ?>
                                    <option value="<?php echo $agency['id_agency'];?>"><?php echo $agency['agency_name'];?></option>
                                  <?php
                                    }
                                  ?>
                                </select>
                              </div><!-- /.form-group -->
                              <div id="platform" class="form-group">
                                <label class="col-sm-3 control-label">Platform</label>
                                <select name="platform" class="form-control select2" style="width: 30%;">
                                  <option selected="selected"></option>
                                  <?php
                                    foreach ($select_platform as $key => $platform) {
                                  ?>
                                    <option value="<?php echo $platform['id_platform'];?>">
                                      <?php echo $platform['platform'];?>
                                    </option>
                                  <?php
                                    }
                                  ?>
                                </select>
                              </div><!-- /.form-group -->
                              <div id="budget" class="form-group">
                                <label class="col-sm-3 control-label">Budget</label>
                                <div class="col-xs-4">
                                  <input name="budget" type="text" class="form-control" placeholder="Budget in $">
                                </div>
                              </div>
                              <div id="client" class="form-group">
                                <label class="col-sm-3 control-label">Client</label>
                                <select name="client" class="form-control select2" style="width: 30%;">
                                  <option selected="selected"></option>
                                  <?php
                                    foreach ($select_client as $key => $client) {
                                  ?>
                                  <option value="<?php echo $client['u_id'];?>"><?php echo $client['nama'];?></option>
                                  <?php
                                    }
                                  ?>
                                </select>
                              </div><!-- /.form-group -->
                            </div><!-- /.box-body -->
                        </div>
                        <div class="modal-footer">
                          <button id="send_edit" type="submit" class="btn-success btn">Save changes</button>
                        </div>
                      </form>
                      </div>
                    </div>
                  </div>
                  <!-- Modal Edit -->
                  <div class="modal fade" id="edit_campaign" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                      <form class="form-horizontal" action="<?php echo site_url('admin/campaign/edit_campaign'); ?>" method="post">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel">Edit Campaign</h4>
                        </div>
                        <div class="modal-body">
                            <div class="box-body">
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Brand</label>
                                <select name="brand" class="form-control select2" style="width: 30%;">
                                  <option selected="selected"></option>
                                  <?php
                                    foreach ($table_brand as $key => $brand) {
                                  ?>
                                  <option value="<?php echo $brand['id_brand'];?>"><?php echo $brand['brand_name'];?></option>
                                  <?php
                                    }
                                  ?>
                                </select>
                                <label style="color:red; font-size : 10px;">Choose IF You Wanna CHANGE Brand</label>
                              </div>
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Campagin Name</label>
                                <div class="col-xs-4">
                                  <input id="id_campaign" name="id_campaign" type="hidden" class="form-control" placeholder="Campagin ID">
                                  <input id="name" name="name" type="text" class="form-control" placeholder="Campagin Name">
                                  <input id="id_brand_old" name="id_brand_old" type="hidden" class="form-control" placeholder="Agency ID">
                                  <input id="id_agency_old" name="id_agency_old" type="hidden" class="form-control" placeholder="Agency ID">
                                  <input id="id_platform_old" name="id_platform_old" type="hidden" class="form-control" placeholder="Platform ID">
                                  <input id="u_id" name="u_id" type="hidden" class="form-control" placeholder="User ID">
                                </div>
                              </div>
                              <div id="agency" class="form-group">
                                <label class="col-sm-3 control-label">Agency Name</label>
                                <select name="agency" class="form-control select2" style="width: 30%;">
                                  <option selected="selected"></option>
                                  <?php
                                    foreach ($select_agency as $key => $agency) {
                                  ?>
                                  <option value="<?php echo $agency['id_agency'];?>"><?php echo $agency['agency_name'];?></option>
                                  <?php
                                    }
                                  ?>
                                </select>
                                <label style="color:red; font-size : 10px;">Choose IF You Wanna CHANGE Agency</label>
                              </div><!-- /.form-group -->
                              <div id="platform" class="form-group">
                                <label class="col-sm-3 control-label">Platform</label>
                                <select name="platform" class="form-control select2" style="width: 30%;">
                                  <option selected="selected"></option>
                                  <?php
                                    foreach ($select_platform as $key => $platform) {
                                  ?>
                                  <option value="<?php echo $platform['id_platform'];?>"><?php echo $platform['platform'];?></option>
                                  <?php
                                    }
                                  ?>
                                </select>
                                <label style="color:red; font-size : 10px;">Choose IF You Wanna CHANGE Platform</label>
                              </div><!-- /.form-group -->
                              <div class="form-group">
                                <label class="col-sm-3 control-label">Budget</label>
                                <div class="col-xs-4">
                                  <input id="budget_edt" name="budget_edt" type="text" class="form-control" placeholder="Budget">
                                </div>
                              </div>
                              <div id="client" class="form-group">
                                <label class="col-sm-3 control-label">Client</label>
                                <select name="client" class="form-control select2" style="width: 30%;">
                                  <option selected="selected"></option>
                                  <?php
                                    foreach ($select_client as $key => $client) {
                                  ?>
                                  <option value="<?php echo $client['u_id'];?>"><?php echo $client['nama'];?></option>
                                  <?php
                                    }
                                  ?>
                                </select>
                                <label style="color:red; font-size : 10px;">Choose IF You Wanna CHANGE Client</label>
                              </div><!-- /.form-group -->
                            </div><!-- /.box-body -->
                        </div>
                        <div class="modal-footer">
                          <button id="send_edit" type="submit" class="btn-success btn">Save changes</button>
                        </div>
                      </form>
                      </div>
                    </div>
                  </div>
                  <!-- Modal Delete -->
                  <div class="modal fade" id="delete_campaign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                          <h4 class="modal-title" id="myModalLabel">Delete Campaign</h4>
                        </div>
                        <div class="modal-body">
                            <div class="box-body">
                              <div class="form-group">
                                <label for="inputEmail3">Do You Want to Delete This Data ?</label>
                              </div>
                            </div><!-- /.box-body -->
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
                          <button id="dlt_campaign" type="submit" data-dismiss="modal" class="btn btn-success">Yes</button>
                        </div>
                      </div>
                    </div>
                  </div>
                <!-- Hidden Form -->
              </div><!-- End Table Agency -->
            </div><!-- End col Right -->
          </div><!-- /.row -->
    </section>
    <!-- End Main content -->
</div><!-- /.content-wrapper -->

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Microad Trading Desk
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-users"></i> User</a></li>
            <li class="active">Add User or Clients</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
              <!-- Start Table Agency -->
              <div class="box">

                <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Data User</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                      <div style="padding-bottom:10px">
                        <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#add_user">
                          Add
                        </button>
                        <button id="edit_user" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#edit_user_module">
                          Edit
                        </button>
                        <button id='agency_dlt' type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#delete_user">
                          Delete
                        </button>
                        <!-- START MESSAGE -->
                            <?php //FOR MESSAGE INPUT DATA
                            if (!empty($message) && $valid == "sukses") {
                                ?>
                                <div style="padding : 10px;">
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <i class="icon fa fa-check"></i><strong>SUCCESS !</strong>
                                        <?php echo $message; ?>
                                    </div>
                                </div>
                            <?php
                                }
                                elseif (!empty($message) && $valid == "gagal") {
                            ?>
                                <div style="padding : 10px;">
                                    <div class="alert alert-warning alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <i class="icon fa fa-warning"></i><strong>FAILED !</strong>
                                        <?php echo $message; ?>
                                    </div>
                                </div>
                            <?php
                                }
                                elseif (!empty($message) && $valid == "kosong") {
                            ?>
                                <div style="padding : 10px;">
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <i class="icon fa fa-ban"></i><strong>WARNING !</strong>
                                        <?php echo $message; ?>
                                    </div>
                                </div>
                            <?php
                                }//END FOR INPUT DATA

                                //START FOR DELETE DATA
                                elseif (!empty($message) && $valid == "dihapus") {
                            ?>
                                <div style="padding : 10px;">
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <i class="icon fa fa-ban"></i><strong>SUCCESS !</strong>
                                        <?php echo $message; ?>
                                    </div>
                                </div>
                            <?php
                                }//END FOR DELETE DATA

                                //START FOR EDIT DATA
                                elseif (!empty($message) && $valid == "sukses_edit") {
                             ?>
                                <div style="padding : 10px;">
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <i class="icon fa fa-check"></i><strong>SUCCESS !</strong>
                                        <?php echo $message; ?>
                                    </div>
                                </div>
                             <?php
                                }
                                elseif (!empty($message) && $valid == "gagal_edit") {
                             ?>
                                <div style="padding : 10px;">
                                    <div class="alert alert-warning alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <i class="icon fa fa-warning"></i><strong>FAILED !</strong>
                                        <?php echo $message; ?>
                                    </div>
                                </div>
                             <?php
                                }//END FOR EDIT DATA
                             ?>
                        <!-- END MESSAGE -->
                      </div>
                      <table id="table_user" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>ID User</th>
                            <th>ID Agency</th>
                            <th>ID Sales</th>
                            <th>Agency Name</th>
                            <th>Sales Name</th>
                            <th>Name</th>
                            <th>User Name</th>
                            <th>Active</th>
                            <th>Role</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                            foreach ($selec_user as $key => $user) {
                          ?>
                          <tr>
                            <td><?php echo $user['u_id'];?></td>
                            <td><?php echo $user['id_agency'];?></td>
                            <td><?php echo $user['id_sales'];?></td>
                            <td><?php echo $user['agency_name'];?></td>
                            <td><?php echo $user['sales_name'];?></td>
                            <td><?php echo $user['nama'];?></td>
                            <td><?php echo $user['u_name'];?></td>
                            <td><?php echo $user['active'];?></td>
                            <td><?php echo $user['role'];?></td>
                          </tr>
                          <?php
                            }
                          ?>
                        </tbody>
                      </table>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->

                  <!-- Hidden Form -->
                    <!-- Modal Add -->
                     <div class="modal fade" id="add_user" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                        <form class="form-horizontal" action="<?php echo site_url('admin/user_clients/insert_user'); ?>" method="post">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Add User</h4>
                          </div>
                          <div class="modal-body">
                              <div class="box-body">
                                <div class="form-group">
                                  <label class="col-sm-3 control-label">Role</label>
                                  <select name="role" onchange="Hide(this.value);" class="form-control select2" style="width: 30%;">
                                    <option selected="selected" value="superadmin">Administrator</option>
                                    <option value="user">User</option>
                                    <option value="client">Client</option>
                                    <option value="publisher">Publisher</option>
                                  </select>
                                </div>
                                
                                <!-- For Client -->
                                <div id="agency" class="form-group">
                                  <label class="col-sm-3 control-label">Agency Name</label>
                                  <select name="agency" class="form-control select2" style="width: 30%;">
                                    <option selected="selected"></option>
                                    <?php
                                      foreach ($select_agency as $key => $agency) {
                                    ?>
                                    <option value="<?php echo $agency['id_agency'];?>"><?php echo $agency['agency_name'];?></option>
                                    <?php
                                      }
                                    ?>
                                  </select>
                                </div>
                                <div id="sales" class="form-group">
                                  <label class="col-sm-3 control-label">Sales Name</label>
                                  <select name="sales" class="form-control select2" style="width: 30%;">
                                    <option selected="selected"></option>
                                    <?php
                                      foreach ($select_sales as $key => $sales) {
                                    ?>
                                    <option value="<?php echo $sales['id_sales'];?>"><?php echo $sales['sales_name'];?></option>
                                    <?php
                                      }
                                    ?>
                                  </select>
                                </div>
                                <!-- End Client -->
                                
                                <!--For Admin-->
                                <div id="non-user" class="form-group">
                                  <label class="col-sm-3 control-label">Name</label>
                                  <div class="col-xs-4">
                                    <input name="name_non" type="text" class="form-control" placeholder="Name">
                                  </div>
                                </div>
                                <!--End Admin-->

                                <!--For Publisher-->
                                <div id="publisher" class="form-group">
                                  <label class="col-sm-3 control-label">Publisher Name</label>
                                  <div class="col-xs-4">
                                    <input name="name_pub" type="text" class="form-control" placeholder="Name">
                                  </div>
                                </div>
                                <!--End Admin-->

                                <!-- For Sales User -->
                                <div id="user" class="form-group">
                                  <label class="col-sm-3 control-label">Name</label>
                                  <select name="name_user" class="form-control select2" style="width: 30%;">
                                    <option selected="selected"></option>
                                    <?php
                                      foreach ($name_sales as $key => $sales) {
                                    ?>
                                    <option value="<?php echo $sales['sales_name'];?>"><?php echo $sales['sales_name'];?></option>
                                    <?php
                                      }
                                    ?>
                                  </select>
                                </div>
                                <!-- End Sales User -->

                                <div class="form-group">
                                  <label class="col-sm-3 control-label">User Name</label>
                                  <div class="col-xs-4">
                                    <input name="usr_name" type="text" class="form-control" placeholder="User Name">
                                  </div>
                                </div>
                                <div id="password" class="form-group">
                                  <label class="col-sm-3 control-label">Password</label>
                                  <div class="col-xs-4">
                                    <input name="pass" type="text" class="form-control" value="blade123" placeholder="Password">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-3 control-label">Active</label>
                                  <select name="active" class="form-control select2" style="width: 30%;">
                                    <option selected="selected" value="Y">Yes</option>
                                    <option value="N">No</option>
                                  </select>
                                </div>
                              </div>
                          </div>
                          <div class="modal-footer">
                            <button id="send_edit" type="submit" class="btn btn-success">Save changes</button>
                          </div>
                        </form>
                        </div>
                      </div>
                    </div>
                    <!-- Modal Edit -->
                     <div class="modal fade" id="edit_user_module" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                        <form class="form-horizontal" action="<?php echo site_url('admin/user_clients/edit_user'); ?>" method="post">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Edit User</h4>
                          </div>
                          <div class="modal-body">
                              <div class="box-body">
                                <div class="form-group">
                                  <label class="col-sm-3 control-label">Role</label>
                                  <select name="role" onchange="Hide(this.value);" id="role" class="form-control select_role" style="width: 30%;">
                                    <option value="superadmin">Administrator</option>
                                    <option value="user">User</option>
                                    <option value="client">Client</option>
                                  </select>
                                </div><!-- /.form-group -->
                                <div id="agency" class="form-group">
                                  <label class="col-sm-3 control-label">Agency Name</label>
                                  <select id="agency" name="agency" class="form-control select_agency" style="width: 30%;">
                                    <option selected="selected"></option>
                                    <?php
                                      foreach ($select_agency as $key => $agency) {
                                    ?>
                                    <option value="<?php echo $agency['id_agency'];?>"><?php echo $agency['agency_name'];?></option>
                                    <?php
                                      }
                                    ?>
                                  </select>
                                  <label style="color:red; font-size : 10px;">Choose IF You Wanna CHANGE Agency</label>
                                </div><!-- /.form-group -->
                                <div id="sales" class="form-group">
                                  <label class="col-sm-3 control-label">Sales Name</label>
                                  <select name="sales" class="form-control select_sales" style="width: 30%;">
                                    <option selected="selected"></option>
                                    <?php
                                      foreach ($select_sales as $key => $sales) {
                                    ?>
                                    <option value="<?php echo $sales['id_sales'];?>"><?php echo $sales['sales_name'];?></option>
                                    <?php
                                      }
                                    ?>
                                  </select>
                                  <label style="color:red; font-size : 10px;">Choose IF You Wanna CHANGE Sales</label>
                                </div><!-- /.form-group -->
                                <div class="form-group">
                                  <label for="inputMngName" class="col-sm-3 control-label">Name</label>
                                  <div class="col-xs-4">
                                    <input id="id_user" name="id_user" type="hidden" class="form-control" placeholder="ID">
                                    <input id="agency_old" name="agency_old" type="hidden" class="form-control" placeholder="Agency">
                                    <input id="sales_old" name="sales_old" type="hidden" class="form-control" placeholder="Sales">
                                    <input id="name" name="name" type="text" class="form-control" placeholder="Name">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="inputCntName" class="col-sm-3 control-label">User Name</label>
                                  <div class="col-xs-4">
                                    <input id="user_name" name="user_name" type="text" class="form-control" placeholder="User Manager">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="inputCntName" class="col-sm-3 control-label">Password</label>
                                  <div class="col-xs-4">
                                    <input id="password" name="password" type="text" class="form-control" placeholder="Password">
                                  </div>
                                  <label style="color:red; font-size : 10px;">Type IF You Wanna CHANGE Password</label>
                                </div>
                                <div class="form-group">
                                  <label class="col-sm-3 control-label">Active</label>
                                  <select name="active" id="active" class="form-control select_active" style="width: 30%;">
                                    <option value="Y">Yes</option>
                                    <option value="N">No</option>
                                  </select>
                                </div><!-- /.form-group -->
                              </div><!-- /.box-body -->
                          </div>
                          <div class="modal-footer">
                            <button id="send_edit" type="submit" class="btn btn-success">Save changes</button>
                          </div>
                        </form>
                        </div>
                      </div>
                    </div>
                  <!-- Modal Delete -->
                    <div class="modal fade" id="delete_user" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Delete Agency</h4>
                          </div>
                          <div class="modal-body">
                              <div class="box-body">
                                <div class="form-group">
                                  <label for="inputEmail3">Do You Want Delete This Data ?</label>
                                </div>
                              </div><!-- /.box-body -->
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
                            <button id="dlt_agency" type="submit" data-dismiss="modal" class="btn btn-success">Yes</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  <!-- Hidden Form -->
              </div><!-- End Table Agency -->

            </div><!--/.col (left) -->
          </div>   <!-- /.row -->
    </section>
    <!-- End Main content -->
</div><!-- /.content-wrapper -->

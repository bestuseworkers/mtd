<style>
    #data_dash .classDataTable { font-size: 5px; }
    #chart  {display: none;}
    #table {display: none;}
    #iframe {display: none;}
    #type_targeting {display: none;}
</style>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Microad Trading Desk
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box-header with-border">
                            <h3 class="box-title"><strong>Select Parameters</strong></h3>
                            <div class="box-tools pull-right">
                                <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                <!--<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                            </div>
                        </div><!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                             <!-- <form action="<?php echo site_url('admin/dashboard_admin/open_dashboard'); ?>" method="post" enctype="multipart/form-data"> -->
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="col-sm-1 control-label" style="padding-top:5px; padding-right:70px;">Brand</label>
                                    <select id="brand" name="brand" class="form-control select_brand" style="width: 33%;" onchange="selectCampaign()">
                                        <option selected="selected"></option>
                                        <?php
                                          foreach ($brand as $key => $brand) {
                                        ?>
                                            <option value="<?php echo $brand['id_brand'];?>"><?php echo $brand['brand_name'];?></option>
                                        <?php
                                          }
                                        ?>
                                    </select>
                                </div><!-- /.form-group -->
                                <div class="form-group">
                                    <label class="col-sm-1 control-label" style="padding-top:5px; padding-right:70px;">Campaign</label>
                                    <select id="campaign" name="campaign" class="form-control select_campaign" style="width: 33%;" onchange="selectPlatform()">
                                      <!-- <option selected="selected">Choose</option> -->
                                    </select>
                                </div><!-- /.form-group -->
                                <div class="form-group">
                                  <label class="col-sm-1 control-label" style="padding-top:5px; padding-right:70px;">Platform</label>
                                    <select id="platform" name="platform" class="form-control select_platform" style="width: 33%;" onchange="selectType()">
                                      <!-- <option selected="selected">Choose</option> -->
                                    </select>
                                </div><!-- /.form-group -->
                                <div id="type_targeting">
                                    <div class="form-group">
                                      <label class="col-sm-1 control-label" style="padding-top:5px; padding-right:70px;color:#dd4b39">Type</label>
                                        <select id="type" name="type" class="form-control select_type" style="width: 33%;" onchange="selectTargeting()">
                                            <!-- <option value="ALL" selected="selected">ALL</option> -->
                                        <?php 
                                            // foreach ($type as $key => $value) {        
                                        ?>
                                            <!-- <option value="<?php echo $value['device'];?>"><?php echo $value['device'];?></option> -->
                                        <?php 
                                            // }
                                        ?>
                                        </select>
                                        <span class="label label-danger">optional</span>
                                    </div><!-- /.form-group -->
                                    <div class="form-group">
                                      <label class="col-sm-1 control-label" style="padding-top:5px; padding-right:70px;color:#dd4b39">Targeting</label>
                                        <select id="targeting" name="targeting" class="form-control select_targeting" style="width: 33%;">
                                        <option selected="selected"></option>
                                        </select>
                                        <span class="label label-danger">optional</span>
                                    </div><!-- /.form-group -->
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-1 control-label" style="padding-top:5px; padding-right:70px;">Period</label>
                                    <div class="input-group">
                                      <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="text" class="form-control" id="reservation" name="date" style="width: 175px;">
                                    </div><!-- /.input group -->
                                </div><!-- /.form-group -->
                                
                            </div>
                            <div class="col-md-6">

                            </div>
                        </div>
                        <div class="box-footer">
                            <button id="search" type="submit" class="btn-success applyBtn btn btn-small pull-right" onclick="showDashboard()">Search <i class="fa fa-search"></i><i class="ic-indicator fa fa-spinner fa-spin" style="display:none"></i></button>
                        </div>
                        <!-- </form> -->
                    </div>
                </div><!-- /.box -->
            </div><!-- /.col 1-->
        </div><!-- /.row -->

        <!-- Info boxes -->
        <div class="row" id="chart">
            <div class="col-md-12">
                <div class="box" id="chart-box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><strong id="head_chart"></strong></h3>
                        <div align="right">
                          <select id="params_1" name="params_1" class="params_1" style="width: 11%;" onchange="line_1()">
                                <option selected="selected">CPC</option>

                            </select>
                            <strong>&</strong>
                            <select id="params_2" name="params_2" class="params_2" style="width: 11%;" onchange="line_2()">
                                <option selected="selected">Impression</option>

                            </select>
                            <button id="download_btn" type="submit" onclick="download()" data-toggle="modal"><i class="fa fa-download"></i></button>
                            <div id="iframe">
                          				<iframe id="ifreport" src="" width="100%" height="420px">
                          					 masukin pdf disini
                          				</iframe>
                          	</div>
                        </div>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            <!--<button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>-->
                        </div>
                    </div><!-- /.box-header -->
                    <!-- Loading (remove the following to stop the loading)-->
                    <!-- end loading -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <p class="text-center">
                                    <strong id="chart_date"></strong>
                                </p>
                                <div class="chart-responsive" id="position_dash">
                                    <!-- Sales Chart Canvas -->
                                    <canvas id="dashboard" height="65"></canvas>
                                </div><!-- /.chart-responsive -->
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- ./box-body -->
                    <div class="box-footer">
                    <div class="row" id="ket_dash">
                      <!-- isinya dari Javascript-->
                    </div><!-- /.row -->
                  </div><!-- /.box-footer -->
                </div><!-- /.box -->
            </div><!-- /.col -->
        </div><!-- /.row -->
        <!--Start Table -->
        <div class="row" id="table">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title"><strong id="head_table"></strong></h3>
                <!-- <h3 class="box-title"><strong>Data Table <?php echo $campaign_nm['name'].'  '.$platform_dt['platform'] ?></strong></h3> -->
              </div><!-- /.box-header -->
              <div class="box-body">
                <div class="form-group" id="for-table">
                      
                </div>
            </div><!-- /.box-body -->
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
    </div><!-- /.row -->
        <!--End Table -->
        <!-- Main row -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

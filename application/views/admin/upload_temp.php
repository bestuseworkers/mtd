<?

?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Microad Trading Desk
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-upload"></i> Upload</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Upload Confirmation Data</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="<?php echo site_url('admin/upload/input'); ?>" method="post" enctype="multipart/form-data">
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-1 control-label">Brand</label>
                        <div class="input-group col-xs-2">
                          <input type="text" class="form-control" placeholder="<?php echo $brand;?>" disabled>
                        </div><!-- /.input group -->
                    </div><!-- /.form-group -->
                    <div class="form-group">
                      <label class="col-sm-1 control-label">Campaign</label>
                        <div class="input-group col-xs-2">
                          <input type="text" class="form-control" placeholder="<?php echo $campaign;?>" disabled>
                          <?php
                            $id = array();
                            foreach ($id_temp as $key => $id_temp) {
                          ?>
                            <input 
                              type="hidden" 
                              name="id_<?php echo $key + 1;?>" 
                              class="form-control" 
                              value="<?php echo $id_temp[0];?>" >
                          <?php
                            }
                          ?>
                        </div><!-- /.input group -->
                    </div><!-- /.form-group -->
                    <div class="form-group">
                      <label class="col-sm-1 control-label">Platform</label>
                        <div class="input-group col-xs-2">
                          <input 
                            id="platform" 
                            type="text" 
                            class="form-control" 
                            placeholder="<?php echo $platform_name;?>" 
                            value="<?php echo $platform_name;?>" 
                            disabled 
                          />
                        </div><!-- /.input group -->
                    </div><!-- /.form-group -->
                    <div class="form-group">
                      <table id="table_campaign" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                          <?php
                            foreach ($th as $key => $th) {
                          ?>
                            <th><?php echo $th; ?></th>
                          <?php
                            }
                          ?>
                          </tr>
                        </thead>
                        <tbody>
                            <?php
                            // echo '<pre/>'; print_r($temp_table); die;
                              foreach ($temp_table as $key => $temp_table) {
                                if ($platform_name == 'Facebook') {
                            ?>
                                  <tr>
                                    <td><?php echo $temp_table['id_tmp_dash']; ?></td>
                                    <td><?php echo $temp_table['date']; ?></td>
                                    <td><?php echo $temp_table['cost']; ?></td>
                                    <td><?php echo $temp_table['impression']; ?></td>
                                    <td><?php echo $temp_table['clicks']; ?></td>
                                    <td><?php echo $temp_table['ctr']; ?></td>
                                    <td><?php echo $temp_table['engagement']; ?></td>
                                    <td><?php echo $temp_table['eng_rate']; ?></td>
                                    <td><?php echo $temp_table['page_likes']; ?></td>
                                    <td><?php echo $temp_table['like_rates']; ?></td>
                                    <td><?php echo $temp_table['views_video']; ?></td>
                                    <td><?php echo $temp_table['view_rates']; ?></td>
                                    <td><?php echo $temp_table['conversions']; ?></td>
                                    <td><?php echo $temp_table['leads']; ?></td>
                                    <td><?php echo $temp_table['reach']; ?></td>
                                    <td><?php echo $temp_table['cvr']; ?></td>
                                    <td><?php echo $temp_table['cpc']; ?></td>
                                    <td><?php echo $temp_table['cpm']; ?></td>
                                    <td><?php echo $temp_table['cpe']; ?></td>
                                    <td><?php echo $temp_table['cpl']; ?></td>
                                    <td><?php echo $temp_table['cpv']; ?></td>
                                    <td><?php echo $temp_table['cpa']; ?></td>
                                    <td><?php echo $temp_table['cpleads']; ?></td>
                                    <td><?php echo $temp_table['cpreach']; ?></td>
                                    <td><?php echo $temp_table['adRecall']; ?></td>
                                    <td><?php echo $temp_table['session']; ?></td>
                                  </tr>
                            <?php
                                }
                                elseif ($platform_name == 'Programmatic') {
                            ?>
                                  <tr>
                                    <td><?php echo $temp_table['id_tmp_dash']; ?></td>
                                    <td><?php echo $temp_table['date']; ?></td>
                                    <td><?php echo $temp_table['device']; ?></td>
                                    <td><?php echo $temp_table['targeting']; ?></td>
                                    <td><?php echo $temp_table['cost']; ?></td>
                                    <td><?php echo $temp_table['impression']; ?></td>
                                    <td><?php echo $temp_table['clicks']; ?></td>
                                    <td><?php echo $temp_table['ctr']; ?></td>
                                    <td><?php echo $temp_table['conversions']; ?></td>
                                    <td><?php echo $temp_table['cvr']; ?></td>
                                    <td><?php echo $temp_table['cpc']; ?></td>
                                    <td><?php echo $temp_table['cpm']; ?></td>
                                    <td><?php echo $temp_table['cpa']; ?></td>
                                    <td><?php echo $temp_table['session']; ?></td>
                                  </tr>
                            <?php
                                }
                                elseif ($platform_name == 'Twitter') {
                            ?>
                                  <tr>
                                    <td><?php echo $temp_table['id_tmp_dash']; ?></td>
                                    <td><?php echo $temp_table['date']; ?></td>
                                    <td><?php echo $temp_table['cost']; ?></td>
                                    <td><?php echo $temp_table['impression']; ?></td>
                                    <td><?php echo $temp_table['clicks']; ?></td>
                                    <td><?php echo $temp_table['ctr']; ?></td>
                                    <td><?php echo $temp_table['engagement']; ?></td>
                                    <td><?php echo $temp_table['eng_rate']; ?></td>
                                    <td><?php echo $temp_table['followers']; ?></td>
                                    <td><?php echo $temp_table['follow_rates']; ?></td>
                                    <td><?php echo $temp_table['views_video']; ?></td>
                                    <td><?php echo $temp_table['view_rates']; ?></td>
                                    <td><?php echo $temp_table['cpc']; ?></td>
                                    <td><?php echo $temp_table['cpm']; ?></td>
                                    <td><?php echo $temp_table['cpl']; ?></td>
                                    <td><?php echo $temp_table['session']; ?></td>
                                  </tr>
                            <?php
                                }
                                elseif ($platform_name == 'Instagram') {
                            ?>
                                  <tr>
                                    <td><?php echo $temp_table['id_tmp_dash']; ?></td>
                                    <td><?php echo $temp_table['date']; ?></td>
                                    <td><?php echo $temp_table['cost']; ?></td>
                                    <td><?php echo $temp_table['impression']; ?></td>
                                    <td><?php echo $temp_table['clicks']; ?></td>
                                    <td><?php echo $temp_table['ctr']; ?></td>
                                    <td><?php echo $temp_table['engagement']; ?></td>
                                    <td><?php echo $temp_table['eng_rate']; ?></td>
                                    <td><?php echo $temp_table['views_video']; ?></td>
                                    <td><?php echo $temp_table['view_rates']; ?></td>
                                    <td><?php echo $temp_table['leads']; ?></td>
                                    <td><?php echo $temp_table['reach']; ?></td>
                                    <td><?php echo $temp_table['cpc']; ?></td>
                                    <td><?php echo $temp_table['cpm']; ?></td>
                                    <td><?php echo $temp_table['cpe']; ?></td>
                                    <td><?php echo $temp_table['cpv']; ?></td>
                                    <td><?php echo $temp_table['cpleads']; ?></td>
                                    <td><?php echo $temp_table['cpreach']; ?></td>
                                    <td><?php echo $temp_table['adRecall']; ?></td>
                                    <td><?php echo $temp_table['session']; ?></td>
                                  </tr>
                            <?php
                                }
                                elseif ($platform_name == 'Native') {
                            ?>
                                  <tr>
                                    <td><?php echo $temp_table['id_tmp_dash']; ?></td>
                                    <td><?php echo $temp_table['date']; ?></td>
                                    <td><?php echo $temp_table['cost']; ?></td>
                                    <td><?php echo $temp_table['impression']; ?></td>
                                    <td><?php echo $temp_table['clicks']; ?></td>
                                    <td><?php echo $temp_table['ctr']; ?></td>
                                    <td><?php echo $temp_table['cpc']; ?></td>
                                    <td><?php echo $temp_table['cpm']; ?></td>
                                    <td><?php echo $temp_table['session']; ?></td>
                                  </tr>
                            <?php
                                }
                                elseif ($platform_name == 'YouTube' or $platform_name == 'Video Network') {
                            ?>
                                  <tr>
                                    <td><?php echo $temp_table['id_tmp_dash']; ?></td>
                                    <td><?php echo $temp_table['date']; ?></td>
                                    <td><?php echo $temp_table['device']; ?></td>
                                    <td><?php echo $temp_table['targeting']; ?></td>
                                    <td><?php echo $temp_table['cost']; ?></td>
                                    <td><?php echo $temp_table['impression']; ?></td>
                                    <td><?php echo $temp_table['clicks']; ?></td>
                                    <td><?php echo $temp_table['views_video']; ?></td>
                                    <td><?php echo $temp_table['view_rates']; ?></td>
                                    <td><?php echo $temp_table['ctr']; ?></td>
                                    <td><?php echo $temp_table['cpc']; ?></td>
                                    <td><?php echo $temp_table['cpm']; ?></td>
                                    <td><?php echo $temp_table['cpv']; ?></td>
                                    <td><?php echo $temp_table['session']; ?></td>
                                  </tr>
                            <?php
                                }
                                elseif ($platform_name == 'CPI') {
                            ?>
                                <tr>
                                    <td><?php echo $temp_table['id_tmp_dash']; ?></td>
                                    <td><?php echo $temp_table['date']; ?></td>
                                    <td><?php echo $temp_table['device']; ?></td>
                                    <td><?php echo $temp_table['targeting']; ?></td>
                                    <td><?php echo $temp_table['cost']; ?></td>
                                    <td><?php echo $temp_table['impression']; ?></td>
                                    <td><?php echo $temp_table['clicks']; ?></td>
                                    <td><?php echo $temp_table['install']; ?></td>
                                    <td><?php echo $temp_table['install_rate']; ?></td>
                                    <td><?php echo $temp_table['ctr']; ?></td>
                                    <td><?php echo $temp_table['cpi']; ?></td>
                                    <td><?php echo $temp_table['session']; ?></td>
                                </tr>
                            <?php
                                }
                                elseif ($platform_name == 'Publisher') {
                            ?>
                                <tr>
                                    <td><?php echo $temp_table['id_tmp_dash']; ?></td>
                                    <td><?php echo $temp_table['date']; ?></td>
                                    <td><?php echo $temp_table['revenue']; ?></td>
                                    <td><?php echo $temp_table['ad_request']; ?></td>
                                    <td><?php echo $temp_table['impression']; ?></td>
                                    <td><?php echo $temp_table['fill_rate']; ?></td>
                                    <td><?php echo $temp_table['clicks']; ?></td>
                                    <td><?php echo $temp_table['ctr']; ?></td>
                                    <td><?php echo $temp_table['cpm']; ?></td>
                                    <td><?php echo $temp_table['cpc']; ?></td>
                                    <td><?php echo $temp_table['session']; ?></td>
                                </tr>
                            <?php
                                }
                                elseif ($platform_name == 'SEM') {
                            ?>
                                <tr>
                                    <td><?php echo $temp_table['id_tmp_dash']; ?></td>
                                    <td><?php echo $temp_table['date']; ?></td>
                                    <td><?php echo $temp_table['device']; ?></td>
                                    <td><?php echo $temp_table['targeting']; ?></td>
                                    <td><?php echo $temp_table['cost']; ?></td>
                                    <td><?php echo $temp_table['impression']; ?></td>
                                    <td><?php echo $temp_table['clicks']; ?></td>
                                    <td><?php echo $temp_table['conversions']; ?></td>
                                    <td><?php echo $temp_table['cpm']; ?></td>
                                    <td><?php echo $temp_table['cpc']; ?></td>
                                    <td><?php echo $temp_table['cpa']; ?></td>
                                    <td><?php echo $temp_table['cvr']; ?></td>
                                    <td><?php echo $temp_table['session']; ?></td>
                                </tr>
                            <?php
                                }
                                elseif ($platform_name == 'Tiktok') {
                            ?>
                                <tr>
                                    <td><?php echo $temp_table['date']; ?></td>
                                    <td><?php echo $temp_table['cost']; ?></td>
                                    <td><?php echo $temp_table['impression']; ?></td>
                                    <td><?php echo $temp_table['clicks']; ?></td>
                                    <td><?php echo $temp_table['ctr']; ?></td>
                                    <td><?php echo $temp_table['engagement']; ?></td>
                                    <td><?php echo $temp_table['conversions']; ?></td>
                                    <td><?php echo $temp_table['reach']; ?></td>
                                    <td><?php echo $temp_table['followers']; ?></td>
                                    <td><?php echo $temp_table['viewSecond']; ?></td>
                                    <td><?php echo $temp_table['viewSecondRate']; ?></td>
                                    <td><?php echo $temp_table['videoLikes']; ?></td>
                                    <td><?php echo $temp_table['comment']; ?></td>
                                    <td><?php echo $temp_table['shares']; ?></td>
                                    <td><?php echo $temp_table['profileVisits']; ?></td>
                                    <td><?php echo $temp_table['session']; ?></td>
                                </tr>
                            <?php
                                    }
                                }
                            ?>
                        </tbody>
                      </table>
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" name="submitted" value="cancle" class="btn btn-danger">Cancel</button>
                    <button type="submit" name="submitted" value="submit" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->
        </div>   <!-- /.row -->
    </section>
    <!-- End Main content -->
</div><!-- /.content-wrapper -->

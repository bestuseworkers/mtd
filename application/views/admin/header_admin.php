<header class="main-header">

    <!-- Logo -->

    <a href="#" class="logo">
        <img style="vertical-align: baseline; height:48px; width:145px>" src="<?php echo base_url('assets/media/logo.png') ?>">
    </a>

    <!-- Header Navbar: style can be found in header.less -->

    <nav class="navbar navbar-static-top" role="navigation">

        <!-- Sidebar toggle button-->

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">

            <span class="sr-only">Hide navigation</span>

        </a>

        <!-- Navbar Right Menu -->

        <div class="navbar-custom-menu">

            <ul class="nav navbar-nav">

                <!-- User Account: style can be found in dropdown.less -->

                <li class="dropdown user user-menu">

                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                        <span class="hidden-xs"><?php echo $this->session->userdata('nama'); ?></span>

                    </a>

                    <ul class="dropdown-menu">

                        <!-- User image -->

                        <li class="user-header">

                            <p>

                                <?php echo $this->session->userdata('nama'); ?>

                                <br/>

                                <?php echo strtoupper($this->session->userdata('role')); ?>

                                <small>MicroAD Blade Indonesia</small>

                            </p>

                        </li>

                        <!-- Menu Footer-->

                        <li class="user-footer">

                            <div class="pull-right">

                                <a href="<?php echo site_url('dashboard/logout'); ?>" class="btn-success applyBtn btn btn-small pull-right">Sign out</a>

                            </div>

                        </li>

                    </ul>

                </li>

            </ul>

        </div>

    </nav>

</header>


<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
                <a href="<?php echo site_url('admin/dashboard_admin'); ?>">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview">
                <a href="<?php echo site_url('admin/campaign'); ?>">
                    <i class="fa fa-tag"></i> <span>Campaign & Brand</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i> <span>User</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('admin/user_clients'); ?>"><i class="fa fa-circle-o"></i>User And Client</a></li>
                    <li><a href="<?php echo site_url('admin/agency_sales'); ?>"><i class="fa fa-circle-o"></i>Agency And Sales</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="<?php echo site_url('admin/upload'); ?>">
                    <i class="fa fa-upload"></i> <span>Upload</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Microad Trading Desk
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-users"></i> User</a></li>
            <li class="active">Add Agency Or Sales</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- Start Table Agency -->
              <div class="box">

                <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Data Agency</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                      <div style="padding-bottom:10px">
                        <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#add_Agency">
                          Add
                        </button>
                        <button id="edit" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#edit_Agency">
                          Edit
                        </button>
                        <button id='agency_dlt' type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#agency">
                          Delete
                        </button>
                        <!-- START MESSAGE -->
                            <?php //FOR MESSAGE INPUT DATA
                            if (!empty($message) && $valid == "sukses") {
                                ?>
                                <div style="padding : 10px;">
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <i class="icon fa fa-check"></i><strong>SUCCESS !</strong>
                                        <?php echo $message; ?>
                                    </div>
                                </div>
                            <?php
                                }
                                elseif (!empty($message) && $valid == "gagal") {
                            ?>
                                <div style="padding : 10px;">
                                    <div class="alert alert-warning alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <i class="icon fa fa-warning"></i><strong>FAILED !</strong>
                                        <?php echo $message; ?>
                                    </div>
                                </div>
                            <?php
                                }
                                elseif (!empty($message) && $valid == "kosong") {
                            ?>
                                <div style="padding : 10px;">
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <i class="icon fa fa-ban"></i><strong>WARNING !</strong>
                                        <?php echo $message; ?>
                                    </div>
                                </div>
                            <?php
                                }//END FOR INPUT DATA

                                //START FOR DELETE DATA
                                elseif (!empty($message) && $valid == "dihapus") {
                            ?>
                                <div style="padding : 10px;">
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <i class="icon fa fa-ban"></i><strong>SUCCESS !</strong>
                                        <?php echo $message; ?>
                                    </div>
                                </div>
                            <?php
                                }//END FOR DELETE DATA

                                //START FOR EDIT DATA
                                elseif (!empty($message) && $valid == "sukses_edit") {
                             ?>
                                <div style="padding : 10px;">
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <i class="icon fa fa-check"></i><strong>SUCCESS !</strong>
                                        <?php echo $message; ?>
                                    </div>
                                </div>
                             <?php
                                }
                                elseif (!empty($message) && $valid == "gagal_edit") {
                             ?>
                                <div style="padding : 10px;">
                                    <div class="alert alert-warning alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <i class="icon fa fa-warning"></i><strong>FAILED !</strong>
                                        <?php echo $message; ?>
                                    </div>
                                </div>
                             <?php
                                }//END FOR EDIT DATA
                             ?>
                        <!-- END MESSAGE -->
                      </div>
                      <table id="table_agency" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>ID Agency</th>
                            <th>PT</th>
                            <th>Agency Name</th>
                            <th>Email</th>
                            <th>Sales</th>
                            <th>ID Sales</th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                         // echo '<pre/>'; print_r($table_agency); die();//Debug
                         foreach ($table_agency as $key => $agency) {
                        ?>
                          <tr>
                            <td><?php echo $agency['id_agency']?></td>
                            <td><?php echo $agency['pt']?></td>
                            <td><?php echo $agency['agency_name']?></td>
                            <td><?php echo $agency['email']?></td>
                            <td><?php echo $agency['sales_name']?></td>
                            <td><?php echo $agency['id_sales']?></td>
                          </tr>
                        <?php
                         }
                        ?>
                        </tbody>
                      </table>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->

                  <!-- Hidden Form -->
                    <!-- Modal Add -->
                    <div class="modal fade" id="add_Agency" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                        <form class="form-horizontal" action="<?php echo site_url('admin/agency_sales/input_agency'); ?>" method="post">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Add Agency</h4>
                          </div>
                          <div class="modal-body">
                              <div class="box-body">
                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-3 control-label">PT</label>
                                  <div class="col-xs-4">
                                    <input id="pt" name="pt" type="text" class="form-control" placeholder="PT">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-3 control-label">Agency Name</label>
                                  <div class="col-xs-4">
                                    <input id="agency_name" name="agency_name" type="text" class="form-control" placeholder="Agency Name">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
                                  <div class="col-xs-4">
                                    <input id="email" name="email" type="text" class="form-control" placeholder="Email">
                                  </div>
                                </div>
                                <div id="sales" class="form-group">
                                  <label class="col-sm-3 control-label">Sales Name</label>
                                  <select id="sales" name="sales" class="form-control select_sales" style="width: 30%;">
                                    <option selected="selected"></option>
                                    <?php
                                      foreach ($select_sales as $key => $sales) {
                                    ?>
                                    <option value="<?php echo $sales['id_sales'];?>"><?php echo $sales['sales_name'];?></option>
                                    <?php
                                      }
                                    ?>
                                  </select>
                                </div><!-- /.form-group -->
                              </div><!-- /.box-body -->
                          </div>
                          <div class="modal-footer">
                            <button type="submit" class="btn btn-success">Save changes</button>
                          </div>
                        </form>
                        </div>
                      </div>
                    </div>
                    <!-- Modal Edit -->
                    <div class="modal fade" id="edit_Agency" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                        <form class="form-horizontal" action="<?php echo site_url('admin/agency_sales/edit_agency'); ?>" method="post">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Edit Agency Agency</h4>
                          </div>
                          <div class="modal-body">
                              <div class="box-body">
                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-3 control-label">PT</label>
                                  <div class="col-xs-4">
                                    <input id="pt_edit" name="pt" type="text" class="form-control" placeholder="PT">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-3 control-label">Agency Name</label>
                                  <div class="col-xs-4">
                                    <input id="name" name="name" type="text" class="form-control" placeholder="Agency Name">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label for="inputEmail3" class="col-sm-3 control-label">Email</label>
                                  <div class="col-xs-4">
                                    <input id="email_edit" name="email_edit" type="text" class="form-control" placeholder="Email">
                                    <input id="agency_id" name="agency_id" type="hidden" class="form-control" placeholder="Agency ID">
                                    <input id="sales_old" name="sales_old" type="hidden" class="form-control" placeholder="Sales Old">
                                  </div>
                                </div>
                                <div id="sales_edit" class="form-group">
                                  <label class="col-sm-3 control-label">Sales Name</label>
                                  <select id="sales_edit" name="sales_edit" class="form-control sales_edit" style="width: 30%;">
                                    <option selected="selected"></option>
                                    <?php 
                                      foreach ($select_sales as $key => $sales) {
                                    ?>
                                    <option value="<?php echo $sales['id_sales'];?>"><?php echo $sales['sales_name'];?></option>
                                    <?php
                                      }
                                    ?>
                                  </select>
                                  <label style="color:red; font-size : 10px;">Choose IF You Wanna CHANGE Sales</label>
                                </div><!-- /.form-group -->
                              </div><!-- /.box-body -->
                          </div>
                          <div class="modal-footer">
                            <button type="submit" class="btn btn-success">Save changes</button>
                          </div>
                        </form>
                        </div>
                      </div>
                    </div>
                  <!-- Modal Delete -->
                    <div class="modal fade" id="agency" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                      <div class="modal-dialog">
                        <div class="modal-content">
                          <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <h4 class="modal-title" id="myModalLabel">Delete Agency</h4>
                          </div>
                          <div class="modal-body">
                              <div class="box-body">
                                <div class="form-group">
                                  <label for="inputEmail3">Do You Want Delete This Data ?</label>
                                </div>
                              </div><!-- /.box-body -->
                          </div>
                          <div class="modal-footer">
                            <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
                            <button id="dlt_agency" type="submit" data-dismiss="modal" class="btn btn-success">Yes</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  <!-- Hidden Form -->
              </div><!-- End Table Agency -->

            </div><!--/.col (left) -->

            <!-- right column -->
            <div class="col-md-6">
              <!-- Start Table Agency -->
              <div class="box">

                <div class="box">
                    <div class="box-header">
                      <h3 class="box-title">Data Sales</h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <div style="padding-bottom:10px">
                            <!-- Start Button -->
                            <button type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#add_sales">
                              Add
                            </button>
                            <button id="edit_sales_js" type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#edit_sales">
                              Edit
                            </button>
                            <button id='agency_dlt' type="button" class="btn btn-success btn-xs" data-toggle="modal" data-target="#dlt_sales">
                              Delete
                            </button>
                            <!-- End Button -->
                        </div>
                        <!-- START MESSAGE -->
                            <?php //FOR MESSAGE INPUT DATA
                            if (!empty($message) && $valid == "sales_sukses") {
                                ?>
                                <div style="padding : 10px;">
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <i class="icon fa fa-check"></i><strong>SUCCESS !</strong>
                                        <?php echo $message; ?>
                                    </div>
                                </div>
                            <?php
                                }
                                elseif (!empty($message) && $valid == "sales_gagal") {
                            ?>
                                <div style="padding : 10px;">
                                    <div class="alert alert-warning alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <i class="icon fa fa-warning"></i><strong>FAILED !</strong>
                                        <?php echo $message; ?>
                                    </div>
                                </div>
                            <?php
                                }
                                elseif (!empty($message) && $valid == "sales_kosong") {
                            ?>
                                <div style="padding : 10px;">
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <i class="icon fa fa-ban"></i><strong>WARNING !</strong>
                                        <?php echo $message; ?>
                                    </div>
                                </div>
                            <?php
                                }//END FOR INPUT DATA

                                //START FOR DELETE DATA
                                elseif (!empty($message) && $valid == "sales_dihapus") {
                            ?>
                                <div style="padding : 10px;">
                                    <div class="alert alert-danger alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <i class="icon fa fa-ban"></i><strong>SUCCESS !</strong>
                                        <?php echo $message; ?>
                                    </div>
                                </div>
                            <?php
                                }//END FOR DELETE DATA

                                //START FOR EDIT DATA
                                elseif (!empty($message) && $valid == "sukses_edit_sales") {
                             ?>
                                <div style="padding : 10px;">
                                    <div class="alert alert-success alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <i class="icon fa fa-check"></i><strong>SUCCESS !</strong>
                                        <?php echo $message; ?>
                                    </div>
                                </div>
                             <?php
                                }
                                elseif (!empty($message) && $valid == "gagal_edit_sales") {
                             ?>
                                <div style="padding : 10px;">
                                    <div class="alert alert-warning alert-dismissible">
                                        <button type="button" class="close" data-dismiss="alert">&times;</button>
                                        <i class="icon fa fa-warning"></i><strong>FAILED !</strong>
                                        <?php echo $message; ?>
                                    </div>
                                </div>
                             <?php
                                }//END FOR EDIT DATA
                             ?>
                        <!-- END MESSAGE -->
                        <!-- Hidden Form -->
                        <!-- Modal Add -->
                        <div class="modal fade" id="add_sales" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                            <form class="form-horizontal" action="<?php echo site_url('admin/agency_sales/insert_sales'); ?>" method="post">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Add Sales</h4>
                              </div>
                              <div class="modal-body">
                                  <div class="box-body">
                                    <div class="form-group">
                                      <label for="inputSalesName" class="col-sm-3 control-label">Sales Name</label>
                                      <div class="col-xs-4">
                                        <input id="sales_name" name="sales_name" type="text" class="form-control" placeholder="Sales Name">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="inputSGHName" class="col-sm-3 control-label">Email</label>
                                      <div class="col-xs-4">
                                        <input id="email_add" name="email_add" type="text" class="form-control" placeholder="SGH Name">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-3 control-label">Position</label>
                                      <select id="position" onchange="Show(this.value);" name="position" class="form-control select_sales" style="width: 30%;">
                                        <option selected="selected"></option>
                                        <option value="Account Executive">Account Executive</option>
                                        <option value="SGH">SGH</option>
                                        <option value="Manager">Manager</option>
                                      </select>
                                    </div><!-- /.form-group -->
                                    <div id="sgh" class="form-group">
                                      <label class="col-sm-3 control-label">SGH</label>
                                      <select id="head" name="head" class="form-control select_sales" style="width:30%;">
                                        <option selected="selected"></option>
                                        <?php
                                          foreach ($select_sgh as $key => $sales) {
                                        ?>
                                        <option value="<?php echo $sales['id_sales'];?>"><?php echo $sales['sales_name'];?></option>
                                        <?php
                                          }
                                        ?>
                                      </select>
                                    </div><!-- /.form-group -->
                                  </div><!-- /.box-body -->
                              </div>
                              <div class="modal-footer">
                                <button id="send_edit" type="submit" class="btn btn-success">Save changes</button>
                              </div>
                            </form>
                            </div>
                          </div>
                        </div>
                        <!-- Modal Edit -->
                        <div class="modal fade" id="edit_sales" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                            <form class="form-horizontal" action="<?php echo site_url('admin/agency_sales/edit_sales'); ?>" method="post">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Edit Agency</h4>
                              </div>
                              <div class="modal-body">
                                  <div class="box-body">
                                    <div class="form-group">
                                      <label for="inputSalesName" class="col-sm-3 control-label">Sales Name</label>
                                      <div class="col-xs-4">
                                        <input id="edt_id_sales" name="edt_id_sales" type="hidden" class="form-control" placeholder="Sales ID">
                                        <input id="edt_id_head" name="edt_id_head" type="hidden" class="form-control" placeholder="Head ID">
                                        <input id="edt_sales_position" name="edt_sales_position" type="hidden" class="form-control" placeholder="Sales Position">
                                        <input id="edt_sales_name" name="edt_sales_name" type="text" class="form-control" placeholder="Sales Name">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="inputSalesName" class="col-sm-3 control-label">Email</label>
                                      <div class="col-xs-4">
                                        <input id="edt_email" name="edt_email" type="text" class="form-control" placeholder="Email">
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label class="col-sm-3 control-label">Position</label>
                                      <select id="position" onchange="lihat(this.value);" name="position" class="form-control select_sales" style="width: 30%;">
                                        <option selected="selected"></option>
                                        <option value="Account Executive">Account Executive</option>
                                        <option value="SGH">SGH</option>
                                        <option value="Manager">Manager</option>
                                      </select>
                                      <label style="color:red; font-size : 10px;">Choose IF You Wanna CHANGE Position</label>
                                    </div>
                                    <div id="head_div" class="form-group">
                                      <label class="col-sm-3 control-label">Head</label>
                                      <select id="head" name="head" class="form-control select_sales" style="width:30%;">
                                        <option selected="selected"></option>
                                        <?php
                                          foreach ($select_sgh as $key => $sales) {
                                        ?>
                                        <option value="<?php echo $sales['id_sales'];?>"><?php echo $sales['sales_name'];?></option>
                                        <?php
                                          }
                                        ?>
                                      </select>
                                      <label style="color:red; font-size : 10px;">Choose IF You Wanna CHANGE Head</label>
                                    </div><!-- /.form-group -->
                                  </div><!-- /.box-body -->
                              </div>
                              <div class="modal-footer">
                                <button id="send_edit" type="submit" class="btn btn-success">Save changes</button>
                              </div>
                            </form>
                            </div>
                          </div>
                        </div>
                        <!-- Modal Delete -->
                        <div class="modal fade" id="dlt_sales" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <h4 class="modal-title" id="myModalLabel">Delete Sales</h4>
                              </div>
                              <div class="modal-body">
                                  <div class="box-body">
                                    <div class="form-group">
                                      <label for="inputEmail3">Do You Want to Delete This Data ?</label>
                                    </div>
                                  </div><!-- /.box-body -->
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-success" data-dismiss="modal">No</button>
                                <button id="delete_sales" type="submit" data-dismiss="modal" class="btn btn-success">Yes</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- Hidden Form -->
                            <table id="table_sales" class="table table-bordered table-striped">
                            <thead>
                              <tr>
                                <th>ID Sales</th><!--di Hide-->
                                <th>ID Head</th><!--di Hide-->
                                <th>Sales Name</th>
                                <th>Position</th>
                                <th>Head</th>
                              </tr>
                            </thead>
                            <tbody>
                                <?php
                                 // echo '<pre/>'; print_r($table_sales); die();//Debug
                                 foreach ($table_sales as $key => $sales) {
                                ?>
                                  <tr>
                                    <td><?php echo $sales[0]?></td>
                                    <td><?php echo $sales[1]?></td>
                                    <td><?php echo $sales[3]?></td><!--Sales Name-->
                                    <td><?php echo $sales[4]?></td><!--position-->
                                    <td><?php echo $sales[2]?></td><!--Head Name-->
                                  </tr>
                                <?php
                                 }
                                ?>
                            </tbody>
                            </table>
                    </div><!-- /.box-body -->
                  </div><!-- /.box -->

              </div><!-- End Table Agency -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
    </section>
    <!-- End Main content -->
</div><!-- /.content-wrapper -->

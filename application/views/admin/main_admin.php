<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Microad Trading Desk
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Info boxes -->
        <div class="row">
            <div class="col-md-12">
                <form action="<?php echo site_url('admin/dashboard_admin/open_dashboard'); ?>" method="post" enctype="multipart/form-data">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><strong>Select Parameters</strong></h3>
                    </div><!-- /.box-header -->
                    <div class="box-body">
                        <?php //echo '<pre/>'; print_r($message); die();
                        if (!empty($message) && $valid == "kosong") {
                        ?>
                          <div style="padding : 10px;">
                              <div class="alert alert-warning alert-dismissible">
                                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                                  <i class="icon fa fa-ban"></i><strong>PERINGATAN !</strong>
                                  <?php echo $message; ?>
                              </div> 
                          </div>
                        <?php
                          }//END FOR INPUT DATA
                        ?>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style="padding-top:5px;">Campaign</label>
                                <select name="campaign" class="form-control select_campaign" style="width: 45%;">
                                    <option selected="selected"></option>
                                    <?php 
                                      foreach ($campaign as $key => $campaign) {
                                    ?>
                                        <option value="<?php echo $campaign['id_campaign'];?>"><?php echo $campaign['name'];?></option>
                                    <?php
                                      }
                                    ?>
                                </select>
                            </div><!-- /.form-group -->
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style="padding-top:5px;">Platform</label>
                                <select name="platform" class="form-control select_campaign" style="width: 45%;">
                                    <option selected="selected"></option>
                                    <?php 
                                      foreach ($platform as $key => $platform) {
                                    ?>
                                        <option value="<?php echo $platform['id_platform'];?>"><?php echo $platform['platform'];?></option>
                                    <?php
                                      }
                                    ?>
                                </select>
                            </div><!-- /.form-group -->
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="col-sm-3 control-label" style="padding-top:5px;">Period</label>
                                <div class="input-group col-xs-6">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" class="form-control pull-right" id="reservation" name="date" >
                                </div><!-- /.input group -->
                            </div><!-- /.form-group -->
                        </div>
                    </div><!-- ./box-body -->
                    <div class="box-footer">
                        <button id="search" type="submit" class="btn btn-primary pull-right">Search <i class="fa fa-search"></i></button>
                    </div>
                </div><!-- /.box -->
                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
        <!-- Main row -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

            
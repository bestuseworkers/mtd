<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Microad Trading Desk
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-upload"></i> Upload</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Upload Data</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <?php //echo form_open_multipart('admin/upload/start_upload');?>
                <form action="<?php echo site_url('admin/upload/start_upload'); ?>" method="post" enctype="multipart/form-data">
                  <!-- START MESSAGE -->
                  <?php //FOR MESSAGE INPUT DATA
                  if (!empty($message) && $valid == "sukses") {
                      ?>
                      <div style="padding : 10px;">
                          <div class="alert alert-success alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <i class="icon fa fa-check"></i><strong>SUCCESS !</strong>
                              <?php echo $message; ?>
                          </div>
                      </div>
                  <?php
                      }
                      elseif (!empty($message) && $valid == "gagal") {
                  ?>
                      <div style="padding : 10px;">
                          <div class="alert alert-warning alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <i class="icon fa fa-warning"></i><strong>FAILED !</strong>
                              <?php echo $message; ?>
                          </div>
                      </div>
                  <?php
                      }
                      elseif (!empty($message) && $valid == "kosong") {
                  ?>
                      <div style="padding : 10px;">
                          <div class="alert alert-danger alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <i class="icon fa fa-ban"></i><strong>WARNING !</strong>
                              <?php echo $message; ?>
                          </div>
                      </div>
                  <?php
                      }//END FOR INPUT DATA

                      //START FOR DELETE DATA
                      elseif (!empty($message) && $valid == "dihapus") {
                  ?>
                      <div style="padding : 10px;">
                          <div class="alert alert-danger alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <i class="icon fa fa-ban"></i><strong>WARNING !</strong>
                              <?php echo $message; ?>
                          </div>
                      </div>
                  <?php
                      }//END FOR DELETE DATA

                      //START FOR EDIT DATA
                      elseif (!empty($message) && $valid == "sukses_edit") {
                   ?>
                      <div style="padding : 10px;">
                          <div class="alert alert-success alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <i class="icon fa fa-check"></i><strong>SUCCESS !</strong>
                              <?php echo $message; ?>
                          </div>
                      </div>
                   <?php
                      }
                      elseif (!empty($message) && $valid == "gagal_edit") {
                   ?>
                      <div style="padding : 10px;">
                          <div class="alert alert-warning alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert">&times;</button>
                              <i class="icon fa fa-warning"></i><strong>GAGAL !</strong>
                              <?php echo $message; ?>
                          </div>
                      </div>
                   <?php
                      }//END FOR EDIT DATA
                   ?>
              <!-- END MESSAGE -->
                  <div class="box-body">
                    <div class="form-group">
                      <label class="col-sm-1 control-label">Brand</label>
                      <select id="get_brand" onchange="changeFunc();" name="brand" class="form-control select_brand" style="width: 15%;">
                        <option selected="selected"></option>
                        <?php
                          foreach ($brand as $key => $brand) {
                        ?>
                        <option value="<?php echo $brand['id_brand'];?>"><?php echo $brand['brand_name'];?></option>
                        <?php
                          }
                        ?>
                      </select>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                      <label class="col-sm-1 control-label">Campaign</label>
                      <select id="campaign" name="campaign" class="form-control select_campaign" style="width: 16%;">
                        
                      </select>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                      <label class="col-sm-1 control-label">Platform</label>
                      <select name="platform" class="form-control select_platform" style="width: 15%;">
                        <option selected="selected"></option>
                        <?php
                          foreach ($select_platform as $key => $platform) {
                        ?>
                        <option value="<?php echo $platform['id_platform'];?>"><?php echo $platform['platform'];?></option>
                        <?php
                          }
                        ?>
                      </select>
                    </div><!-- /.form-group -->
                    <div class="form-group">
                      <label for="exampleInputFile">File input</label>
                      <input type="file" name="userfile" size="20"/>
                      <p class="help-block">Input File Format <strong style="color:red;">.xlsx</strong></p>
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" class="btn btn-success">Upload</button>
                  </div>
                </form>
              </div><!-- /.box -->
        </div>   <!-- /.row -->
    </section>
    <!-- End Main content -->
</div><!-- /.content-wrapper -->

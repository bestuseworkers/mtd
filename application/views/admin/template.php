<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>MTD Administrator</title>
    <link rel="icon" href="<?php echo base_url('assets/media/Logo-Circle.png') ?>">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/AdminLTE.min_edited.css'); ?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo base_url('assets/css/skins/_all-skins.min_edited.css'); ?>">
    <!-- Table -->
    <!--<link href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />-->
    <link href="<?php echo base_url('assets/js/plugins/datatables/dataTables.bootstrap1.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="<?php echo base_url('assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.css'); ?>" rel="stylesheet" type="text/css" />
    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2.min.css'); ?>">
    <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css">-->
    <!-- daterange picker -->
    <link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/daterangepicker/daterangepicker-bs3.css'); ?>">
    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url('assets/js/plugins/jQuery/jQuery-2.1.4.min.js'); ?>"></script>

  </head>
  <body class="hold-transition skin-black">
    <div class="wrapper">

      <?php
        //Start Header
            $this->load->view('admin/header_admin'); //for header

        // Left side column. contains the logo and sidebar -->
            $this->load->view('admin/navigation_admin');//Navigation

        //START MAIN CONTENT-->
            // echo "<pre/>"; print_r($msg); die();
            $this->load->view('admin/'.$view, $this->session->flashdata('message'));//Main Content

        //Start Footer
            $this->load->view('admin/footer_admin');//Footer
      ?>
      <!-- Control Sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <!-- Select2 -->
    <script src="<?php echo base_url('assets/js/plugins/select2/select2.full.min.js');?>"></script>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>-->
    <!-- Slimscroll -->
    <script src="<?php echo base_url('assets/js/plugins/slimScroll/jquery.slimscroll.min.js'); ?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url('assets/js/plugins/fastclick/fastclick.min.js'); ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url('assets/js/AdminLTE/app.min.js'); ?>"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url('assets/js/AdminLTE/demo.js'); ?>"></script>
    <!-- Table -->
    <!--<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>-->
    <script src="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min1.js'); ?>"></script>
    <script src="<?php echo base_url('assets/js/plugins/datatables/dataTables.bootstrap.min.js'); ?>"></script>
    <!-- date-range-picker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="<?php echo base_url('assets/js/plugins/daterangepicker/daterangepicker.js');?>"></script>

    <?php
        $this->load->view('script/'.$js);
    ?>
  </body>
</html>

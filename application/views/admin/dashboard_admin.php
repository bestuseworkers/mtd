<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>MTD Administrator</title>
        <link rel="icon" href="<?php echo base_url('assets/media/Logo-Circle.png') ?>">
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <!-- <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" /> -->
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link href="<?php echo base_url('assets/css/ionicons.min.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- Morris chart -->
        <!-- <link href="<?php echo base_url('assets/js/plugins/morris/morris.css'); ?>" rel="stylesheet"> -->
        <!-- jvectormap -->
        <link href="<?php echo base_url('assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link href="<?php echo base_url('assets/js/plugins/daterangepicker/daterangepicker-bs3.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="<?php echo base_url('assets/css/AdminLTE.min_edited.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <link href="<?php echo base_url('assets/css/skins/_all-skins.min_edited.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- For Table -->
        <link href="<?php echo base_url('assets/js/plugins/datatables/dataTables.bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
        <!--<link href="<?php echo base_url('assets/js/plugins/datatables/1.10.12/datatables.min.css'); ?>" rel="stylesheet" type="text/css" />-->
        <!-- Select2 -->
        <link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/select2/select2.min.css'); ?>">
         <!-- daterange picker -->
        <link rel="stylesheet" href="<?php echo base_url('assets/js/plugins/daterangepicker/daterangepicker-bs3.css'); ?>">
        <!-- jQuery 2.1.4 -->
        <script src="<?php echo base_url('assets/js/plugins/jQuery/jQuery-2.1.4.min.js'); ?>"></script>

    </head>
    <body class="hold-transition skin-black">
        <div class="wrapper">

            <?php
            //Start Header
                $this->load->view('admin/header_admin'); //for header

            // Left side column. contains the logo and sidebar -->

                $this->load->view('admin/navigation_admin');//Navigation

            // Right side column. Contains the navbar and content of the page -->

            //START MAIN CONTENT-->

                $this->load->view('admin/'.$view, $this->session->flashdata('message'));//Main Content

            //END MAIN CONTENT-->

            //Start Footer
                $this->load->view('admin/footer_admin');//Footer
            ?>

        </div><!-- ./wrapper -->

        <!-- Bootstrap 3.3.2 JS -->
        <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
        <!-- FastClick -->
        <script src="<?php echo base_url('assets/js/plugins/fastclick/fastclick.min.js'); ?>"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url('assets/js/AdminLTE/app.min.js'); ?>" type="text/javascript"></script>
        <!-- Sparkline -->
        <script src="<?php echo base_url('assets/js/plugins/sparkline/jquery.sparkline.min.js'); ?>" type="text/javascript"></script>
        <!-- jvectormap -->
        <script src="<?php echo base_url('assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'); ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'); ?>" type="text/javascript"></script>
        <!-- daterangepicker -->
        <script src="<?php echo base_url('assets/js/plugins/daterangepicker/daterangepicker.js'); ?>" type="text/javascript"></script>
        <!-- datepicker -->
        <script src="<?php echo base_url('assets/js/plugins/datepicker/bootstrap-datepicker.js'); ?>" type="text/javascript"></script>
        <!-- iCheck -->
        <script src="<?php echo base_url('assets/js/plugins/iCheck/icheck.min.js'); ?>" type="text/javascript"></script>
        <!-- SlimScroll 1.3.0 -->
        <script src="<?php echo base_url('assets/js/plugins/slimScroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
        <!-- ChartJS 1.0.1 -->
        <!--<script src="http://www.chartjs.org/assets/Chart.min.js"></script>-->
        <script src="<?php echo base_url('assets/js/plugins/chartjs/Chart.min.js'); ?>" type="text/javascript"></script>
        <!--table-->
        <script src="<?php echo base_url('assets/js/plugins/datatables/jquery.dataTables.min.js'); ?>"></script>
        <script src="<?php echo base_url('assets/js/plugins/datatables/dataTables.bootstrap.min.js'); ?> "></script>
        <!--<script src="<?php echo base_url('assets/js/plugins/datatables/1.10.12/dataTables.min.js'); ?> "></script>-->

        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url('assets/js/AdminLTE/demo.js'); ?>"></script>
        <!-- Select2 -->
        <script src="<?php echo base_url('assets/js/plugins/select2/select2.full.min.js');?>"></script>
        <!-- date-range-picker -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
        <script src="<?php echo base_url('assets/js/plugins/daterangepicker/daterangepicker.js');?>"></script>

        <?php
            $this->load->view('script/'.$js);
        ?>

    </body>
</html>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload extends CI_Controller {

  var $view = '';

  function __construct() {
      parent::__construct();

      $this->load->model(array(
                                'm_campaign'
                                ,'m_upload'
                              )
                          );

      $this->load->helper(array(
                                  'form'
                                  ,'url'
                              )
                          );

      $this->load->library(array(
                                  'excel'
                                  //,'upload'
                              )
                          );
  }

  public function index(){
      // if ($_SESSION['login'] == true) {
    if (!empty($_SESSION['login'])) {
          $data = array(
                        'brand'             => $this->brand(),
                        'select_campaign'   => $this->select_campaign(),
                        'select_platform'   => $this->select_platform(),
                        'view'              => 'upload',
                        'js'                => 'script_upload'
                      );
          $this->load->view('admin/template', $data);
      }
      else{
          $this->session->set_flashdata('result_login', '<br>You Have No Session, Please Login !');
          redirect('login');
      }
  }

  function select_campaign(){
      $select_campaign = $this->m_upload->table_campaign();
      return $select_campaign;
  }

  function start_upload(){
      //START UPLOAD
      $kurs = "Rp.";
      $id_brand    = $_POST['brand'];
      $id_campaign = $_POST['campaign'];
      $id_platform = $_POST['platform'];

      $cek = $this->m_upload->cek_upload($id_brand, $id_campaign, $id_platform);

      if ($cek->num_rows() > 0) {
        $campaign = $this->m_upload->name_campaign($id_campaign);
        $brand    = $this->m_upload->brand_name($id_brand);

        $lvl_up     = call_user_func_array('array_merge', $campaign);//FOR LEVEL UP ARRAY
        $brand_name = call_user_func_array('array_merge', $brand);//FOR LEVEL UP ARRAY

        $name       = str_replace(' ', '_',$lvl_up['name']);
        $name_brand = str_replace(' ', '_',$brand_name['brand_name']);

        $time = date('d-m-Y');

        $config['upload_path'] = './assets/media/excel/';
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size'] = '100000';
        $config['file_name'] = strval($name_brand.'_'.$name.'_'.$time);
        $config['overwrite'] = TRUE;

        $file_name = key($_FILES);
        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload())
        {
            $error = array('error' => $this->upload->display_errors());

            $message = array(
                        'valid'     => 'gagal',
                        'message'   => $error['error']
                    );

            $this->session->set_flashdata('message',$message);
            redirect('admin/upload');
        }
        else
        {
            $kurs = "Rp.";//GANTI KE $ JIKA MENGGUNAKAN DOLLAR
            $campaign  = $this->input->post('campaign');
            $period    = $this->input->post('date');//05/11/2016 - 05/11/2016
            $brand     = $_POST['brand'];

            $upload = array(
                            'id_campaign'   => $campaign
                            ,'id_platform'  => $id_platform
                            ,'id_brand'     => $brand
                            );

            $data = array('upload_data' => $this->upload->data());
            $upload_data = key($data);

            //START PHPEXCEL
            $objReader = PHPExcel_IOFactory::createReader('Excel2007');

            $objReader->setReadDataOnly(true);
            $objPHPExcel        = $objReader->load($data[$upload_data]['full_path']);
            $Sheet              = $objReader->listWorksheetNames($data[$upload_data]['full_path']);

            $objWorksheet       = $objPHPExcel->getSheet(0);
            $highestRow         = $objWorksheet->getHighestRow();
            $highestColumn      = $objWorksheet->getHighestColumn();
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

            for ($row = 1; $row <= $highestRow; ++$row) {
              $arr[$row] = array();
              for ($col = 0; $col <= $highestColumnIndex-1; ++$col) {
                /** Code untuk ambil value (rumus akan dibaca rumus)--> 'getValue()'*/
                //$arr[$row][$col] = $objWorksheet->getCellByColumnAndRow($col, $row)->getValue();
                /** Code untuk ambil value yang berupa rumus --> 'getCalculatedValue()'*/
                $arr[$row][$col] = $objWorksheet->getCellByColumnAndRow($col, $row)->getCalculatedValue();
              }
            }
            //END PHPEXCEL
            
            //Logic to Store to DB
            $this->m_upload->insert_temp($arr, $highestRow, $upload);

            $table_val    = $this->m_upload->table_temp($id_campaign, $id_brand);
            $platform_dt  = call_user_func_array('array_merge', $this->m_upload->platform($id_platform));
            $campaign_dt  = call_user_func_array('array_merge', $this->campaign());
            // $table_val   = call_user_func_array('array_merge', $table_val);
            $value = array();
            $id_temp_dash = array();

            if ($platform_dt['platform'] == 'Programmatic') {
                $table_th = array('id_tmp_dash','Date','Device','Targeting','Cost','Impression','Clicks','CTR','Conversions','CVR','CPC','CPM','CPA', 'Session');

                foreach ($table_val as $key => $table_val) {
                  array_push($value, array(
                    'id_tmp_dash' => $table_val['id_tmp_dash'],
                    'date'        => $table_val['date'],
                    'device'      => $table_val['device'],
                    'targeting'   => $table_val['targeting'],
                    'cost'        => "$ ".number_format($table_val['cost'], 2, ".", ","),
                    'impression'  => number_format($table_val['impression'], 0, '.', ','),
                    'clicks'      => $table_val['clicks'],
                    'ctr'         => number_format($table_val['ctr']*100, 2, ".", ",")." %",
                    'conversions' => $table_val['conversions'],
                    'cvr'         => number_format($table_val['cvr']*100, 2, ",", ".")." %",
                    'cpc'         => "$ ".number_format($table_val['cpc'], 2, ",", "."),
                    'cpm'         => "$ ".number_format($table_val['cpm'], 2, ".", ","),
                    'cpa'         => "$ ".number_format($table_val['cpa'], 2, ",", "."),
                    'session'     => number_format($table_val['session'], 0, ",", "."),
                    )
                  );
                  
              array_push($id_temp_dash, array(
                    $table_val['id_tmp_dash']
                  )
                );
              }
            }
            elseif ($platform_dt['platform'] == 'Facebook') {
              $table_th = array(
                'id_tmp_dash',
                'Date',
                'Cost',
                'Impression',
                'Clicks',
                'CTR',
                'Engagement',
                'Eng Rate',
                'Page Likes',
                'Likes Rate',
                'Views (Video)',
                'Views Rate',
                'Conversions',
                'Leads',
                'Reach',
                'CVR',
                'CPC',
                'CPM',
                'CPE',
                'CPL',
                'CPV',
                'CPA',
                'CPLeads',
                'CPReach',
                'adRecall',
                'Session'
              );

              foreach ($table_val as $key => $table_val) {
                array_push($value, array(
                  'id_tmp_dash'   => $table_val['id_tmp_dash'],
                  'date'          => $table_val['date'],
                  'cost'          => $kurs.number_format($table_val['cost'], 2, ".", ","),
                  'impression'    => number_format($table_val['impression'], 0, '.', ','),
                  'clicks'        => $table_val['clicks'],
                  'ctr'           => number_format($table_val['ctr']*100, 2, ".", ",")." %",
                  'engagement'    => number_format($table_val['engagement'], 0, '.', ','),
                  'eng_rate'      => number_format($table_val['eng_rate']*100, 2, ".", ",")." %",
                  'page_likes'    => number_format($table_val['page_likes'], 0, '.', ','),
                  'like_rates'    => number_format($table_val['like_rates']*100, 2, ".", ",")." %",
                  'views_video'   => number_format($table_val['views_video'], 0, '.', ','),
                  'view_rates'    => number_format($table_val['view_rates']*100, 2, ".", ",")." %",
                  'conversions'   => number_format($table_val['conversions'], 0, '.', ','),
                  'leads'         => number_format($table_val['leads'], 0, '.', ','),
                  'reach'         => number_format($table_val['reach'], 0, '.', ','),
                  'cvr'           => number_format($table_val['cvr']*100, 2, ".", ",")." %",
                  'cpc'           => $kurs.number_format($table_val['cpc'], 2, ",", "."),
                  'cpm'           => $kurs.number_format($table_val['cpm'], 2, ".", ","),
                  'cpe'           => $kurs.number_format($table_val['cpe'], 2, ".", ","),
                  'cpl'           => $kurs.number_format($table_val['cpl'], 2, ",", "."),
                  'cpv'           => $kurs.number_format($table_val['cpv'], 2, ",", "."),
                  'cpa'           => $kurs.number_format($table_val['cpa'], 2, ",", "."),
                  'cpleads'       => $kurs.number_format($table_val['cpleads'], 2, ",", "."),
                  'cpreach'       => $kurs.number_format($table_val['cpreach'], 2, ",", "."),
                  'adRecall'      => number_format($table_val['adRecall'], 0, ",", "."),
                  'session'       => number_format($table_val['session'], 0, ",", "."),
                ));
                
                array_push($id_temp_dash, array(
                  $table_val['id_tmp_dash']
                )
                );
              }
            }
            elseif ($platform_dt['platform'] == 'Instagram') {
              $table_th = array(
                'id_tmp_dash',
                'Date','Cost',
                'Impression',
                'Clicks',
                'CTR',
                'Engagement',
                'Eng Rate',
                'Views (Video)',
                'View Rate',
                'Leads',
                'Reach',
                'CPC',
                'CPM',
                'CPE',
                'CPV',
                'CPLeads',
                'CPReach',
                'adRecall',
                'Session'
              );

              foreach ($table_val as $key => $table_val) {
                array_push($value, array(
                  'id_tmp_dash' => $table_val['id_tmp_dash'],
                  'date'        => $table_val['date'],
                  'cost'        => $kurs.number_format($table_val['cost'], 2, ".", ","),
                  'impression'  => number_format($table_val['impression'], 0, '.', ','),
                  'clicks'      => $table_val['clicks'],
                  'ctr'         => number_format($table_val['ctr']*100, 2, ".", ",")." %",
                  'engagement'  => number_format($table_val['engagement'], 0, '.', ','),
                  'eng_rate'    => number_format($table_val['eng_rate']*100, 2, ".", ",")." %",
                  'views_video' => number_format($table_val['views_video'], 0, '.', ','),
                  'view_rates'  => number_format($table_val['view_rates']*100, 2, ".", ",")." %",
                  'leads'       => number_format($table_val['leads'], 0, '.', ','),
                  'reach'       => number_format($table_val['reach'], 0, '.', ','),
                  'cpc'         => $kurs.number_format($table_val['cpc'], 2, ",", "."),
                  'cpe'         => $kurs.number_format($table_val['cpe'], 2, ".", ","),
                  'cpm'         => $kurs.number_format($table_val['cpm'], 2, ".", ","),
                  'cpv'         => $kurs.number_format($table_val['cpv'], 2, ",", "."),
                  'cpleads'     => $kurs.number_format($table_val['cpleads'], 2, ",", "."),
                  'cpreach'     => $kurs.number_format($table_val['cpreach'], 2, ",", "."),
                  'adRecall'    => number_format($table_val['adRecall'], 0, ",", "."),
                  'session'     => number_format($table_val['session'], 0, ",", "."),
                ));
                
                array_push($id_temp_dash, array(
                  $table_val['id_tmp_dash']
                ));
              }
            }
            elseif ($platform_dt['platform'] == 'Twitter') {
                $table_th = array('id_tmp_dash','Date','Cost','Impression','Clicks','CTR','Engagement', 'Eng Rate', 'Followers', 'Follow Rate','Views (Video)', 'View Rate','CPC','CPM','CPL', 'Session');

                foreach ($table_val as $key => $table_val) {
                  array_push($value, array(
                    'id_tmp_dash' => $table_val['id_tmp_dash'],
                    'date'        => $table_val['date'],
                    'cost'        => $kurs.number_format($table_val['cost'], 2, ".", ","),
                    'impression'  => number_format($table_val['impression'], 0, '.', ','),
                    'clicks'      => $table_val['clicks'],
                    'ctr'         => number_format($table_val['ctr']*100, 2, ".", ",")." %",
                    'engagement'  => number_format($table_val['engagement'], 0, '.', ','),
                    'eng_rate'    => number_format($table_val['eng_rate']*100, 2, ".", ",")." %",
                    'followers'   => number_format($table_val['followers'], 0, '.', ','),
                    'follow_rates'=> number_format($table_val['follow_rates']*100, 2, '.', ',')."%",
                    'views_video' => number_format($table_val['views_video'], 0, '.', ','),
                    'view_rates'  => number_format($table_val['view_rates']*100, 2, ".", ",")." %",
                    'cpc'         => $kurs.number_format($table_val['cpc'], 2, ",", "."),
                    'cpm'         => $kurs.number_format($table_val['cpm'], 2, ".", ","),
                    'cpl'         => $kurs.number_format($table_val['cpl'], 2, ",", "."),
                    'session'     => number_format($table_val['session'], 0, ",", "."),
                  ));
                  
                  array_push($id_temp_dash, array(
                    $table_val['id_tmp_dash']
                  ));
                }
            }
            elseif ($platform_dt['platform'] == 'Native') {
                $table_th = array('id_tmp_dash','Date','Cost','Impression','Clicks','CTR','CPC','CPM', 'Session');

                foreach ($table_val as $key => $table_val) {
                  array_push($value, array(
                    'id_tmp_dash' => $table_val['id_tmp_dash'],
                    'date'        => $table_val['date'],
                    'cost'        => $kurs.number_format($table_val['cost'], 2, ".", ","),
                    'impression'  => number_format($table_val['impression'], 0, '.', ','),
                    'clicks'      => $table_val['clicks'],
                    'ctr'         => number_format($table_val['ctr']*100, 2, ".", ",")." %",
                    'cpc'         => $kurs.number_format($table_val['cpc'], 2, ",", "."),
                    'cpm'         => $kurs.number_format($table_val['cpm'], 2, ".", ","),
                    'session'     => number_format($table_val['session'], 0, ".", ","),
                  ));
                  
                  array_push($id_temp_dash, array(
                    $table_val['id_tmp_dash']
                  ));
                }
            }
            elseif ($platform_dt['platform'] == 'YouTube' or $platform_dt['platform'] == 'Video Network') {
                $table_th = array('id_tmp_dash', 'Date', 'Device','Targeting', 'Cost', 'Impression', 'Clicks', 'Views (Video)', 'Views Rate', 'CTR', 'CPC', 'CPM', 'CPV', 'Session');

                foreach ($table_val as $key => $table_val) {
                  array_push($value, array(
                    'id_tmp_dash' => $table_val['id_tmp_dash'],
                    'date'        => $table_val['date'],
                    'device'      => $table_val['device'],
                    'targeting'   => $table_val['targeting'],
                    'cost'        => $kurs.number_format($table_val['cost'], 2, ".", ","),
                    'impression'  => number_format($table_val['impression'], 0, '.', ','),
                    'clicks'      => $table_val['clicks'],
                    'views_video' => number_format($table_val['views_video'], 0, '.', ','),
                    'view_rates'  => number_format($table_val['view_rates']*100, 2, ".", ",")." %",
                    'ctr'         => number_format($table_val['ctr']*100, 2, ".", ",")." %",
                    'cpc'         => $kurs.number_format($table_val['cpc'], 2, ",", "."),
                    'cpm'         => $kurs.number_format($table_val['cpm'], 2, ".", ","),
                    'cpv'         => $kurs.number_format($table_val['cpv'], 2, ",", "."),
                    'session'         => number_format($table_val['session'], 0, ",", "."),
                  ));
                  
                  array_push($id_temp_dash, array(
                    $table_val['id_tmp_dash']
                  ));
              }
            }
            elseif ($platform_dt['platform'] == 'CPI') {
                $table_th = array('id_tmp_dash', 'Date', 'Device','Targeting', 'Cost', 'Impression', 'Clicks', 'Install', 'Install Rate','CTR','CPI', 'Session');

                  foreach ($table_val as $key => $table_val) {
                    array_push($value, array(
                      'id_tmp_dash'     => $table_val['id_tmp_dash'],
                      'date'            => $table_val['date'],
                      'device'          => $table_val['device'],
                      'targeting'       => $table_val['targeting'],
                      'cost'            => $kurs.number_format($table_val['cost'], 2, ".", ","),
                      'impression'      => number_format($table_val['impression'], 0, '.', ','),
                      'clicks'          => $table_val['clicks'],
                      'install'         => number_format($table_val['install'], 0, '.', ','),
                      'install_rate'    => number_format($table_val['install_rate']*100, 2, ".", ",")." %",
                      'ctr'             => number_format($table_val['ctr']*100, 2, ".", ",")." %",
                      'cpi'             => $kurs.number_format($table_val['cpi'], 2, ",", "."),
                      'session'         => number_format($table_val['session'], 0, ",", "."),
                    ));
                    
                    array_push($id_temp_dash, array(
                      $table_val['id_tmp_dash']
                    ));
                }
            }
            elseif ($platform_dt['platform'] == 'Publisher') {
              $table_th = array('id_tmp_dash', 'Date', 'Revenue','Ad Request', 'Impression', 'Fill Rate', 'Clicks', 'CTR','CPM','CPC', 'Session');

              foreach ($table_val as $key => $table_val) {
                array_push($value, array(
                  'id_tmp_dash'     => $table_val['id_tmp_dash'],
                  'date'            => $table_val['date'],
                  'revenue'         => $kurs.number_format($table_val['cost'], 2, ".", ","),
                  'ad_request'      => $table_val['ad_request'],
                  'impression'      => number_format($table_val['impression'], 0, '.', ','),
                  'fill_rate'       => number_format($table_val['fill_rate']*100, 2, ".", ",")." %",
                  'clicks'          => $table_val['clicks'],
                  'ctr'             => number_format($table_val['ctr']*100, 2, ".", ",")." %",
                  'cpm'             => $kurs.number_format($table_val['cpm'], 2, ".", ","),
                  'cpc'             => $kurs.number_format($table_val['cpm'], 2, ".", ","),
                  'session'         => number_format($table_val['session'], 0, ".", ","),
                ));
                
                array_push($id_temp_dash, array(
                  $table_val['id_tmp_dash']
                ));
              }
            }
            elseif ($platform_dt['platform'] == 'SEM') {
                $table_th = array('id_tmp_dash', 'Date', 'Device','Targeting', 'Cost', 'Impression', 'Clicks', 'Conversions', 'CPM','CPC', 'CPA', 'CVR', 'Session');

                  foreach ($table_val as $key => $table_val) {
                    // echo '<pre/>'; print_r($table_val); die();//DEBUG
                    array_push($value, array(
                      'id_tmp_dash'     => $table_val['id_tmp_dash'],
                      'date'            => $table_val['date'],
                      'device'          => $table_val['device'],
                      'targeting'       => $table_val['targeting'],
                      'cost'            => "$ ".number_format($table_val['cost'], 2, ".", ","),
                      'impression'      => number_format($table_val['impression'], 0, '.', ','),
                      'clicks'          => $table_val['clicks'],
                      'conversions'     => number_format($table_val['conversions'], 0, '.', ','),
                      'cpm'             => "$ ".number_format($table_val['cpm'], 2, ".", ","),
                      'cpc'             => "$ ".number_format($table_val['cpm'], 2, ".", ","),
                      'cpa'             => "$ ".number_format($table_val['cpa'], 2, ",", "."),
                      'cvr'             => number_format($table_val['cvr']*100, 2, ".", ",")." %",
                      'session'         => number_format($table_val['session'], 0, ".", ","),
                    ));
                    
                    array_push($id_temp_dash, array(
                      $table_val['id_tmp_dash']
                    ));
                }
            }
            elseif ($platform_dt['platform'] == 'Tiktok') {
              // GET view 2s / 6s
              $viewSecond;
              $rateSecond;

              foreach($table_val as $key => $vSecond){
                if ($vSecond['view_6s'] != 0) {
                  $viewSecond = 'View 6s';
                  $rateSecond = 'View Rates 6s';
                  break;
                }
                else if($vSecond['view_2s'] != 0) {
                  $viewSecond = 'View 2s';
                  $rateSecond = 'View Rates 2s';
                  break;
                }
              }

              $table_th = array(
                'Date', 'Cost', 'Impression', 'Clicks', 'CTR', 'Engagement', 'Conversion', 'Reach', 'Followers', $viewSecond, $rateSecond, 'Video Likes', 'Comment', 'Share', 'Profile Visits', 'Session'
              );

              foreach ($table_val as $key => $table_val) {
                if ($table_val['view_6s'] != 0) {
                  $tdview     = number_format($table_val['view_6s'], 0, '.', ','); 
                  $tdviewRate = number_format($table_val['viewRates_2_6_s']*100, 2, '.', ',')." %"; 
                }
                else if($table_val['view_2s'] != 0) {
                  $tdview     = number_format($table_val['view_2s'], 0, '.', ','); 
                  $tdviewRate = number_format($table_val['viewRates_2_6_s']*100, 2, '.', ',')." %"; 
                }

                array_push(
                  $value, 
                  array(
                    'id_tmp_dash'   => $table_val['id_tmp_dash'],
                    'date'          => $table_val['date'],
                    'device'        => $table_val['device'],
                    'targeting'     => $table_val['targeting'],
                    'cost'          => "Rp ".number_format($table_val['cost'], 0, ".", ","),
                    'impression'    => number_format($table_val['impression'], 0, '.', ','),
                    'clicks'        => $table_val['clicks'],
                    'ctr'           => number_format($table_val['ctr']*100, 2, ".", ",")." %",
                    'engagement'    => number_format($table_val['engagement'], 0, '.', ','),
                    'conversions'   => number_format($table_val['conversions'], 0, '.', ','),
                    'reach'         => number_format($table_val['reach'], 0, '.', ','),
                    'followers'     => number_format($table_val['followers'], 0, '.', ','),
                    'viewSecond'    => $tdview,
                    'viewSecondRate'=> $tdviewRate,
                    'videoLikes'    => number_format($table_val['videoLikes'], 0, '.', ','),
                    'comment'       => number_format($table_val['comment'], 0, '.', ','),
                    'shares'        => number_format($table_val['shares'], 0, '.', ','),
                    'profileVisits' => number_format($table_val['profileVisits'], 0, '.', ','),
                    'session'       => number_format($table_val['session'], 0, '.', ','),
                  )
                );
                
                array_push(
                  $id_temp_dash, 
                  array(
                        $table_val['id_tmp_dash']
                  )
                );
              }
          }

          $data = array(
            'campaign'      => $campaign_dt['campaign']
            ,'platform_name'=> $platform_dt['platform']
            ,'brand'        => $name_brand
            ,'th'           => $table_th
            ,'temp_table'   => $value
            ,'id_temp'      => $id_temp_dash
            ,'view'         => 'upload_temp'
            ,'js'           => 'script_upload'
          );

          $this->load->view('admin/template', $data);
        }
        //END UPLOAD
      }
      else{
        $message = array(
            'valid'     => 'kosong',
            'message'   => 'Data You Wanna Input Not Registered in Campaign, Please Insert In Campaign First !'
        );

        $this->session->set_flashdata('message',$message);

        redirect('admin/upload');
      }

  }

  function campaign(){
      $campaign = $this->m_upload->campaign();
      return $campaign;
  }

  function select_platform(){//FOR SELECT USER
      $select_platform = $this->m_campaign->table_platform();
      // echo '<pre/>'; print_r($select_sales);die();//debug
      return $select_platform;
  }

  function input() {
    if ($_POST['submitted'] ==  'submit') {
      foreach ($_POST as $key => $v) {
        if ($v != 'submit') {
          $id[] = $v;
        }
      }
      
      $cek = $this->m_upload->input_dashboard($id);

      $message = array(
          'valid'     => $cek['valid'],
          'message'   => $cek['message']
      );

      $this->session->set_flashdata('message',$message);
      redirect('admin/upload');
    }
    else {
      foreach ($_POST as $key => $v) {
        if ($v != 'cancle') {
          $id[] = $v;
        }
      }

      $cek = $this->m_upload->delete_temp($id);

      $message = array(
        'valid'     => $cek['valid'],
        'message'   => $cek['message']
      );

      $this->session->set_flashdata('message',$message);
      redirect('admin/upload');
    }
  }

  function brand(){
      $brand = $this->m_upload->brand();

      return $brand;
  }

  function get_campaign(){
      $id_brand = $_POST['id_brand'];

      $get_brand = $this->m_upload->get_campagin($id_brand);


      $campaign = array();

      foreach ($get_brand as $key => $get_brand) {
          array_push($campaign,  array(
                                          'id_campaign'   => $get_brand['id_campaign'],
                                          'name'          => $get_brand['name']
                                      )
                      );
      }

      echo json_encode($campaign);

  }
}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

  function __construct() {
    parent::__construct();

    $this->load->model(array(
      'm_campaign'
      ,'m_dashboard'
      ,'m_upload'
    ));

    $this->load->library(array(
      'excel'
      // ,'excel_export'
    ));
  }

	public function index(){
		// if ($_SESSION['login'] == true) {
    if (!empty($_SESSION['login'])) {
      // print_r($_SESSION['role']); die;
			if ($_SESSION['role'] == 'user') {
				$data = array(
          'chart'			=> 'tidak',
          'brand'			=> $this->select_brand_user(),
          'campaign'	=> $this->select_campaign(),
          'platform'	=> $this->select_platform(),
          'view' 			=> 'main_dashboard',
          'js'   			=> 'script_dashboard'
        );
			}
			elseif ($_SESSION['role']=='client'){
				$data = array(
          'chart'			=> 'tidak',
          'brand'			=> $this->select_brand(),
          'campaign'	=> $this->select_campaign(),
          'publisher'	=> $this->select_platform(),
          'view' 			=> 'main_dashboard',
          'js'   			=> 'script_dashboard'
        );
			}
      elseif ($_SESSION['role']=='publisher') {
        $data = array(
          'chart'			=> 'tidak',
          'publisher'	=> $this->get_publisher(),
          'campaign'	=> $this->publisher_campaign(),
          'view' 			=> 'main_dashboard',
          'js'   			=> 'script_publisher'
        );
      }

      $this->load->view('dashboard', $data);
    }
    else{
      $this->session->set_flashdata('result_login', '<br>Please Input Your Username and Password First !');
      redirect('login');
    }
  }

	function select_brand(){
    $id_user    = $_SESSION['u_id'];

		$dashboard  = $this->m_dashboard->name_brand_client($id_user);
		return $dashboard;
	}

	function select_brand_user(){
    $name 		  = $_SESSION['nama'];
    $get_id 	  = $this->m_dashboard->get_id_sales($name);
		$dashboard 	= $this->m_dashboard->name_brand_user($get_id);
		
    return $dashboard;
	}

  function publisher_campaign(){
    $publisher    = $this->get_publisher();
    $get_campagin = $this->m_dashboard->campaign_publisher($publisher);

    // echo '<pre/>'; print_r($get_campagin); die();//DEBUG
    return $get_campagin;
  }

	function select_campaign(){
		$dashboard = $this->m_dashboard->name_campaign();
		return $dashboard;
	}

	function select_platform(){
		$platform = $this->m_dashboard->platform();
		return $platform;
	}

	function campaign($id_campaign){
    $campaign = $this->m_dashboard->campaign($id_campaign);
    return $campaign;
  }

  function select_type(){
    $id_brand     = $_POST['id_brand'];
    $id_campaign  = $_POST['id_campaign'];
    $id_platform  = $_POST['id_platform'];

    $type    = $this->m_dashboard->type($id_brand, $id_campaign, $id_platform);
    $device1 = array();

    $count   = COUNT($type);

    # Check ada berapa device
    if ($count < 3 && $count > 1) {
      # looping data
      foreach ($type as $key => $v) {
        if ($v['device'] != 'ALL') {
          array_push($device1, $v);
         }
      }
    }
    if ($count < 2) {
      $device1 = $type;
    }
    if ($count >= 3) {
      foreach ($type as $key => $v) {
        array_push($device1, $v);
      }
    }

    echo json_encode($device1);
  }

  // Start For Publisher Dashboard
  // For Get Publisher Name
  function get_publisher(){
    $name = $_SESSION['nama'];
    return ($name);
  }

  function get_uid(){
    $name         = $_POST['name'];
    $date         = $_POST['date'];
    $id_campaign  = $_POST['id_campaign'];
    $u_id         = $this->m_dashboard->get_uid($name);
    $campaign_dt  = $this->m_dashboard->get_campagin_data($u_id, $id_campaign);

    $pub_data     = array(
      'name'        => $name,
      'date_from'   => date('Y-m-d',strtotime(substr($date, 0,10))),
      'date_to'     => date('Y-m-d',strtotime(substr($date, 13, 22))),
      'u_id'        => $u_id[0],
      'id_campaign' => $campaign_dt[0]['id_campaign'],
      'id_brand'    => $campaign_dt[0]['id_brand'],
      'id_platform' => $campaign_dt[0]['id_platform']
    );

    //Send Data via JSON for Javascript
    echo json_encode($pub_data); 
  }

  function open_dashboard_pub() {
    $count_dash   = array();
    $count_dash = array();
    $table_val  = array();

    $name         = $_POST['name'];
    $id_brand     = $_POST['id_brand'];
    $id_campaign  = $_POST['id_campaign'];
    $id_platform  = $_POST['id_platform'];
    $u_id         = $_POST['u_id'];
    $date_from    = $_POST['date_from'];
    $date_to      = $_POST['date_to'];
    $tanggal      = $this->period($date_from, $date_to);

    $dt_dashboard = $this->m_dashboard->get_dasboard_pub($id_brand, $id_campaign, $id_platform, $date_from, $date_to);
    $campaign_nm = call_user_func_array('array_merge', $this->m_upload->name_campaign($id_campaign));
    $platform_dt = call_user_func_array('array_merge', $this->m_upload->platform($id_platform));

    //KETERANGAN TABLE
    $ket_table = array('Date', 'Device', 'Targeting', 'Cost', 'Impression', 'Clicks', 'CTR', 'Engagement', 'Page likes', 'Followers', 'Views video', 'Views Rate', 'Conversions', 'CVR', 'CPC','CPM','CPE','CPL','CPV', 'CPA', 'Install', 'Install Rate', 'CPI', 'Ad Request', 'Fill Rate');

		foreach ($dt_dashboard as $key => $value) {
			array_push($table_val, array(
        $value['date'],
        $value['device'],
        $value['targeting'],
        number_format($value['cost'], 2, '.', ''),
        $value['impression'],
        $value['clicks'],
        number_format($value['ctr']*100, 2, '.', ''),
        $value['engagement'],
        $value['page_likes'],
        $value['followers'],
        $value['views_video'],
        $value['view_rates'],
        $value['conversions'],
        number_format($value['cvr'], 2, '.', ''),
        number_format($value['cpc'], 2, '.', ''),
        number_format($value['cpm'], 2, '.', ''),
        number_format($value['cpe'], 2, '.', ''),
        number_format($value['cpl'], 2, '.', ''),
        number_format($value['cpv'], 2, '.', ''),
        number_format($value['cpa'], 2, '.', ''),
        number_format($value['install'], 2, '.', ''),
        number_format($value['install_rate'], 2, '.', ''),
        number_format($value['cpi'], 2, '.', ''),
        number_format($value['ad_request'], 2, '.', ''),
        number_format($value['fill_rate'], 2, '.', ''),
      ));
		}

    // Start Publisher Code
    $result = array(0, 0, 0, 0, 0);
    $ket_dash_1 = array('Revenue','ad_request','Impression', 'fill_rate', 'Clicks', 'CTR', 'CPM','CPC');

    foreach ($dt_dashboard as $key => $value) {
      array_push($count_dash, array(
        $value['cost'],//0
        $value['ad_request'],//1
        $value['impression'],//2
        $value['fill_rate'], //3
        $value['clicks'],//4
      ));
    }

    //UNTUK SUM DATA DARI ARRAY KE ARRAY
    foreach ($count_dash as $row)
    {
        foreach ($row as $key => $value)
        {
          $result[$key] += $value;
        }
    }

    // Kodisi & Perhitungan
    if ($result[1] != 0) {
      $CTR = $result[4]/$result[2]*100; //CLICK / IMPRESSIONS * 100
    }
    else {
      $CTR = 0;
    }

    if ($result[1] != 0) {
      $CPM = ($result[0]/$result[2])*1000;//COST / IMPRESSION * 1000
    }
    else{
      $CPM = 0;
    }

    if ($result[2] != 0) {
      $CPC = $result[0]/$result[4]; //COST / CLICK
    }
    else{
      $CPC = 0;
    }

    if ($result[3]!=0) {
      $FR = ($result[2]/$result[1])*100;// impression / ad request * 100
    }
    else{
      $FR = 0;
    }


    // Push data Ke Array
    $last = array(
      "Rp. ".number_format($result[0], 2, ".", ","),//COST (REVENUE)
      number_format($result[1], 0, '.', ','),//ADD REQUEST
      number_format($result[2], 0, '.', ','),//IMPRESSION
      number_format($FR, 2, ".", ",")." %",//FILL RATE
      number_format($result[4], 0, '.', ','),//CLICK
      number_format($CTR, 2, ".", ",")." %",//CTR (CLICK / IMPRESSIONS)
      "Rp. ".number_format($CPM, 2, ".", ","), //CPM (COST / IMPRESSION)
      "Rp. ".number_format($CPC, 2, ".", ","), //CPC (COST / CLICK)
    );

    if (empty($dt_dashboard)) {
      $message = array(
        'valid'     => 'kosong',
        'message'   => 'Data Tidak Ditemukan !'
      );

      $this->session->set_flashdata('message',$message);

      redirect('admin/dashboard_admin');
    }
    else{
      $ket_chart = array();
      $dash 	   = array();
      $table 	   = array();

      array_push($ket_chart,  array(
          'platform'   => $platform_dt['platform'],
          'campaign'   => $campaign_nm['name'],
          'from'		    => $tanggal['period_f'],
          'to'			    => $tanggal['period_t'],
          'ket_dash_1'	=> $ket_dash_1,
          'ket_dash_2'	=> $last
      ));

      array_push($table,  array(
        'ket'   	    => $ket_table,
        'value'      => $table_val
      ));

      foreach ($dt_dashboard as $key => $dt_dashboard) {
        $tgl  = $dt_dashboard['date'];
        $date = $this->chart_date($tgl);
        array_push($dash,  array(
          'id_dashboard'    => $dt_dashboard['id_dashboard'],
          'id_brand'        => $dt_dashboard['id_brand'],
          'id_campaign'     => $dt_dashboard['id_campaign'],
          'id_platform'     => $dt_dashboard['id_platform'],
          'date'       	    => $date['tgl'],
          'device'		      => $dt_dashboard['device'],
          'targeting'       => $dt_dashboard['targeting'],
          'Cost'       	    => number_format($dt_dashboard['cost'], 2, '.', ''),
          'Impression'      => $dt_dashboard['impression'],
          'Clicks'       	  => $dt_dashboard['clicks'],
          'CTR'       		  => number_format($dt_dashboard['ctr']*100, 2, '.', ''),
          'Engagement'      => $dt_dashboard['engagement'],
          'Eng_Rate'        => number_format($dt_dashboard['eng_rate']*100, 2, '.', ''),
          'Page_Likes'      => $dt_dashboard['page_likes'],
          'Likes_Rate'      => number_format($dt_dashboard['like_rates']*100, 2, '.', ''),
          'Followers'       => $dt_dashboard['followers'],
          'Follow_Rate'     => number_format($dt_dashboard['follow_rates']*100, 2, '.', ''),
          'views_video'     => $dt_dashboard['views_video'],
          'Views_Rate'      => number_format($dt_dashboard['view_rates']*100, 2, '.', ''),
          'Conversions'    => $dt_dashboard['conversions'],
          'CVR'       		  => number_format($dt_dashboard['cvr'], 2, '.', ''),
          'CPC'       		  => number_format($dt_dashboard['cpc'], 2, '.', ''),
          'CPM'       		  => number_format($dt_dashboard['cpm'], 2, '.', ''),
          'CPE'       		  => number_format($dt_dashboard['cpe'], 2, '.', ''),
          'CPL'       		  => number_format($dt_dashboard['cpl'], 2, '.', ''),
          'CPF'       		  => number_format($dt_dashboard['cpf'], 2, '.', ''),
          'CPV'       		  => number_format($dt_dashboard['cpv'], 2, '.', ''),
          'CPA'       		  => number_format($dt_dashboard['cpa'], 2, '.', ''),
          'Install'         => number_format($dt_dashboard['install'], 2, '.', ''),
          'install_Rate'    => number_format($dt_dashboard['install_rate']*100, 2, '.', ''),
          'CPI'             => number_format($dt_dashboard['cpi'], 2, '.', ''),
          'ad_request'   	  => number_format($dt_dashboard['ad_request']*100, 2, '.', ''),
          'fill_rate'       => number_format($dt_dashboard['fill_rate'], 2, '.', '')
        ));
      }

      $data['ket']	  = $ket_chart;
      $data['dash']	  = $dash;
      $data['table']	= $table;

      echo json_encode($data);
    }
}

function get_table_publisher(){
  $value        = array();

  $id_brand     = $_POST['id_brand'];
  $id_campaign  = $_POST['id_campaign'];
  $id_platform  = $_POST['id_platform'];
  $date         = $_POST['date'];
  $date_from    = date('Y-m-d',strtotime(substr($date, 0,10)));
  $date_to      = date('Y-m-d',strtotime(substr($date, 13, 22)));
  $name         = $_POST['name'];
  // echo "<pre/>"; print_r($_POST); die();//DEBUG

  $dt_table     = $this->m_dashboard->data_table_pub($id_brand, $id_campaign, $id_platform, $date_from, $date_to);
  $table_th     = array('Date','Revenue','Ad Request','Impression', 'Fill Rate', 'Clicks', 'CTR', 'CPM','CPC');

  foreach ($dt_table as $key => $table_val) {
    array_push($value, array(
      'date'        	=> $table_val['date'],
      'cost'        	=> "Rp. ".number_format($table_val['cost'], 2, ".", ","),
      'ad_request'  	=> number_format($table_val['ad_request'], 0, '.', ','),
      'impression'  	=> number_format($table_val['impression'], 0, '.', ','),
      'fill_rate'  		=> number_format($table_val['fill_rate']*100, 2, ".", ",")." %",
      'clicks'      	=> $table_val['clicks'],
      'ctr'         	=> number_format($table_val['ctr']*100, 2, ".", ",")." %",
      'cpm'         	=> "Rp. ".number_format($table_val['cpm'], 2, ",", "."),
      'cpc' 	  			=> number_format($table_val['cpc'], 0, '.', ','),
    ));
  }

  $data = array(
    'platform_name' => 'Publisher'
    ,'th'           => $table_th
    ,'temp_table'   => $value
  );

  $data = $this->load->view('admin/table', $data, TRUE);
  echo json_encode($data);
}

  // Dashboard Chart Code
  function open_dashboard_chart(){
    $kurs					= "Rp.";
    $id_brand     = $_POST['id_brand'];
    $id_campaign  = $_POST['id_campaign'];
    $id_platform  = $_POST['id_platform'];
    $date         = $_POST['date'];//05/11/2016 - 05/11/2016
    $date_from    = date('Y-m-d',strtotime(substr($date, 0,10)));
    $date_to      = date('Y-m-d',strtotime(substr($date, 13, 22)));
    $tanggal      = $this->period($date_from, $date_to);

    if ($id_platform == 1) {
      if (empty($_POST['id_type'])) {
        $id_type  = '';
        $targeting  = '';

        $type_1 = $this->m_dashboard->type($id_brand, $id_campaign, $id_platform);
        $device = array();
        $count  = COUNT($type_1);

        # Check ada berapa device
        if ($count < 3 && $count > 1) {
          # looping data
          foreach ($type_1 as $key => $v) {
            if ($v['device'] != 'ALL') {
              array_push($device, $v['device']);
             }
          }
          $type     = RESET($device);
          $targeting  = $this->m_dashboard->get_targeting($id_brand, $id_campaign, $id_platform, $type);
          $id_type  = $type;
        }
        if ($count < 2) {
          $device   = $type_1[0]['device'];
          $targeting  = $this->m_dashboard->get_targeting($id_brand, $id_campaign, $id_platform, $device);
          $id_type  = $device;
        }
        if ($count >= 3) {
        #PILIH HANYA DATA ALL
          foreach ($type_1 as $key => $v) {
            if ($v['device'] == 'ALL') {
              array_push($device, $v['device']);
            }
          }
          $type     = RESET($device);
          $targeting  = $this->m_dashboard->get_targeting($id_brand, $id_campaign, $id_platform, $type);
          $id_type  = $type;
        }

        $targeting2 = array();
        $targeting3 = array();
        $count    = COUNT($targeting);

        # Check ada berapa TARGETING
        if ($count < 3 && $count > 1) {
          # looping data
          foreach ($targeting as $key => $v) {
            if ($v['targeting'] != 'ALL') {
              array_push($targeting2, $v['targeting']);
             }
          }
          $targeting = reset($targeting2);
        }
        if ($count < 2) {
          $targeting2 = $targeting[0]['targeting'];
          $targeting  = $targeting2;
        }
        if ($count >= 3) {
          foreach ($targeting as $key => $v) {
						# Masukkan hanya ALL
						if ($v['targeting']=='ALL') {
							array_push($targeting2, $v['targeting']);
						}
					}
					$targeting = RESET($targeting2);
        }
      }
      elseif(!empty($_POST['id_type']) && empty($_POST['targeting'])) {
        $id_type  = $_POST['id_type'];
        $targeting  = '';
        $targeting2 = array();
        $targeting  = $this->m_dashboard->get_targeting($id_brand, $id_campaign, $id_platform, $id_type);

        $count    = COUNT($targeting);

        # Check ada berapa device
        if ($count < 3 && $count > 1) {
          # looping data
          foreach ($targeting as $key => $v) {
            if ($v['targeting'] != 'ALL') {
              array_push($targeting2, $v['targeting']);
             }
          }
          $targeting = reset($targeting2);
        }
        if ($count < 2) {
          $targeting2 = $targeting[0]['targeting'];
          $targeting  = $targeting2;
        }
        if ($count >= 3) {
          foreach ($targeting as $key => $v) {
            if ($v['targeting'] == 'ALL') {
              array_push($targeting2, $v['targeting']);
            }
          }
          $targeting = RESET($targeting2);
        }
        // print_r($targeting2); die();#DEBIG
      }
      else{
        $id_type    = $_POST['id_type'];
        $targeting  = $_POST['targeting'];
      }

      // echo '<pre/>'; print_r($id_type.' '.$targeting); die();//DEBUG
      $dt_dashboard  = $this->m_dashboard->data_dashboard2($id_brand, $id_campaign, $id_platform, $date_from, $date_to, $id_type, $targeting);
    }
    else{
      if (!empty($_POST['id_type'])) {
        $id_type     = $_POST['id_type'];
        $targeting   = $_POST['targeting'];
      }

      if (!empty($id_type) AND !empty($targeting)) {
        $dt_dashboard   = $this->m_dashboard->data_dashboard2($id_brand, $id_campaign, $id_platform, $date_from, $date_to, $id_type, $targeting);
        }
        else{
        $dt_dashboard   = $this->m_dashboard->data_dashboard($id_brand, $id_campaign, $id_platform, $date_from, $date_to);
        }
    }

    // $period = $this->createDateRangeArray($date_from, $date_to);
    $platform_dt = call_user_func_array('array_merge', $this->m_upload->platform($id_platform));
    $campaign_nm = call_user_func_array('array_merge', $this->m_upload->name_campaign($id_campaign));

    $count_dash = array();
    $table_val  = array();

    //KETERANGAN TABLE
    $ket_table = array('Date', 'Device', 'Targeting', 'Cost', 'Impression', 'Clicks', 'CTR', 'Engagement', 'Page likes', 'Followers', 'Views video', 'Views Rate', 'Conversions', 'CVR', 'CPC','CPM','CPE','CPL','CPV', 'CPA', 'Install', 'Install Rate', 'CPI', 'Ad Request', 'Fill Rate');

		foreach ($dt_dashboard as $key => $value) {
			array_push($table_val, array(
											$value['date'],
											$value['device'],
											$value['targeting'],
											number_format($value['cost'], 2, '.', ''),
											$value['impression'],
											$value['clicks'],
											number_format($value['ctr']*100, 2, '.', ''),
											$value['engagement'],
											$value['page_likes'],
											$value['followers'],
											$value['views_video'],
											$value['view_rates'],
											$value['conversions'],
											number_format($value['cvr'], 2, '.', ''),
											number_format($value['cpc'], 2, '.', ''),
											number_format($value['cpm'], 2, '.', ''),
											number_format($value['cpe'], 2, '.', ''),
											number_format($value['cpl'], 2, '.', ''),
											number_format($value['cpv'], 2, '.', ''),
											number_format($value['cpa'], 2, '.', ''),
											number_format($value['install'], 2, '.', ''),
											number_format($value['install_rate'], 2, '.', ''),
											number_format($value['cpi'], 2, '.', ''),
											number_format($value['ad_request'], 2, '.', ''),
											number_format($value['fill_rate'], 2, '.', ''),
										)
					);
		}

    if ($platform_dt['platform'] == 'Programmatic') {
      $result = array(0, 0, 0, 0, 0, 0, 0);
      $ket_dash_1 = array('Cost','Impression','Clicks','CTR', 'Engagement', 'Eng_Rate','Conversions', 'View Video', 'View Rates', 'CPV','CVR','CPC','CPM','CPA', 'CPE', 'Session');

        foreach ($dt_dashboard as $key => $value) {
          array_push($count_dash, array(
            $value['cost'],//0
            $value['impression'],//1
            $value['clicks'],//2
            intval($value['conversions']),//3
            $value['engagement'],//4
            $value['views_video'],//5
            $value['session']//6
          ));
        }

        //UNTUK SUM DATA DARI ARRAY KE ARRAY
        foreach ($count_dash as $row)
        {
          foreach ($row as $key => $value)
          {
            $result[$key] += $value;
          }
        }

      //KONDISI UNTUK MENGHINDARI  NILAI TAK HINGGA
      if ($result[1] != 0) {
        $CTR = $result[2]/$result[1]*100; //CLICK / IMPRESSIONS
      }
      else {
        $CTR = 0;
      }

      if ($result[2] != 0) {
        $CPC = $result[0]/$result[2];
      }
      else{
        $CPC = 0;
      }

      if ($result[1] != 0) {
        $CPM = ($result[0]/$result[1])*1000;
      }
      else{
        $CPM = 0;
      }

      if ($result[3] != 0) {
        $CPA = $result[0]/$result[3];
      }
      else{
        $CPA = 0;
      }

      if ($result[3] != 0) {
        $CVR = ($result[3]/$result[2])*100;
      }
      else{
        $CVR = 0;
      }

      if ($result[4] != 0) {
        $ER = ($result[4]/$result[1])*100;//Eng Rate = Enggament / immpression
      }
      else{
        $ER = 0;
      }

      if ($result[4] != 0) {
        $CPE = ($result[0]/$result[4]);//Eng Rate = Cost / Enggament
      }
      else{
        $CPE = 0;
      }

      if ($result[5] != 0) {
        $CPV = ($result[0]/$result[5]);//CPV = Cost / View Video
      }
      else{
        $CPV = 0;
      }

      if ($result[1] != 0) {
        $view_rates = ($result[5]/$result[1]);//view rates = view video / impression
      }
      else{
        $view_rates = 0;
      }
      
      $last = array(
        $kurs.number_format($result[0], 2, ".", ","),
        number_format($result[1], 0, '.', ','),
        number_format($result[2], 0, '.', ','),
        number_format($CTR, 2, ".", ",")." %",
        number_format($result[4], 0, ".", ","),
        number_format($ER, 0, ".", ",")." %",
        number_format($result[3], 0, ".", ","),
        number_format($result[5], 0, '.', ','),
        number_format($view_rates*100, 2, ".", ",")." %",
        $kurs.number_format($CPV, 2, ".", ","), //FOR CPV
        number_format($CVR, 2, ".", ",")." %",
        $kurs.number_format($CPC, 2, ".", ","), //FOR CPC
        $kurs.number_format($CPM, 2, ".", ","), //FOR CPM
        $kurs.number_format($CPA, 2, ".", ","), // FOR CPA
        $kurs.number_format($CPE, 2, ".", ","), // FOR CPE
        number_format($result[6], 0, ".", ","), // FOR Session
      );
    }
    elseif ($platform_dt['platform'] == 'Facebook') {
      $result = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			$ket_dash_1 = array('Cost','Impression','Clicks','CTR','Engagement','Page_Likes','views_video', 'View Rates','Conversions','Leads', 'Reach', 'CVR','CPC','CPM','CPE','CPL','CPV', 'CPA','CPLeads','CPReach', 'adRecall', 'Session');

			foreach ($dt_dashboard as $key => $value) {
				array_push($count_dash, array(
            $value['cost'],//0
            $value['impression'],//1
            $value['clicks'],//2
            // number_format($value['ctr']*100, 2, '.', ''),
            $value['engagement'],//3
            $value['page_likes'],//4
            $value['views_video'],//5
            $value['conversions'],//6
            $value['leads'],//7
            $value['cpleads'],//8
            $value['reach'],//9
            $value['cpreach'],//10
            $value['adRecall'],//11
            $value['session'],//12
          )
				);
			}

			//UNTUK SUM DATA DARI ARRAY KE ARRAY
			foreach ($count_dash as $row)
			{
			  foreach ($row as $key => $value)
			  {
			      $result[$key] += $value;
			  }
			}
			//KONDISI UNTUK MENGHINDARI NILAI TAK HINGGA
			if ($result[1] != 0) {
				$CTR = $result[2]/$result[1]*100; //CLICK / IMPRESSIONS * 100
			}
			else {
				$CTR = 0;
			}

			if ($result[2] != 0) {
				$CPC = $result[0]/$result[2]; //FOR CPC (COST / CLICK)
			}
			else{
				$CPC = 0;
			}

			if ($result[1] != 0) {
				$CPM = ($result[0]/$result[1])*1000; //FOR CPM (COST / IMPRESSIONS)
			}
			else{
				$CPM = 0;
			}

			if ($result[3] != 0) {
				$CPE = $result[0]/$result[3]; // FOR CPE (COST / ENGAGEMENT)
			}
			else{
				$CPE = 0;
			}

			if ($result[5] != 0) {
				$CPV = $result[0]/$result[5]; // FOR CPV (COST / VIDEO VIEW)
			}
			else {
				$CPV = 0;
			}

			if($result[4] != 0){
				$CPL = $result[0]/$result[4]; // FOR CPL (COST / PAGE LIKES)
			}
			else{
				$CPL = 0;
			}

			if ($result[6] != 0) {
				$CVR = $result[6]/$result[2]*100;# FOR CVR (conversions / click)
			}
			else{
				$CVR = 0;
			}

			if ($result[6] != 0) {
				$CPA = $result[0]/$result[6];# CPA (Cost / Conversions)
			}
			else{
				$CPA = 0;
			}

			if ($result[1] != 0) {
				$view_rates = $result[5]/$result[1];# View Rates (View Rates / Impression)
			}
			else{
				$view_rates = 0;
			}

			$last = array(
        $kurs.number_format($result[0], 2, ".", ","),
        number_format($result[1], 0, '.', ','),
        number_format($result[2], 0, '.', ','),
        // number_format($result[3], 2, ".", ",")." %",
        number_format($CTR, 2, ".", ",")." %",
        number_format($result[3], 0, ".", ","),
        number_format($result[4], 0, ".", ","),
        number_format($result[5], 0, ".", ","),//View
        number_format($view_rates*100, 2, ".", ",")." %",//View Rates
        number_format($result[6], 0, ".", ","),//CONVERSION
        number_format($result[7], 0, ".", ","),//Leads
        number_format($result[9], 0, ".", ","),//Leads
        number_format($CVR, 2, ".", ",")." %",// CVR (conversions / click)
        $kurs.number_format($CPC, 2, ".", ","),//FOR CPC (COST / CLICK)
        $kurs.number_format($CPM, 2, ".", ","),//FOR CPM (COST / IMPRESSIONS)
        $kurs.number_format($CPE, 2, ".", ","),// FOR CPE (COST / ENGAGEMENT)
        $kurs.number_format($CPL, 2, ".", ","),// FOR CPL (COST / PAGE LIKES)
        $kurs.number_format($CPV, 2, ".", ","), // FOR CPV (COST / VIDEO VIEW)
        $kurs.number_format($CPA, 2, ".", ","), // CPA (Cost / Conversions)
        $kurs.number_format($result[8], 2, ".", ","), // CPLeads
        $kurs.number_format($result[10], 2, ".", ","), // CPReach
        number_format($result[11], 0, ".", ","), // adRecall
        number_format($result[12], 0, ".", ","), // Session
      );
    }
    elseif ($platform_dt['platform'] == 'Instagram') {
      $result = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
      $ket_dash_1 = array('Cost','Impression','Clicks','CTR','Engagement','views_video','Leads', 'Reach','CPC','CPM','CPE','CPV','CPLeads', 'CPReach', 'Session');

      foreach ($dt_dashboard as $key => $value) {
      array_push($count_dash, array(
          $value['cost'], //0
          $value['impression'], //1
          $value['clicks'], //2
          $value['engagement'], //3
          $value['views_video'], //4
          $value['leads'],//5
          $value['cpleads'],//6
          $value['reach'],//7
          $value['cpreach'],//8
          $value['adRecall'],//9
          $value['session'],//10
        ));
      }
      //UNTUK SUM DATA DARI ARRAY KE ARRAY
      foreach ($count_dash as $row)
      {
        foreach ($row as $key => $value)
        {
            $result[$key] += $value;
        }
      }

      //KONDISI UNTUK MENGHINDARI  NILAI TAK HINGGA
      if ($result[1] != 0) {
        $CTR = $result[2]/$result[1]*100; //CLICK / IMPRESSIONS
      }
      else {
        $CTR = 0;
      }

      if ($result[2] != 0) {
        $CPC = $result[0]/$result[2]; //COST / CLICK
      }
      else{
        $CPC = 0;
      }

      if ($result[1] != 0) {
        $CPM = ($result[0]/$result[1])*1000; //COST / IMPRESSION * 1000
      }
      else{
        $CPM = 0;
      }

      if ($result[3] != 0) {
        $CPE = $result[0]/$result[3]; //COST / ENGAGEMENT
      }
      else{
        $CPE = 0;
      }

      if ($result[4] != 0) {
        $CPV = $result[0]/$result[4]; //COST / VIEWS
      }
      else {
        $CPV = 0;
      }

      $last = array(
        $kurs.number_format($result[0], 2, ".", ","),//COST
        number_format($result[1], 0, '.', ','),//IMPRESSIONS
        number_format($result[2], 0, '.', ','),//CLICKS
        number_format($CTR, 2, ".", ",")." %",//CTR
        number_format($result[3], 0, ".", ","),//ENGAGEMENT
        number_format($result[4], 0, ".", ","),//VIEWS
        number_format($result[5], 0, ".", ","),//Leads
        number_format($result[7], 0, ".", ","),//Reach
        $kurs.number_format($CPC, 2, ".", ","), //FOR CPC (COST / CLICK)
        $kurs.number_format($CPM, 2, ".", ","), //FOR CPM (COST / IMPRESSIONS)
        $kurs.number_format($CPE, 2, ".", ","), // FOR CPE (COST / ENGAGEMENT)
        $kurs.number_format($CPV, 2, ".", ","), // FOR CPV (COST / VIDEO VIEW)
        $kurs.number_format($result[6], 2, ".", ","), // FOR CPLeads
        $kurs.number_format($result[8], 2, ".", ","), // FOR CPReach
        number_format($result[9], 0, ".", ","), // FOR adRecall
        number_format($result[10], 0, ".", ","), // FOR Session
      );
    }
    elseif ($platform_dt['platform'] == 'Twitter') {
      $result = array(0, 0, 0, 0, 0, 0, 0);
      $ket_dash_1 = array('Cost','Impression','Clicks','CTR','Engagement', 'Followers','views_video', 'CPC','CPM','CPF', 'Session');

      foreach ($dt_dashboard as $key => $value) {
        array_push($count_dash, array(
          $value['cost'],//0
          $value['impression'],//1
          $value['clicks'],//2
          // number_format($value['ctr']*100, 2, '.', ''),
          $value['engagement'],//3
          $value['followers'],//4
          $value['view_rates'],//5
          $value['session']//6
        ));
      }
      //UNTUK SUM DATA DARI ARRAY KE ARRAY
      foreach ($count_dash as $row)
      {
        // echo '<pre/>'; print_r($row);
          foreach ($row as $key => $value)
          {
            $result[$key] += $value;
          }
      }

      //KONDISI UNTUK MENGHINDARI  NILAI TAK HINGGA
      if ($result[1] != 0) {
        $CTR = $result[2]/$result[1]*100; //CLICK / IMPRESSIONS * 100
      }
      else {
        $CTR = 0;
      }

      if ($result[2] != 0) {
        $CPC = $result[0]/$result[2]; //COST / CLICK
      }
      else{
        $CPC = 0;
      }

      if ($result[1] != 0) {
        $CPM = ($result[0]/$result[1])*1000;//COST / IMPRESSION * 1000
      }
      else{
        $CPM = 0;
      }

      if ($result[4] != 0) {
        $CPF = $result[0]/$result[4];//COST / FOLLOWER
      }
      else{
        $CPF = 0;
      }

      $last = array(
        $kurs.number_format($result[0], 2, ".", ","),//COST
        number_format($result[1], 0, '.', ','),//IMPRESSIONS
        number_format($result[2], 0, '.', ','),//CLICK
        // number_format($result[3], 2, ".", ",")." %",
        number_format($CTR, 2, ".", ",")." %",//CLICK / IMPRESSIONS
        number_format($result[3], 0, ".", ","),//ENGAGEMENT
        number_format($result[4], 0, ".", ","),//FOLLOWER
        number_format($result[5], 0, ".", ","),//VIEWS
        $kurs.number_format($CPC, 2, ".", ","), //FOR CPC (COST / CLICK)
        $kurs.number_format($CPM, 2, ".", ","), //FOR CPM (COST / IMPRESSIONS)
        $kurs.number_format($CPF, 2, ".", ","), // FOR CPL (COST / FOLLOWER)
        number_format($result[6], 2, ".", ",") // FOR Session
      );
    }
    elseif ($platform_dt['platform'] == 'Native') {
      $result = array(0, 0, 0, 0);
      $ket_dash_1 = array('Cost','Impression','Clicks','CTR','CPC','CPM', 'Session');

      foreach ($dt_dashboard as $key => $value) {
        array_push($count_dash, array(
          $value['cost'],//0
          $value['impression'],//1
          $value['clicks'],//2
          $value['session'],//3
        ));
      }
      // print_r($dt_dashboard); die();
      //UNTUK SUM DATA DARI ARRAY KE ARRAY
      foreach ($count_dash as $row)
      {
          foreach ($row as $key => $value)
          {
            $result[$key] += $value;
          }
      }

      //KONDISI UNTUK MENGHINDARI  NILAI TAK HINGGA
      if ($result[1] != 0) {
        $CTR = $result[2]/$result[1]*100; //CLICK / IMPRESSIONS * 100
      }
      else {
        $CTR = 0;
      }

      if ($result[2] != 0) {
        $CPC = $result[0]/$result[2]; //COST / CLICK
      }
      else{
        $CPC = 0;
      }

      if ($result[1] != 0) {
        $CPM = ($result[0]/$result[1])*1000;//COST / IMPRESSION * 1000
      }
      else{
        $CPM = 0;
      }

      $last = array(
        $kurs.number_format($result[0], 2, ".", ","),//COST
        number_format($result[1], 0, '.', ','),//IMPRESSIONS
        number_format($result[2], 0, '.', ','),//CLICK
        number_format($CTR, 2, ".", ",")." %",//CLICK / IMPRESSIONS
        $kurs.number_format($CPC, 2, ".", ","), //FOR CPC (COST / CLICK)
        $kurs.number_format($CPM, 2, ".", ","), //FOR CPM (COST / IMPRESSIONS)
        number_format($result[3], 0, ".", ","), //FOR Session
      );
    }
    elseif ($platform_dt['platform'] == 'YouTube' or $platform_dt['platform'] == 'Video Network') {
      $result = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
      $ket_dash_1 = array('Cost','Impression','Clicks', 'views_video', 'Views Rate','CTR','CPC','CPM', 'CPV', 'Session');

      foreach ($dt_dashboard as $key => $value) {
        array_push($count_dash, array(
          $value['cost'],//0
          $value['impression'],//1
          $value['clicks'],//2
          $value['views_video'], //3
          $value['session']//4
        ));
      }

      //UNTUK SUM DATA DARI ARRAY KE ARRAY
      foreach ($count_dash as $row)
      {
        foreach ($row as $key => $value)
        {
          $result[$key] += $value;
        }
      }

      //KONDISI UNTUK MENGHINDARI  NILAI TAK HINGGA
      if ($result[1] != 0) {
        $CTR = $result[2]/$result[1]*100; //CLICK / IMPRESSIONS * 100
      }
      else {
        $CTR = 0;
      }

      if ($result[2] != 0) {
        $CPC = $result[0]/$result[2]; //COST / CLICK
      }
      else{
        $CPC = 0;
      }

      if ($result[1] != 0) {
        $CPM = ($result[0]/$result[1])*1000;//COST / IMPRESSION * 1000
      }
      else{
        $CPM = 0;
      }

      if ($result[3] != 0) {
        $CPV = $result[0]/$result[3]; //COST / VIEWS
      }
      else {
        $CPV = 0;
      }

      if ($result[1] != 0) {
        $view_rates = $result[3]/$result[1]; //View Video / Impression
      }
      else {
        $view_rates = 0;
      }

      $last = array(
        $kurs.number_format($result[0], 2, ".", ","),//COST
        number_format($result[1], 0, '.', ','),//IMPRESSIONS
        number_format($result[2], 0, '.', ','),//CLICK
        number_format($result[3], 0, '.', ','),//vIEW VIDEO
        number_format($view_rates*100, 2, ".", ",")." %",//vIEW RATES
        number_format($CTR, 2, ".", ",")." %",//CLICK / IMPRESSIONS
        $kurs.number_format($CPC, 2, ".", ","), //FOR CPC (COST / CLICK)
        $kurs.number_format($CPM, 2, ".", ","), //FOR CPM (COST / IMPRESSIONS)
        $kurs.number_format($CPV, 2, ".", ","), //FOR CPV (COST / VIEWS)
        number_format($result[3], 0, ".", ","), //FOR Session
      );
    }
    elseif ($platform_dt['platform'] == 'CPI') {
      $result = array(0, 0, 0, 0, 0, 0, 0, 0);
      $ket_dash_1 = array('Cost','Impression','Clicks', 'Install', 'install_Rate', 'CTR','CPI', 'Session');

      foreach ($dt_dashboard as $key => $value) {
        array_push($count_dash, array(
          $value['cost'],//0
          $value['impression'],//1
          $value['clicks'],//2
          $value['install'], //3
          $value['install_rate'],//4
          $value['session']//5
        ));
      }
      // echo '<pre/>'; print_r($count_dash);
      //UNTUK SUM DATA DARI ARRAY KE ARRAY
      foreach ($count_dash as $row)
      {
          foreach ($row as $key => $value)
          {
            $result[$key] += $value;
          }
      }

      //KONDISI UNTUK MENGHINDARI  NILAI TAK HINGGA
      if ($result[1] != 0) {
        $CTR = $result[2]/$result[1]*100; //CLICK / IMPRESSIONS * 100
      }
      else {
        $CTR = 0;
      }

      if ($result[3] != 0) {
        $CPI = $result[0]/$result[3]; //COST / INSTALL
      }
      else{
        $CPI = 0;
      }


      # JUST FOR JAPAN CLIENT MAKE YEN CURRENCY
      if ($_SESSION['u_id'] == 60) {
      // if ($_SESSION['u_name'] == 'rezaakbar') {
        $last = array(
          "Ұ ".number_format($result[0], 2, ".", ","),//COST
          number_format($result[1], 0, '.', ','),//IMPRESSIONS
          number_format($result[2], 0, '.', ','),//CLICK
          number_format($result[3], 0, '.', ','),//INSTALL
          number_format($result[4]*100, 2, ".", ",")." %",//INSTALL RATES
          number_format($CTR, 2, ".", ",")." %",//CTR (CLICK / IMPRESSIONS)
          "Ұ ".number_format($CPI, 2, ".", ","), //FOR CPI (COST / INSTALL)
          number_format($result[5], 0, ".", ","), //FOR Session
        );
      }
      else{
        $last = array(
          $kurs.number_format($result[0], 2, ".", ","),//COST
          number_format($result[1], 0, '.', ','),//IMPRESSIONS
          number_format($result[2], 0, '.', ','),//CLICK
          number_format($result[3], 0, '.', ','),//INSTALL
          number_format($result[4], 2, ".", ",")." %",//INSTALL RATES
          number_format($CTR, 2, ".", ",")." %",//CTR (CLICK / IMPRESSIONS)
          $kurs.number_format($CPI, 2, ".", ","), //FOR CPI (COST / INSTALL)
          number_format($result[5], 2, ".", ","), //FOR Session
        );
      }

    }
    elseif ($platform_dt['platform'] == 'Publisher') {
      $result = array(0, 0, 0, 0, 0, 0);
      $ket_dash_1 = array('Revenue','ad_request','Impression', 'fill_rate', 'Clicks', 'CTR', 'CPM','CPC', 'Session');

      foreach ($dt_dashboard as $key => $value) {
				array_push($count_dash, array(
          $value['cost'],//0
          $value['ad_request'],//1
          $value['impression'],//2
          $value['fill_rate'], //3
          $value['clicks'],//4
          $value['session'],//5
        ));
			}

      //UNTUK SUM DATA DARI ARRAY KE ARRAY
      foreach ($count_dash as $row)
      {
        foreach ($row as $key => $value)
        {
          $result[$key] += $value;
        }
      }

			if ($result[1] != 0) {
				$CTR = $result[2]/$result[1]*100; //CLICK / IMPRESSIONS * 100
			}
			else {
				$CTR = 0;
			}

			if ($result[1] != 0) {
				$CPM = ($result[0]/$result[2])*1000;//COST / IMPRESSION * 1000
			}
			else{
				$CPM = 0;
			}

			if ($result[2] != 0) {
				$CPC = $result[0]/$result[4]; //COST / CLICK
			}
			else{
				$CPC = 0;
			}

			if ($result[3]!=0) {
				$FR = ($result[2]/$result[1])*100;// impression / ad request * 100
			}
			else{
				$FR = 0;
			}

      $last = array(
        $kurs.number_format($result[0], 2, ".", ","),//COST (REVENUE)
        number_format($result[1], 0, '.', ','),//ADD REQUEST
        number_format($result[2], 0, '.', ','),//IMPRESSION
        number_format($FR, 2, ".", ",")." %",//FILL RATE
        number_format($result[4], 0, '.', ','),//CLICK
        number_format($CTR, 2, ".", ",")." %",//CTR (CLICK / IMPRESSIONS)
        $kurs.number_format($CPM, 2, ".", ","), //CPM (COST / IMPRESSION)
        $kurs.number_format($CPC, 2, ".", ","), //CPC (COST / CLICK)
        number_format($result[5], 2, ".", ","), //CPC Session
      );
    }
    elseif ($platform_dt['platform'] == 'SEM') {
			$result = array(0, 0, 0, 0, 0, 0);
	  	$ket_dash_1 = array('Cost','Impression','Clicks', 'Conversions', 'CPC', 'CPM', 'CPA','CVR', 'Session');

			foreach ($dt_dashboard as $key => $value) {
				array_push($count_dash, array(
										$value['cost'],//0
										$value['impression'],//1
										$value['clicks'],//2
										$value['conversions'], //3
										$value['session'], //4
									)
				);
			}
			//UNTUK SUM DATA DARI ARRAY KE ARRAY
			foreach ($count_dash as $row)
			{
			  	foreach ($row as $key => $value)
			  	{
			      $result[$key] += $value;
			  	}
			}

			if ($result[2] != 0) {
				$CPC = $result[0]/$result[2]; //COST / CLICK
			}
			else{
				$CPC = 0;
			}

			if ($result[1] != 0) {
				$CPM = ($result[0]/$result[1])*1000;//COST / IMPRESSION * 1000
			}
			else{
				$CPM = 0;
			}

			if ($result[2] != 0) {
				$CVR = $result[3]/$result[2]*100;# FOR CVR (conversions / click)
			}
			else{
				$CVR = 0;
			}

			if ($result[3] != 0) {
				$CPA = $result[0]/$result[3];# CPA (Cost / Conversions)
			}
			else{
				$CPA = 0;
			}

			$last = array(
        $kurs.number_format($result[0], 2, ".", ","),//COST (REVENUE)
        number_format($result[1], 0, '.', ','),//IMPRESSION
        number_format($result[2], 0, '.', ','),//CLICK
        number_format($result[3], 0, '.', ','),//CONVERSION
        $kurs.number_format($CPC, 2, ".", ","), //CPC (COST / CLICK)
        $kurs.number_format($CPM, 2, ".", ","), //CPM (COST / IMPRESSION)
        $kurs.number_format($CPA, 2, ".", ","), // CPA (Cost / Conversions)
        number_format($CVR, 2, ".", ",")." %",// CVR (conversions / click)
        number_format($result[4], 0, ".", ","),// Session
      );
		}
    elseif ($platform_dt['platform'] == 'Tiktok') {

			foreach ($dt_dashboard as $key => $value) {
				if ($value['view_6s'] != 0) {
					$viewSecond = 'View 6s';
					$rateSecond = 'View Rates 6s';
					$viewSTable = 'view_6s';
				}
				else if($value['view_2s'] != 0){
					$viewSecond = 'View 2s';
					$rateSecond = 'View Rates 2s';
					$viewSTable = 'view_2s';
				}
			}

			$result = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
			$ket_dash_1 = array('Cost','Impression','Clicks', 'CTR', 'Conversions', 'Engagement', 'Reach', 
			'Session', $viewSecond, $rateSecond, 'Video Likes', 'Comments', 'Follower', 'Shares', 'Profile Visit');

			foreach ($dt_dashboard as $key => $value) {
				array_push($count_dash, array(
					$value['cost'],//Cost [0]
					$value['impression'],//Impressions [1]
					$value['clicks'],//Clicks [2]
					$value['ctr'], // [3]
					$value['conversions'],// [4]
					$value['engagement'],// [5]
					$value['reach'],// [6]
					$value['session'],// [7]
					$value[$viewSTable],//[8]
					$value['viewRates_2_6_s'],//[9]
					$value['videoLikes'],//[10]
					$value['comment'],//[11]
					$value['followers'],//[12]
					$value['shares'],//[13]
					$value['profileVisits']//[14]
				)
				);
			}

			//UNTUK SUM DATA DARI ARRAY KE ARRAY
			foreach ($count_dash as $row)
			{
				foreach ($row as $key => $value){
					$result[$key] += $value;
				}
			}

			if ($result[1] != 0) {
				$CTR = $result[2]/$result[1]*100; //CLICK / IMPRESSIONS * 100
			}
			else {
				$CTR = 0;
			}
			
			// CALCULATION VALUE
			$last = array(
				"Rp. ".number_format($result[0], 0, ".", ","),//COST
				number_format($result[1], 0, '.', ','),//IMPRESSIONS
				number_format($result[2], 0, '.', ','),//CLICK
				number_format($CTR, 2, ".", ",")."%",//CLICK / IMPRESSIONS
				number_format($result[4], 0, ".", ","), //Conversions
				number_format($result[5], 0, ".", ","), //Engagement
				number_format($result[6], 0, ".", ","), //Reach
				number_format($result[7], 0, ".", ","), //Session
				number_format($result[8], 0, ".", ","), //View 2s/6s
				number_format($result[9]*100, 2, ".", ",")."%", //View Rate 2s/6s
				number_format($result[10], 0, ".", ","), //Video Likes
				number_format($result[11], 0, ".", ","), //Comments
				number_format($result[12], 0, ".", ","), //Follower
				number_format($result[13], 0, ".", ","), //Shares
				number_format($result[14], 0, ".", ","), //Provile Visits
			);
		}

    if (empty($dt_dashboard)) {
      $message = array(
        'valid'     => 'kosong',
        'message'   => 'Data Tidak Ditemukan !'
      );

      $this->session->set_flashdata('message',$message);
      redirect('admin/dashboard_admin');
    }
    else{
      $ket_chart = array();
      $dash 	   = array();
      $table 	   = array();

      array_push($ket_chart,  array(
        'platform'   => $platform_dt['platform'],
        'campaign'   => $campaign_nm['name'],
        'from'		    => $tanggal['period_f'],
        'to'			    => $tanggal['period_t'],
        'ket_dash_1'	=> $ket_dash_1,
        'ket_dash_2'	=> $last
      ));

      array_push($table,  array(
        'ket'   	 => $ket_table,
        'value'   => $table_val
      ));

      foreach ($dt_dashboard as $key => $dt_dashboard) {
        $tgl  = $dt_dashboard['date'];
        $date = $this->chart_date($tgl);

        if ($platform_dt['platform'] == 'Tiktok') {
					if ($dt_dashboard['view_6s'] != 0) {
						$viewSecond = 'view_6s';
					}
					else if($dt_dashboard['view_2s'] != 0){
						$viewSecond = 'view_2s';
					}
				}
				else {
					$viewSecond = 'view_6s';
				}
        
        array_push($dash,  array(
          'id_dashboard'   => $dt_dashboard['id_dashboard'],
          'id_brand'       => $dt_dashboard['id_brand'],
          'id_campaign'    => $dt_dashboard['id_campaign'],
          'id_platform'    => $dt_dashboard['id_platform'],
          'date'       	  => $date['tgl'],
          'device'		      => $dt_dashboard['device'],
          'targeting'      => $dt_dashboard['targeting'],
          'Cost'       	  => number_format($dt_dashboard['cost'], 2, '.', ''),
          'Impression'     => $dt_dashboard['impression'],
          'Clicks'       	=> $dt_dashboard['clicks'],
          'CTR'       		  => number_format($dt_dashboard['ctr']*100, 2, '.', ''),
          'Engagement'     => $dt_dashboard['engagement'],
          'Eng_Rate'       => number_format($dt_dashboard['eng_rate']*100, 2, '.', ''),
          'Page_Likes'     => $dt_dashboard['page_likes'],
          'Likes_Rate'     => number_format($dt_dashboard['like_rates']*100, 2, '.', ''),
          'Followers'      => $dt_dashboard['followers'],
          'Follow_Rate'    => number_format($dt_dashboard['follow_rates']*100, 2, '.', ''),
          'views_video'    => $dt_dashboard['views_video'],
          'Views_Rate'     => number_format($dt_dashboard['view_rates']*100, 2, '.', ''),
          'Conversions'   => $dt_dashboard['conversions'],
          'CVR'       		  => number_format($dt_dashboard['cvr'], 2, '.', ''),
          'CPC'       		  => number_format($dt_dashboard['cpc'], 2, '.', ''),
          'CPM'       		  => number_format($dt_dashboard['cpm'], 2, '.', ''),
          'CPE'       		  => number_format($dt_dashboard['cpe'], 2, '.', ''),
          'CPL'       		  => number_format($dt_dashboard['cpl'], 2, '.', ''),
          'CPF'       		  => number_format($dt_dashboard['cpf'], 2, '.', ''),
          'CPV'       		  => number_format($dt_dashboard['cpv'], 2, '.', ''),
          'CPA'       		  => number_format($dt_dashboard['cpa'], 2, '.', ''),
          'Install'        => number_format($dt_dashboard['install'], 2, '.', ''),
          'install_Rate'   => number_format($dt_dashboard['install_rate']*100, 2, '.', ''),
          'CPI'            => number_format($dt_dashboard['cpi'], 2, '.', ''),
          'ad_request'   	=> number_format($dt_dashboard['ad_request']*100, 2, '.', ''),
          'fill_rate'      => number_format($dt_dashboard['fill_rate'], 2, '.', ''),
          'CPLeads'      		=> number_format($dt_dashboard['cpleads'], 2, '.', ''),
					'CPReach'      		=> number_format($dt_dashboard['cpreach'], 2, '.', ''),
					'adRecall'				=> number_format($dt_dashboard['adRecall'], 0, '.', ''),
					'session'					=> number_format($dt_dashboard['session'], 0, '.', ''),
					'viewSecond'			=> number_format($dt_dashboard[$viewSecond], 0, '.', ''),
					'viewSecondRate'	=> number_format($dt_dashboard['viewRates_2_6_s']*100, 2, '.', ''),
					'videoLikes'			=> number_format($dt_dashboard['videoLikes'], 0, '.', ''),
					'comment'					=> number_format($dt_dashboard['comment'], 0, '.', ''),
					'shares'					=> number_format($dt_dashboard['shares'], 0, '.', ''),
					'profileVisits'		=> number_format($dt_dashboard['profileVisits'], 0, '.', ''),
        ));
      }

      $data['ket']	= $ket_chart;
      $data['dash']	= $dash;
      $data['table']	= $table;

      echo json_encode($data);
    }
	}

  // Dashboard Table Code
	function table_dashboard(){
    $kurs         = "Rp.";
		$id_brand     = $_POST['id_brand'];
    $id_campaign  = $_POST['id_campaign'];
    $id_platform  = $_POST['id_platform'];
    $date         = $_POST['date'];//05/11/2016 - 05/11/2016
    $date_from    = date('Y-m-d',strtotime(substr($date, 0,10)));
    $date_to      = date('Y-m-d',strtotime(substr($date, 13, 22)));
    $tanggal      = $this->period($date_from, $date_to);//Untuk dapet format tanggal "DD MON, YEAR"
    $brand        = call_user_func_array('array_merge', $this->m_dashboard->get_brand($id_brand));
    $campaign_nm  = call_user_func_array('array_merge', $this->m_upload->name_campaign($id_campaign));
    $platform_dt  = call_user_func_array('array_merge', $this->m_upload->platform($id_platform));

    //FOR PROGRAMMATIC TYPE AND TARGETING
    if ($id_platform == 1) {
      if (empty($_POST['id_type'])) {
        $id_type    = '';
        $targeting  = '';

        $type_1 = $this->m_dashboard->type($id_brand, $id_campaign, $id_platform);
        $device = array();
        $count  = COUNT($type_1);

        # Check ada berapa device
        if ($count < 3 && $count > 1) {
          # looping data
          foreach ($type_1 as $key => $v) {
            if ($v['device'] != 'ALL') {
              array_push($device, $v['device']);
             }
          }
          $type     = RESET($device);
          $targeting  = $this->m_dashboard->get_targeting($id_brand, $id_campaign, $id_platform, $type);
          $id_type  = $type;
        }
        if ($count < 2) {
          $device   = $type_1[0]['device'];
          $targeting  = $this->m_dashboard->get_targeting($id_brand, $id_campaign, $id_platform, $device);
          $id_type  = $device;
        }
        if ($count >= 3) {
        #PILIH HANYA DATA ALL
          foreach ($type_1 as $key => $v) {
            if ($v['device'] == 'ALL') {
              array_push($device, $v['device']);
            }
          }
          $type     = RESET($device);
          $targeting  = $this->m_dashboard->get_targeting($id_brand, $id_campaign, $id_platform, $type);
          $id_type  = $type;
        }

          $targeting2 = array();
          $targeting3 = array();
        $count    = COUNT($targeting);

        # Check ada berapa TARGETING
        if ($count < 3 && $count > 1) {
          # looping data
          foreach ($targeting as $key => $v) {
            if ($v['targeting'] != 'ALL') {
              array_push($targeting2, $v['targeting']);
             }
          }
          $targeting = reset($targeting2);
        }
        if ($count < 2) {
          $targeting2 = $targeting[0]['targeting'];
          $targeting  = $targeting2;
        }
        if ($count >= 3) {
          foreach ($targeting as $key => $v) {
						# Masukkan hanya ALL
						if ($v['targeting']=='ALL') {
							array_push($targeting2, $v['targeting']);
						}
					}
					$targeting = RESET($targeting2);
        }
      }
      elseif (!empty($_POST['id_type']) && empty($_POST['targeting'])) {
        $id_type  = $_POST['id_type'];
        $targeting  = '';
        $targeting2 = array();
        $targeting  = $this->m_dashboard->get_targeting($id_brand, $id_campaign, $id_platform, $id_type);

        $count    = COUNT($targeting);

        # Check ada berapa TARGETING
        if ($count < 3 && $count > 1) {
          # looping data
          foreach ($targeting as $key => $v) {
            if ($v['targeting'] != 'ALL') {
              array_push($targeting2, $v['targeting']);
             }
          }
          $targeting = reset($targeting2);
        }
        if ($count < 2) {
          $targeting2 = $targeting[0]['targeting'];
          $targeting  = $targeting2;
        }
        if ($count >= 3) {
          foreach ($targeting as $key => $v) {
            if ($v['targeting'] == 'ALL') {
              array_push($targeting2, $v['targeting']);
            }
          }
        // print_r($targeting2); die();#DEBIG
          $targeting = RESET($targeting2);
        }
      }
      else{
        $id_type      = $_POST['id_type'];
        $targeting      = $_POST['targeting'];
      }

      $dt_table   = $this->m_dashboard->data_dashboard2($id_brand, $id_campaign, $id_platform, $date_from, $date_to, $id_type, $targeting);
    }
    else{
      if (!empty($_POST['id_type'])) {
        $id_type      = $_POST['id_type'];
        $targeting      = $_POST['targeting'];
      }

      if (!empty($id_type) AND !empty($targeting)) {
        $dt_table   = $this->m_dashboard->data_dashboard2($id_brand, $id_campaign, $id_platform, $date_from, $date_to, $id_type, $targeting);
        }
        else{
        $dt_table     = $this->m_dashboard->data_dashboard($id_brand, $id_campaign, $id_platform, $date_from, $date_to);
        }
    }
    
    $value  = array();

    if ($platform_dt['platform'] == 'Programmatic') {
      $table_th = array('Date','Device','Targeting','Cost','Impression','Clicks','CTR', 'Engagement', 'Eng_Rate','Conversions', 'View Video', 'View Rates', 'CPV','CVR','CPC','CPM','CPA', 'CPE', 'Session');

      foreach ($dt_table as $key => $table_val) {
        array_push($value, array(
          'date'        => $table_val['date'],
          'device'      => $table_val['device'],
          'targeting'   => $table_val['targeting'],
          'cost'        => $kurs.number_format($table_val['cost'], 2, ".", ","),
          'impression'  => number_format($table_val['impression'], 0, '.', ','),
          'clicks'      => $table_val['clicks'],
          'ctr'         => number_format($table_val['ctr']*100, 2, ".", ",")." %",
          'engagement'  => number_format($table_val['engagement'], 0, '.', ','),
          'eng_rate'    => number_format($table_val['eng_rate']*100, 2, ".", ",")." %",
          'conversions' => $table_val['conversions'],
          'views_video'	=> $table_val['views_video'],
          'view_rates'	=> number_format($table_val['view_rates']*100, 2, ".", ",")." %",
          'cpv'					=> $kurs.number_format($table_val['cpv'], 2, ",", "."),
          'cvr'         => number_format($table_val['cvr'], 2, ",", ".")." %",
          'cpc'         => $kurs.number_format($table_val['cpc'], 2, ",", "."),
          'cpm'         => $kurs.number_format($table_val['cpm'], 2, ".", ","),
          'cpa'         => $kurs.number_format($table_val['cpa'], 2, ",", "."),
          'cpe'         => $kurs.number_format($table_val['cpe'], 2, ",", "."),
          'session'     => number_format($table_val['session'], 0, ",", "."),
        ));
      }
    }

    elseif ($platform_dt['platform'] == 'Facebook') {
      $table_th = array('Date','Cost','Impression','Clicks','CTR','Engagement','Eng Rate','Page Likes','Likes Rate','Views (Video)', 'Views Rate', 'Conversions','Leads', 'Reach', 'CVR','CPC','CPM','CPE','CPL','CPV', 'CPA','CPLeads', 'CPReach', 'adRecall', 'Session');

      foreach ($dt_table as $key => $table_val) {
        array_push($value, array(
          'date'         => $table_val['date'],
          'cost'         => $kurs.number_format($table_val['cost'], 2, ".", ","),
          'impression'   => number_format($table_val['impression'], 0, '.', ','),
          'clicks'       => $table_val['clicks'],
          'ctr'          => number_format($table_val['ctr']*100, 2, ".", ",")." %",
          'engagement'   => number_format($table_val['engagement'], 0, '.', ','),
          'eng_rate'     => number_format($table_val['eng_rate']*100, 2, ".", ",")." %",
          'page_likes'   => number_format($table_val['page_likes'], 0, '.', ','),
          'like_rates'   => number_format($table_val['like_rates']*100, 2, ".", ",")." %",
          'views_video'  => number_format($table_val['views_video'], 0, '.', ','),
          'view_rates'   => number_format($table_val['view_rates']*100, 2, ".", ",")." %",
          'conversions'  => number_format($table_val['conversions'], 0, '.', ','),
          'leads' 			 => number_format($table_val['leads'], 0, '.', ','),
          'reach' 			 => number_format($table_val['reach'], 0, '.', ','),
          'cvr'   		 	 => number_format($table_val['cvr']*100, 2, ".", ",")." %",
          'cpc'          => $kurs.number_format($table_val['cpc'], 2, ",", "."),
          'cpm'          => $kurs.number_format($table_val['cpm'], 2, ".", ","),
          'cpe'          => $kurs.number_format($table_val['cpe'], 2, ".", ","),
          'cpl'          => $kurs.number_format($table_val['cpl'], 2, ",", "."),
          'cpv'          => $kurs.number_format($table_val['cpv'], 2, ",", "."),
          'cpa'          => $kurs.number_format($table_val['cpa'], 2, ",", "."),
          'cpleads'      => $kurs.number_format($table_val['cpleads'], 2, ",", "."),
          'cpreach'      => $kurs.number_format($table_val['cpreach'], 2, ",", "."),
          'adRecall'     => number_format($table_val['adRecall'], 0, ",", "."),
          'session'      => number_format($table_val['session'], 0, ",", "."),
        ));
      }
    }

    elseif ($platform_dt['platform'] == 'Instagram') {
      $table_th = array('Date','Cost','Impression','Clicks','CTR','Engagement', 'Leads', 'Reach','Eng Rate','Views (Video)','View Rate','CPC','CPM','CPE','CPV', 'CPLeads', 'CPReach', 'adRecall', 'Session');

      foreach ($dt_table as $key => $table_val) {
        array_push($value, array(
          'date'        => $table_val['date'],
          'cost'        => $kurs.number_format($table_val['cost'], 2, ".", ","),
          'impression'  => number_format($table_val['impression'], 0, '.', ','),
          'clicks'      => $table_val['clicks'],
          'ctr'         => number_format($table_val['ctr']*100, 2, ".", ",")." %",
          'engagement'  => number_format($table_val['engagement'], 0, '.', ','),
          'leads'  			=> number_format($table_val['leads'], 0, '.', ','),
          'reach'  			=> number_format($table_val['reach'], 0, '.', ','),
          'eng_rate'    => number_format($table_val['eng_rate']*100, 2, ".", ",")." %",
          'views_video' => number_format($table_val['views_video'], 0, '.', ','),
          'view_rates'  => number_format($table_val['view_rates']*100, 2, ".", ",")." %",
          'cpc'         => $kurs.number_format($table_val['cpc'], 2, ",", "."),
          'cpm'         => $kurs.number_format($table_val['cpm'], 2, ".", ","),
          'cpe'         => $kurs.number_format($table_val['cpe'], 2, ".", ","),
          'cpv'         => $kurs.number_format($table_val['cpv'], 2, ",", "."),
          'cpleads'     => $kurs.number_format($table_val['cpleads'], 2, ",", "."),
          'cpreach'     => $kurs.number_format($table_val['cpreach'], 2, ",", "."),
          'adRecall'    => number_format($table_val['adRecall'], 0, ",", "."),
          'session'     => number_format($table_val['session'], 0, ",", "."),
        ));
      }
    }

    elseif ($platform_dt['platform'] == 'Twitter') {
      $table_th = array('Date','Cost','Impression','Clicks','CTR','Engagement', 'Eng Rate', 'Followers', 'Follow Rate','Views (Video)', 'View Rate','CPC','CPM','CPF', 'Session');

      foreach ($dt_table as $key => $table_val) {
        array_push($value, array(
          'date'        => $table_val['date'],
          'cost'        => $kurs.number_format($table_val['cost'], 2, ".", ","),
          'impression'  => number_format($table_val['impression'], 0, '.', ','),
          'clicks'      => $table_val['clicks'],
          'ctr'         => number_format($table_val['ctr']*100, 2, ".", ",")." %",
          'engagement'  => number_format($table_val['engagement'], 0, '.', ','),
          'eng_rate'    => number_format($table_val['eng_rate']*100, 2, ".", ",")." %",
          'followers'   => number_format($table_val['followers'], 0, '.', ','),
          'follow_rates'=> number_format($table_val['follow_rates']*100, 2, '.', ',')." %",
          'views_video' => number_format($table_val['views_video'], 0, '.', ','),
          'view_rates'  => number_format($table_val['view_rates']*100, 2, ".", ",")." %",
          'cpc'         => $kurs.number_format($table_val['cpc'], 2, ",", "."),
          'cpm'         => $kurs.number_format($table_val['cpm'], 2, ".", ","),
          'cpf'         => $kurs.number_format($table_val['cpf'], 2, ",", "."),
          'session'     => number_format($table_val['session'], 0, ",", "."),
        ));
      }
    }

    elseif ($platform_dt['platform'] == 'Native') {
      $table_th = array('Date','Cost','Impression','Clicks','CTR','CPC','CPM','Session');

      foreach ($dt_table as $key => $table_val) {
        array_push($value, array(
          'date'        => $table_val['date'],
          'cost'        => $kurs.number_format($table_val['cost'], 2, ".", ","),
          'impression'  => number_format($table_val['impression'], 0, '.', ','),
          'clicks'      => $table_val['clicks'],
          'ctr'         => number_format($table_val['ctr']*100, 2, ".", ",")." %",
          'cpc'         => $kurs.number_format($table_val['cpc'], 2, ",", "."),
          'cpm'         => $kurs.number_format($table_val['cpm'], 2, ".", ","),
          'session'     => number_format($table_val['session'], 0, ".", ",")
        ));
      }
    }

    elseif ($platform_dt['platform'] == 'YouTube' or $platform_dt['platform'] == 'Video Network') {
      $table_th = array('Date','Device','Targeting','Cost','Impression','Clicks','Views (Video)', 'View Rate','CTR','CPC', 'CPM', 'CPV', 'Session');

      foreach ($dt_table as $key => $table_val) {
        array_push($value, array(
          'date'        => $table_val['date'],
          'device'      => $table_val['device'],
          'targeting'   => $table_val['targeting'],
          'cost'        => $kurs.number_format($table_val['cost'], 2, ".", ","),
          'impression'  => number_format($table_val['impression'], 0, '.', ','),
          'clicks'      => $table_val['clicks'],
          'views_video' => number_format($table_val['views_video'], 0, '.', ','),
          'view_rates'  => number_format($table_val['view_rates']*100, 2, ".", ",")." %",
          'ctr'         => number_format($table_val['ctr']*100, 2, ".", ",")." %",
          'cpc'         => $kurs.number_format($table_val['cpc'], 2, ",", "."),
          'cpm'         => $kurs.number_format($table_val['cpm'], 2, ".", ","),
          'cpv'         => $kurs.number_format($table_val['cpv'], 2, ",", "."),
          'session'     => number_format($table_val['session'], 0, ",", "."),
        ));
      }
    }

    elseif ($platform_dt['platform'] == 'CPI') {
      $table_th = array('Date','Device','Targeting','Cost','Impression','Clicks', 'Install', 'Install Rate', 'CTR','CPI', 'Session');

      foreach ($dt_table as $key => $table_val) {
        array_push($value, array(
          'date'          => $table_val['date'],
          'device'        => $table_val['device'],
          'targeting'     => $table_val['targeting'],
          'cost'          => $kurs.number_format($table_val['cost'], 2, ".", ","),
          'impression'    => number_format($table_val['impression'], 0, '.', ','),
          'clicks'        => $table_val['clicks'],
          'install'       => number_format($table_val['install'], 0, '.', ','),
          'install_rate'  => number_format($table_val['install_rate']*100, 2, ".", ",")." %",
          'ctr'           => number_format($table_val['ctr']*100, 2, ".", ",")." %",
          'cpi'           => $kurs.number_format($table_val['cpi'], 2, ",", "."), 
          'session'       => number_format($table_val['session'], 0, ",", ".")
        ));
      }
    }

    elseif ($platform_dt['platform'] == 'Publisher') {
      $table_th = array('Date','Revenue','Ad Request','Impression', 'Fill Rate', 'Clicks', 'CTR', 'CPM','CPC', 'Session');

      foreach ($dt_table as $key => $table_val) {
        array_push($value, array(
          'date'        	=> $table_val['date'],
          'cost'        	=> $kurs.number_format($table_val['cost'], 2, ".", ","),
          'ad_request'  	=> number_format($table_val['ad_request'], 0, '.', ','),
          'impression'  	=> number_format($table_val['impression'], 0, '.', ','),
          'fill_rate'  		=> number_format($table_val['fill_rate']*100, 2, ".", ",")." %",
          'clicks'      	=> $table_val['clicks'],
          'ctr'         	=> number_format($table_val['ctr']*100, 2, ".", ",")." %",
          'cpm'         	=> $kurs.number_format($table_val['cpm'], 2, ",", "."),
          'cpc' 	  			=> number_format($table_val['cpc'], 0, '.', ','),
          'session' 	  	=> number_format($table_val['session'], 0, '.', ','),
        ));
      }
    }

    elseif ($platform_dt['platform'] == 'SEM') {
			$table_th = array('Date','Device','Targeting','Cost', 'Impression', 'Click','Conversions', 'CPC', 'CPM', 'CPV', 'CPA', 'CVR', 'Session');
			foreach ($dt_table as $key => $table_val) {
        array_push($value, array(
          'date'        	=> $table_val['date'],
          'device'      	=> $table_val['device'],
          'targeting'   	=> $table_val['targeting'],
          'cost'        	=> $kurs.number_format($table_val['cost'], 2, ".", ","),
          'impression'  	=> number_format($table_val['impression'], 0, '.', ','),
          'clicks'      	=> $table_val['clicks'],
          'conversions' 	=> number_format($table_val['conversions'], 0, '.', ','),
          'cpc'         	=> $kurs.number_format($table_val['cpc'], 2, ",", "."),
          'cpm'         	=> $kurs.number_format($table_val['cpm'], 2, ".", ","),
          'cpv'          	=> $kurs.number_format($table_val['cpv'], 2, ",", "."),
          'cpa'          	=> $kurs.number_format($table_val['cpa'], 2, ",", "."),
          'cvr'   		 	 	=> number_format($table_val['cvr']*100, 2, ".", ",")." %",
          'session'   		=> number_format($table_val['session'], 0, ".", ","),
        ));
      }
		}

    elseif ($platform_dt['platform'] == 'Tiktok') {
			// GET view 2s / 6s
			$viewSecond;
			$rateSecond;

			foreach($dt_table as $key => $vSecond){
				if ($vSecond['view_6s'] != 0) {
					$viewSecond = 'View 6s';
					$rateSecond = 'View Rates 6s';
					break;
				}
				else if($vSecond['view_2s'] != 0) {
					$viewSecond = 'View 2s';
					$rateSecond = 'View Rates 2s';
					break;
				}
			}

			$table_th = array(
				'Date', 'Cost', 'Impression', 'Clicks', 'CTR', 'Engagement', 'Conversion', 'Reach', 'Followers', $viewSecond, $rateSecond, 'Video Likes', 'Comment', 'Share', 'Profile Visits', 'Session'
			);

			foreach ($dt_table as $key => $table_val) {
				if ($table_val['view_6s'] != 0) {
					$tdview     = number_format($table_val['view_6s'], 0, '.', ','); 
					$tdviewRate = number_format($table_val['viewRates_2_6_s']*100, 2, '.', ',')." %"; 
				}
				else if($table_val['view_2s'] != 0) {
					$tdview     = number_format($table_val['view_2s'], 0, '.', ','); 
					$tdviewRate = number_format($table_val['viewRates_2_6_s']*100, 2, '.', ',')." %"; 
				}
				array_push($value, array(
					'date'          => $table_val['date'],
					'device'        => $table_val['device'],
					'targeting'     => $table_val['targeting'],
					'cost'          => $kurs.number_format($table_val['cost'], 0, ".", ","),
					'impression'    => number_format($table_val['impression'], 0, '.', ','),
					'clicks'        => $table_val['clicks'],
					'ctr'           => number_format($table_val['ctr']*100, 2, ".", ",")." %",
					'engagement'    => number_format($table_val['engagement'], 0, '.', ','),
					'conversions'   => number_format($table_val['conversions'], 0, '.', ','),
					'reach'         => number_format($table_val['reach'], 0, '.', ','),
					'followers'     => number_format($table_val['followers'], 0, '.', ','),
					'viewSecond'    => $tdview,
					'viewSecondRate'=> $tdviewRate,
					'videoLikes'    => number_format($table_val['videoLikes'], 0, '.', ','),
					'comment'       => number_format($table_val['comment'], 0, '.', ','),
					'shares'        => number_format($table_val['shares'], 0, '.', ','),
					'profileVisits' => number_format($table_val['profileVisits'], 0, '.', ','),
					'session'       => number_format($table_val['session'], 0, '.', ','),
				));
			}
		}

    $data = array(
      'platform_name' => $platform_dt['platform']
      ,'th'           => $table_th
      ,'temp_table'   => $value
    );

    $data = $this->load->view('admin/table', $data, TRUE);
    echo json_encode($data);
	}

	//FUNCTION UNTUK MENDAPATKAN HARI PER HARI DARI DUA TANGGAL
	function createDateRangeArray($period_from,$period_to){
	    $aryRange=array();

	    $iDateFrom=mktime(1,0,0,substr($period_from,5,2), substr($period_from,8,2), substr($period_from,0,4));
	    $iDateTo=mktime(1,0,0,substr($period_to,5,2), substr($period_to,8,2), substr($period_to,0,4));

	    if ($iDateTo>=$iDateFrom)
	    {
	        array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
	        while ($iDateFrom<$iDateTo)
	        {
	            $iDateFrom+=86400; // add 24 hours
	            array_push($aryRange,date('Y-m-d',$iDateFrom));
	        }
	    }
	    return $aryRange;
	}

	function chart_date($tgl){
		$day   = substr($tgl, 8,2);
		$month = substr($tgl, 5,2);
		$year  = substr($tgl, 0,4);

		$dateObj   	= DateTime::createFromFormat('!m', $month);
		$month 		= $dateObj->format('F'); // March

		// print_r($period); die();//DEBUG

		$period = array(
						'tgl'	=> $day.' '.$month
						);

		// print_r($period); die();//DEBUG
		return $period;
	}

	function period($period_from, $period_to){
		$day_f   = substr($period_from, 8,2);
		$day_t   = substr($period_to, 8,2);
		$month_f = substr($period_from, 5,2);
		$month_t = substr($period_to, 5,2);
		$year_f  = substr($period_from, 0,4);
		$year_t  = substr($period_to, 0,4);

		$dateObj_f   = DateTime::createFromFormat('!m', $month_f);
		$dateObj_t   = DateTime::createFromFormat('!m', $month_t);
		$month_f = $dateObj_f->format('F'); // March
		$month_t = $dateObj_t->format('F'); // March

		// print_r($period); die();//DEBUG

		$period = array(
						'period_f'	=> $day_f.' '.$month_f.', '.$year_f
						,'period_t'	=> $day_t.' '.$month_t.', '.$year_t
						);

		return $period;
	}

	function get_campaign(){
        // echo '<pre/>'; print_r($_POST); die();
        $role       = $_SESSION['role'];
        $id_brand   = $_POST['id_brand'];
        $u_id       = $_POST['u_id'];

        $get_brand = $this->m_dashboard->get_campagin($id_brand, $u_id, $role);

        $campaign = array();

        foreach ($get_brand as $key => $get_brand) {
            array_push($campaign,  array(
                                           'id_campaign'   => $get_brand['id_campaign'],
                                           'name'          => $get_brand['name']
                                        )
                        );
        }

        echo json_encode($campaign);
  }

	function get_platform(){
      $id_brand    = $_POST['id_brand'];
      $id_campaign = $_POST['id_campaign'];

      $platform = $this->m_dashboard->get_platform($id_brand, $id_campaign);


      $id = array();

      foreach ($platform as $key => $platform) {
          array_push($id,  array(
                                     'id_brand' => $platform['id_brand'],
                                     'name'   => $platform['name']
                                  )
                      );
      }
      // print_r($id); die();//DEBUG

      echo json_encode($id);
  }

  function get_platform2(){
      $id_brand   = str_replace('"', '', $_POST['id_brand']);
      $name       = str_replace('"', '', $_POST['name']);

      $platform   = $this->m_dashboard->get_platform2($id_brand, $name);
      $platform2  = array();
      foreach ($platform as $row){
        array_push($platform2, array(
                        $row['id_platform']
                      )
                  );
      }

      $name = array();

      foreach ($platform2 as $key => $platform) {
          $get_plat = $this->m_dashboard->get_platform3($platform);

          foreach ($get_plat as $key => $get_plat) {
            array_push($name,  array(
                          'id_platform' => $get_plat['id_platform']
                                        ,'platform'   => $get_plat['platform']
                                    )
                        );
          }
      }
      // print_r($name); die();//DEBUG

      echo json_encode($name);
  }

  function get_targeting(){
      $id_brand     = $_POST['id_brand'];
      $id_campaign  = $_POST['id_campaign'];
      $id_platform  = $_POST['id_platform'];
      $type       = $_POST['id_type'];

      $targeting    = $this->m_dashboard->get_targeting($id_brand, $id_campaign, $id_platform, $type);
      $targeting2   = array();

      $count  = COUNT($targeting);

      # Check ada berapa device
      if ($count < 3 && $count > 1) {
        # looping data
        foreach ($targeting as $key => $v) {
        // print_r($v);die();//DEBUG
          if ($v['targeting'] != 'ALL') {
            array_push($targeting2, $v);
           }
        }
      }
      if ($count < 2) {
        $targeting2 = $targeting;
      }
      if ($count >= 3) {
        foreach ($targeting as $key => $v) {
          array_push($targeting2, $v);
        }
      }

      echo json_encode($targeting2);
      // #END PROSES PEMILIHAN TARGETING
  }

  function download(){
  	$id_brand     = $_GET['id_brand'];
  	$id_campaign  = $_GET['id_campaign'];
  	$id_platform  = $_GET['id_platform'];
  	$date         = $_GET['date'];//05/11/2016 - 05/11/2016
  	$date_from    = date('Y-m-d',strtotime(substr($date, 0,10)));
  	$date_to      = date('Y-m-d',strtotime(substr($date, 13, 22)));
  	$tanggal 	  = $this->period($date_from, $date_to);//Untuk dapet format tanggal "DD MON, YEAR"
  	$brand		  = call_user_func_array('array_merge', $this->m_dashboard->get_brand($id_brand));
  	$campaign_nm  = call_user_func_array('array_merge', $this->m_upload->name_campaign($id_campaign));
  	$platform_dt  = call_user_func_array('array_merge', $this->m_upload->platform($id_platform));
  	$dt_download  = $this->m_dashboard->data_dashboard($id_brand, $id_campaign, $id_platform, $date_from, $date_to);
  	// print_r($campaign_nm['name']);die;//DEBUG

  	//START DOWNLOAD METHOD
  	$objReader = PHPExcel_IOFactory::createReader('Excel5');
  	$template = 'C:/xampp5.9/htdocs/MTD/assets/media/template/twitter.xls';

  	if ($platform_dt['platform'] == 'Facebook') {
	  $template = $_SERVER['DOCUMENT_ROOT'].'/assets/media/template/facebook.xls';
  	}
  	elseif ($platform_dt['platform'] == 'Programmatic') {
  	  $template = $_SERVER['DOCUMENT_ROOT'].'/assets/media/template/programatic.xls';
  	}
  	elseif ($platform_dt['platform'] == 'Instagram') {
  	  $template = $_SERVER['DOCUMENT_ROOT'].'/assets/media/template/instagram.xls';
  	}
  	elseif ($platform_dt['platform'] == 'Twitter') {
  	  $template = $_SERVER['DOCUMENT_ROOT'].'/assets/media/template/twitter.xls';
  	}
    elseif ($platform_dt['platform'] == 'Native') {
      $template = $_SERVER['DOCUMENT_ROOT'].'/assets/media/template/native.xls';//FOR SERVER
      // $template = $_SERVER['DOCUMENT_ROOT'].'/mtd/assets/media/template/native.xls';//FOR LOCAL
    }
    elseif ($platform_dt['platform'] == 'YouTube' or $platform_dt['platform'] == 'Video Network') {
      $template = $_SERVER['DOCUMENT_ROOT'].'/assets/media/template/youtube.xls';//FOR SERVER
      // $template = $_SERVER['DOCUMENT_ROOT'].'/mtd/assets/media/template/youtube.xls';//FOR LOCAL
    }
    elseif ($platform_dt['platform'] == 'CPI') {
      if ($_SESSION['u_id'] == 60) {
        $template = $_SERVER['DOCUMENT_ROOT'].'/assets/media/template/cpi_yen.xls';//FOR SERVER
        // $template = $_SERVER['DOCUMENT_ROOT'].'/mtd/assets/media/template/cpi.xls';//FOR LOCAL
      }
      else{
        $template = $_SERVER['DOCUMENT_ROOT'].'/assets/media/template/cpi.xls';//FOR SERVER
        // $template = $_SERVER['DOCUMENT_ROOT'].'/mtd/assets/media/template/cpi.xls';//FOR LOCAL
      }
    }
    elseif ($platform_dt['platform'] == 'SEM') {
      $template = $_SERVER['DOCUMENT_ROOT'].'/assets/media/template/sem.xls';//FOR SERVER
      // $template = $_SERVER['DOCUMENT_ROOT'].'/mtd/assets/media/template/sem.xls';//FOR LOCAL
    }

  	$brand		= 'Brand : '.$brand['brand_name'];
  	$campagin	= 'Campagin Name : '.$campaign_nm['name'];
  	$platform	= 'Platform : '.$platform_dt['platform'];
  	$period		= 'Period : '.$tanggal['period_f'].' to '.$tanggal['period_t'];

  	$objPHPExcel = $objReader->load($template);

  	$objPHPExcel->getSheet(0)->mergeCells('A9:E9')->setCellValue('A9', $brand);
  	$objPHPExcel->getSheet(0)->mergeCells('A10:E10')->setCellValue('A10', $campagin);
  	$objPHPExcel->getSheet(0)->mergeCells('A11:E11')->setCellValue('A11', $platform);
  	$objPHPExcel->getSheet(0)->mergeCells('A12:E12')->setCellValue('A12', $period);

  	$baris = 15;

  	for ($i=0; $i < COUNT($dt_download); $i++) {
  		$jum = $baris+$i;
  		$BStyle = array(
  		  'borders' => array(
  		    'outline' => array(
  		      'style' => PHPExcel_Style_Border::BORDER_THIN
  		    )
  		  )
  		);
  		// print_r($dt_download[$i]);
  		if ($platform_dt['platform'] == 'Twitter') {
  			$objPHPExcel->getActiveSheet()->setCellValue('A'.$jum, $dt_download[$i]['date']);
  			$objPHPExcel->getActiveSheet()->getStyle('A'.$jum)->applyFromArray($BStyle);
  			$objPHPExcel->getActiveSheet()->setCellValue('B'.$jum, $dt_download[$i]['cost']);
  			$objPHPExcel->getActiveSheet()->getStyle('B'.$jum)->applyFromArray($BStyle);
  			$objPHPExcel->getActiveSheet()->setCellValue('C'.$jum, $dt_download[$i]['impression']);
  			$objPHPExcel->getActiveSheet()->getStyle('C'.$jum)->applyFromArray($BStyle);
  			$objPHPExcel->getActiveSheet()->setCellValue('D'.$jum, $dt_download[$i]['clicks']);
  			$objPHPExcel->getActiveSheet()->getStyle('D'.$jum)->applyFromArray($BStyle);
  			$objPHPExcel->getActiveSheet()->setCellValue('E'.$jum, number_format($dt_download[$i]['ctr']*100, 2, '.', ''));
  			$objPHPExcel->getActiveSheet()->getStyle('E'.$jum)->applyFromArray($BStyle);
  			$objPHPExcel->getActiveSheet()->setCellValue('F'.$jum, $dt_download[$i]['engagement']);
  			$objPHPExcel->getActiveSheet()->getStyle('F'.$jum)->applyFromArray($BStyle);
  			$objPHPExcel->getActiveSheet()->setCellValue('G'.$jum, number_format($dt_download[$i]['eng_rate']*100, 2, '.', ''));
  			$objPHPExcel->getActiveSheet()->getStyle('G'.$jum)->applyFromArray($BStyle);
  			$objPHPExcel->getActiveSheet()->setCellValue('H'.$jum, $dt_download[$i]['followers']);
  			$objPHPExcel->getActiveSheet()->getStyle('H'.$jum)->applyFromArray($BStyle);
  			$objPHPExcel->getActiveSheet()->setCellValue('I'.$jum, number_format($dt_download[$i]['follow_rates']*100, 2, '.', ''));
  			$objPHPExcel->getActiveSheet()->getStyle('I'.$jum)->applyFromArray($BStyle);
  			$objPHPExcel->getActiveSheet()->setCellValue('J'.$jum, $dt_download[$i]['views_video']);
  			$objPHPExcel->getActiveSheet()->getStyle('J'.$jum)->applyFromArray($BStyle);
  			$objPHPExcel->getActiveSheet()->setCellValue('K'.$jum, number_format($dt_download[$i]['view_rates']*100, 2, '.', ''));
  			$objPHPExcel->getActiveSheet()->getStyle('K'.$jum)->applyFromArray($BStyle);
  			$objPHPExcel->getActiveSheet()->setCellValue('L'.$jum, number_format($dt_download[$i]['cpc'], 2, '.', ''));
  			$objPHPExcel->getActiveSheet()->getStyle('L'.$jum)->applyFromArray($BStyle);
  			$objPHPExcel->getActiveSheet()->setCellValue('M'.$jum, number_format($dt_download[$i]['cpm'], 2, '.', ''));
  			$objPHPExcel->getActiveSheet()->getStyle('M'.$jum)->applyFromArray($BStyle);
  			$objPHPExcel->getActiveSheet()->setCellValue('N'.$jum, number_format($dt_download[$i]['cpl'], 2, '.', ''));
  			$objPHPExcel->getActiveSheet()->getStyle('N'.$jum)->applyFromArray($BStyle);
  		}

  		elseif ($platform_dt['platform'] == 'Facebook') {
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$jum, $dt_download[$i]['date']);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$jum, $dt_download[$i]['cost']);
				$objPHPExcel->getActiveSheet()->getStyle('B'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$jum, $dt_download[$i]['impression']);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$jum, $dt_download[$i]['clicks']);
				$objPHPExcel->getActiveSheet()->getStyle('D'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$jum, number_format($dt_download[$i]['ctr']*100, 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('E'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$jum, $dt_download[$i]['engagement']);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$jum, number_format($dt_download[$i]['eng_rate']*100, 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('G'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$jum, $dt_download[$i]['page_likes']);
				$objPHPExcel->getActiveSheet()->getStyle('H'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$jum, number_format($dt_download[$i]['like_rates']*100, 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('I'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$jum, $dt_download[$i]['views_video']);
				$objPHPExcel->getActiveSheet()->getStyle('J'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$jum, number_format($dt_download[$i]['view_rates']*100, 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('K'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$jum, $dt_download[$i]['conversions']);
				$objPHPExcel->getActiveSheet()->getStyle('L'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$jum, $dt_download[$i]['leads']);
				$objPHPExcel->getActiveSheet()->getStyle('M'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('N'.$jum, $dt_download[$i]['reach']);
				$objPHPExcel->getActiveSheet()->getStyle('N'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('O'.$jum, number_format($dt_download[$i]['cvr'], 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('O'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('P'.$jum, number_format($dt_download[$i]['cpc'], 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('P'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('Q'.$jum, number_format($dt_download[$i]['cpm'], 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('Q'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('R'.$jum, number_format($dt_download[$i]['cpe'], 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('R'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('S'.$jum, number_format($dt_download[$i]['cpl'], 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('S'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('T'.$jum, number_format($dt_download[$i]['cpv'], 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('T'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('U'.$jum, number_format($dt_download[$i]['cpa'], 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('U'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('V'.$jum, number_format($dt_download[$i]['cpleads'], 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('V'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('W'.$jum, number_format($dt_download[$i]['cpreach'], 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('W'.$jum)->applyFromArray($BStyle);
  	  }

  		elseif ($platform_dt['platform'] == 'Programmatic') {
  	    $objPHPExcel->getActiveSheet()->setCellValue('A'.$jum, $dt_download[$i]['date']);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->getStyle('B'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$jum, $dt_download[$i]['device']);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$jum, $dt_download[$i]['targeting']);
        $objPHPExcel->getActiveSheet()->getStyle('C'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$jum, number_format($dt_download[$i]['cost'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('D'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$jum, $dt_download[$i]['impression']);
        $objPHPExcel->getActiveSheet()->getStyle('E'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$jum, $dt_download[$i]['clicks']);
        $objPHPExcel->getActiveSheet()->getStyle('F'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$jum, number_format($dt_download[$i]['ctr']*100, 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('G'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$jum, $dt_download[$i]['engagement']);
        $objPHPExcel->getActiveSheet()->getStyle('H'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('I'.$jum, number_format($dt_download[$i]['eng_rate']*100, 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('I'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('J'.$jum, $dt_download[$i]['conversions']);
        $objPHPExcel->getActiveSheet()->getStyle('J'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('K'.$jum, number_format($dt_download[$i]['cvr'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('K'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('L'.$jum, number_format($dt_download[$i]['cpc'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('L'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('M'.$jum, number_format($dt_download[$i]['cpm'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('M'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('N'.$jum, number_format($dt_download[$i]['cpa'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('N'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('O'.$jum, number_format($dt_download[$i]['cpe'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('O'.$jum)->applyFromArray($BStyle);
  	  }

  		elseif ($platform_dt['platform'] == 'Instagram') {
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$jum, $dt_download[$i]['date']);
				$objPHPExcel->getActiveSheet()->getStyle('A'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('B'.$jum, $dt_download[$i]['cost']);
				$objPHPExcel->getActiveSheet()->getStyle('B'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('C'.$jum, $dt_download[$i]['impression']);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('D'.$jum, $dt_download[$i]['clicks']);
				$objPHPExcel->getActiveSheet()->getStyle('D'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('E'.$jum, number_format($dt_download[$i]['ctr']*100, 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('E'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('F'.$jum, $dt_download[$i]['engagement']);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('G'.$jum, $dt_download[$i]['leads']);
				$objPHPExcel->getActiveSheet()->getStyle('G'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('H'.$jum, $dt_download[$i]['reach']);
				$objPHPExcel->getActiveSheet()->getStyle('H'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('I'.$jum, number_format($dt_download[$i]['eng_rate']*100, 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('I'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('J'.$jum, $dt_download[$i]['views_video']);
				$objPHPExcel->getActiveSheet()->getStyle('J'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('K'.$jum, number_format($dt_download[$i]['view_rates']*100, 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('K'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('L'.$jum, number_format($dt_download[$i]['cpc'], 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('L'.$jum)->applyFromArray($BStyle);
				$objPHPExcel->getActiveSheet()->setCellValue('M'.$jum, number_format($dt_download[$i]['cpm'], 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('M'.$jum)->applyFromArray($BStyle);
		    $objPHPExcel->getActiveSheet()->setCellValue('N'.$jum, number_format($dt_download[$i]['cpe'], 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('N'.$jum)->applyFromArray($BStyle);
		    $objPHPExcel->getActiveSheet()->setCellValue('O'.$jum, number_format($dt_download[$i]['cpv'], 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('O'.$jum)->applyFromArray($BStyle);
		    $objPHPExcel->getActiveSheet()->setCellValue('P'.$jum, number_format($dt_download[$i]['cpleads'], 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('P'.$jum)->applyFromArray($BStyle);
		    $objPHPExcel->getActiveSheet()->setCellValue('Q'.$jum, number_format($dt_download[$i]['cpreach'], 2, '.', ''));
				$objPHPExcel->getActiveSheet()->getStyle('Q'.$jum)->applyFromArray($BStyle);
  	  }

      elseif ($platform_dt['platform'] == 'Native') {
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$jum, $dt_download[$i]['date']);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$jum, $dt_download[$i]['cost']);
        $objPHPExcel->getActiveSheet()->getStyle('B'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$jum, $dt_download[$i]['impression']);
        $objPHPExcel->getActiveSheet()->getStyle('C'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$jum, $dt_download[$i]['clicks']);
        $objPHPExcel->getActiveSheet()->getStyle('D'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$jum, number_format($dt_download[$i]['ctr']*100, 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('E'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$jum, number_format($dt_download[$i]['cpc'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('F'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$jum, number_format($dt_download[$i]['cpm'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('G'.$jum)->applyFromArray($BStyle);
      }

      elseif ($platform_dt['platform'] == 'YouTube' OR $platform_dt['platform' == 'Video Network']) {
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$jum, $dt_download[$i]['date']);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$jum, $dt_download[$i]['device']);
        $objPHPExcel->getActiveSheet()->getStyle('B'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$jum, $dt_download[$i]['targeting']);
        $objPHPExcel->getActiveSheet()->getStyle('C'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$jum, number_format($dt_download[$i]['cost'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('D'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$jum, $dt_download[$i]['impression']);
        $objPHPExcel->getActiveSheet()->getStyle('E'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$jum, $dt_download[$i]['clicks']);
        $objPHPExcel->getActiveSheet()->getStyle('F'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$jum, $dt_download[$i]['views_video']);
        $objPHPExcel->getActiveSheet()->getStyle('G'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$jum, number_format($dt_download[$i]['view_rates']*100, 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('H'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('I'.$jum, number_format($dt_download[$i]['ctr']*100, 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('I'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('J'.$jum, number_format($dt_download[$i]['cpc'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('J'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('K'.$jum, number_format($dt_download[$i]['cpm'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('K'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('L'.$jum, number_format($dt_download[$i]['cpv'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('L'.$jum)->applyFromArray($BStyle);
      }

      elseif ($platform_dt['platform'] == 'CPI') {
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$jum, $dt_download[$i]['date']);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$jum, $dt_download[$i]['device']);
        $objPHPExcel->getActiveSheet()->getStyle('B'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$jum, $dt_download[$i]['targeting']);
        $objPHPExcel->getActiveSheet()->getStyle('C'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$jum, number_format($dt_download[$i]['cost'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('D'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$jum, $dt_download[$i]['impression']);
        $objPHPExcel->getActiveSheet()->getStyle('E'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$jum, $dt_download[$i]['clicks']);
        $objPHPExcel->getActiveSheet()->getStyle('F'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$jum, $dt_download[$i]['install']);
        $objPHPExcel->getActiveSheet()->getStyle('G'.$jum)->applyFromArray($BStyle);
        // $objPHPExcel->getActiveSheet()->setCellValue('H'.$jum, number_format($dt_download[$i]['install_rate']*100, 2, '.', ''));
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$jum, number_format($dt_download[$i]['install_rate'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('H'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('I'.$jum, number_format($dt_download[$i]['ctr']*100, 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('I'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('J'.$jum, number_format($dt_download[$i]['cpi'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('J'.$jum)->applyFromArray($BStyle);
      }

      elseif ($platform_dt['platform'] == 'SEM') {
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$jum, $dt_download[$i]['date']);
        $objPHPExcel->getActiveSheet()->getStyle('A'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$jum, $dt_download[$i]['device']);
        $objPHPExcel->getActiveSheet()->getStyle('B'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$jum, $dt_download[$i]['targeting']);
        $objPHPExcel->getActiveSheet()->getStyle('C'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$jum, number_format($dt_download[$i]['cost'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('D'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$jum, $dt_download[$i]['impression']);
        $objPHPExcel->getActiveSheet()->getStyle('E'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$jum, $dt_download[$i]['clicks']);
        $objPHPExcel->getActiveSheet()->getStyle('F'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$jum, $dt_download[$i]['conversions']);
        $objPHPExcel->getActiveSheet()->getStyle('G'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$jum,number_format($dt_download[$i]['cpc'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('H'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('I'.$jum, number_format($dt_download[$i]['cpm'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('I'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('J'.$jum, number_format($dt_download[$i]['cpv'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('J'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('K'.$jum, number_format($dt_download[$i]['cpa'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('K'.$jum)->applyFromArray($BStyle);
        $objPHPExcel->getActiveSheet()->setCellValue('L'.$jum, number_format($dt_download[$i]['cvr'], 2, '.', ''));
        $objPHPExcel->getActiveSheet()->getStyle('L'.$jum)->applyFromArray($BStyle);
      }

  	}
  	//activate worksheet number 1
  	$objPHPExcel->setActiveSheetIndex(0);
  	//name the worksheet
  	$objPHPExcel->getActiveSheet()->setTitle($campaign_nm['name']);
  	//set cell A1 content with some text


  	$filename='MTD_'.str_replace(' ', '_', $campaign_nm['name']).'_'.$platform_dt['platform'].'.xls'; //save our workbook as this file name
  	header('Content-Type: application/vnd.ms-excel'); //mime type
  	header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
  	header('Cache-Control: max-age=0'); //no cache

  	//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
  	//if you want to save it as .XLSX Excel 2007 format
  	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  	//force user to download the Excel file without writing it to server's HD
  	$objWriter->save('php://output');
  	// END DOWNLOAD
  }

  function logout() {
    $this->session->sess_destroy();
    redirect('login');
  }
}

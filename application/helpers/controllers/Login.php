<?php
class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model(array('M_user'));

        if ($this->session->userdata('u_name')) {

            if ($this->session->userdata('active') == 'Y') {

                if ($this->session->userdata('role') == 'superadmin')//for administrator
                {
                    redirect('admin/dashboard_admin');
                }
                else//for client
                {
                    redirect('dashboard');
                }
            }
        }
    }

    function index() {
        $this->load->view('login');
    }

    function proses() {
        $this->form_validation->set_rules('username', 'username', 'required|trim|xss_clean');
        $this->form_validation->set_rules('password', 'password', 'required|trim|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            $this->load->view('login');
        }
        else {

            $usr = $this->input->post('username', TRUE);
            $psw = $this->input->post('password', TRUE);
            $u = $usr;
            $p = md5($psw);

            // echo '<pre/>'; print_r($u.' '.$p); die();
            $cek = $this->M_user->cek($u, $p);

            if ($cek->num_rows() > 0) {

                //login berhasil, buat session
                foreach ($cek->result() as $qad) {                    $sess_data['u_id']           = $qad->u_id;
                    $sess_data['login']          = "true";
                    $sess_data['nama']           = $qad->nama;
                    $sess_data['u_name']         = $qad->u_name;
                    $sess_data['role']           = $qad->role;
                    $sess_data['active']         = $qad->active;
                    $sess_data['last_activity']  = time();
                    $sess_data['expire']         = $sess_data['last_activity'] + (30 * 60);

                    $this->session->set_userdata($sess_data);
                }

                if ($sess_data['active'] == 'Y') {
                    //Autentication Admin
                    if ($sess_data['role'] == 'superadmin') //for administrator
                    {
                        redirect('admin/dashboard_admin');
                    }
                    else //for client
                    {
                        redirect('dashboard');
                    }
                }
                else {
                    $this->session->set_flashdata('result_login', '<br>Your Username Has Not Active !');
                    redirect('login');
                }

            } else {
                $this->session->set_flashdata('result_login', '<br>Username or Password Wrong !');
                redirect('login');
            }
        }
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('login');
    }
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Campaign extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model(array('m_agency_sales'));
        $this->load->model(array('m_user_clients'));
        $this->load->model(array('m_campaign'));

    }

    public function index()
	{
    	// if ($_SESSION['login'] == true) {
        if (!empty($_SESSION['login'])) {
            $data = array(
													'select_agency'     => $this->select_agency(),
													'select_client'     => $this->select_client(),
													'select_platform'   => $this->select_platform(),
													'table_campaign'    => $this->table_campaign(),
													'table_brand'		=> $this->table_brand(),
													'view'              => 'campaign',
													'js'                => 'script_campaign'
                        );
            // echo "<pre/>"; print_r($data); die();//DEBUG
            $this->load->view('admin/template', $data);
        }
        else{
            $this->session->set_flashdata('result_login', '<br>You Have No Session, Please Login !');
            redirect('login');
        }
	}

	function insert_campaign() {
        $brand           = $this->input->post('brand');
        $campaign        = $this->input->post('cmpgn_name');
        $agency          = $this->input->post('agency');
        $platform        = $this->input->post('platform');
        $budget          = $this->input->post('budget');
        $client          = $this->input->post('client');

        $campaign = array(
                            'id_brand'    => $brand,
                            'name'        => $campaign,
                            'id_agency'   => $agency,
                            'id_platform' => $platform,
                            'budget'      => $budget,
                            'client'      => $client
                        );

        // echo "<pre/>"; print_r($campaign); die();//debug

        $cek = $this->m_campaign->insert_campaign($campaign);

        $message = array(
        					'valid' 	=> $cek['valid'],
        					'message'	=> $cek['message']
        				);
		$this->session->set_flashdata('message',$message);
		redirect('admin/campaign');
    }

	function edit_campaign() {
        if (empty($this->input->post('brand'))) {
            $id_brand   = $this->input->post('id_brand_old');
        }else{
            $id_brand   = $this->input->post('brand');
        }

        $id_campaign     = $this->input->post('id_campaign');
        $campaign        = $this->input->post('name');


        if (empty($this->input->post('agency'))) {
            $agency   = $this->input->post('id_agency_old');
        }else{
            $agency   = $this->input->post('agency');
        }

        if (empty($this->input->post('platform'))) {
            $platform   = $this->input->post('id_platform_old');
        }else{
            $platform   = $this->input->post('platform');
        }

        $budget         = $this->input->post('budget_edt');

        if (empty($this->input->post('client'))) {
            $client     = $this->input->post('u_id');
        }else{
            $client     = $this->input->post('client');
        }

        $data = array(
                        'id_campaign'   => $id_campaign,
                        'id_brand'      => $id_brand,
                        'name'          => $campaign,
                        'id_agency'     => $agency,
                        'id_platform'   => $platform,
                        'budget'        => $budget,
                        'u_id'          => $client
                    );
        // echo '<pre/>'; print_r($data); die();//debug
        $edit_agency =  $this->m_campaign->edit_campaign($data);

		$message = array(
						'valid' 	=> $edit_agency['valid'],
						'message'	=> $edit_agency['message']
					);

        // echo "<pre/>"; print_r($message); die();//debug

		$this->session->set_flashdata('message',$message);
		redirect('admin/campaign');
    }

    function table_campaign(){
    	$table_campaign = $this->m_campaign->table_campaign();
    	return $table_campaign;
    }

    function delete_campaign(){
    	$id_campaign = array('id_campaign' => $_POST['id_campaign']);

    	// echo '<pre/>'; print_r($id_campaign['id_campaign'][0]); die();//debug
    	$id = $id_campaign['id_campaign'][0];

		$dlt_agency = $this->m_campaign->delete_campaign($id);

		//for message
		$message = array(
        					'valid' 	=> $dlt_agency['valid'],
        					'message'	=> $dlt_agency['message']
        				);
        // echo "<pre/>"; print_r($message); die();//debug
		$this->session->set_flashdata('message',$message);

		redirect('admin/campaign');
    }

    function select_agency(){//FOR SELECT AGENCY
        $select_agency = $this->m_user_clients->table_agency();
        // echo '<pre/>'; print_r($select_agency);die();//debug
        return $select_agency;
    }

    function select_sales(){//FOR SELECT USER
        $select_sales = $this->m_user_clients->table_sales();
        // echo '<pre/>'; print_r($select_sales);die();//debug
        return $select_sales;
    }

    function select_platform(){//FOR SELECT USER
        $select_platform = $this->m_campaign->table_platform();
        // echo '<pre/>'; print_r($select_sales);die();//debug
        return $select_platform;
    }

    function select_client(){//FOR SELECT USER
        $select_client = $this->m_campaign->table_client();
        // echo '<pre/>'; print_r($select_sales);die();//debug
        return $select_client;
    }

    function insert_brand(){
        $brand   = $_POST['brand_name'];

        $cek = $this->m_campaign->insert_brand($brand);

        $message = array(
                            'valid'     => $cek['valid'],
                            'message'   => $cek['message']
                        );
        // echo "<pre/>"; print_r($message); die();//debug
        $this->session->set_flashdata('message',$message);
        redirect('admin/campaign');
    }

    function table_brand(){
        $table_brand = $this->m_campaign->table_brand();
        return $table_brand;
    }

    function edit_brand(){

        $brand = array(
                        'id_brand'      => $_POST['id_brand']
                        ,'brand_name'   => $_POST['brand_name']
                    );

        if (!empty($brand['brand_name'])) {
            $edit_brand =  $this->m_campaign->edit_brand($brand);

            $message = array(
                            'valid'     => $edit_brand['valid'],
                            'message'   => $edit_brand['message']
                        );

            // echo "<pre/>"; print_r($message); die();//debug

            $this->session->set_flashdata('message',$message);
            redirect('admin/campaign');
        }
        else{
            $message = array(
                            'valid'     => 'gagal_edit',
                            'message'   => 'Nama Brand Tidak Boleh Kosong'
                        );


            $this->session->set_flashdata('message',$message);
            redirect('admin/campaign');
        }
    }

    function delete_brand(){
        $id_brand = $_POST['id_brand'][0];

        $dlt_brand = $this->m_campaign->delete_brand($id_brand);

        //for message
        $message = array(
                            'valid'     => $dlt_brand['valid'],
                            'message'   => $dlt_brand['message']
                        );
        // echo "<pre/>"; print_r($message); die();//debug
        $this->session->set_flashdata('message',$message);

        redirect('admin/campaign');
    }

}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_clients extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model(array('m_user_clients'));

    }

    public function index(){
    	// if ($_SESSION['login'] == true) {
        if (!empty($_SESSION['login'])) {
            $data = array(
													'select_agency' => $this->select_agency(),
                                                    'select_sales'  => $this->select_sales(),
													'name_sales'    => $this->select_name_sales(),
													'selec_user'    => $this->select_user(),
													'view'          => 'user_clients',
													'js'            => 'script_tbl_user'
                        );
            // echo "<pre/>"; print_r($data); die();//DEBUG
            $this->load->view('admin/template', $data);
        }
        else{
            $this->session->set_flashdata('result_login', '<br>You Have No Session, Please Login !');
            redirect('login');
        }
    }

    function select_agency(){//FOR SELECT AGENCY
        $select_agency = $this->m_user_clients->table_agency();
        // echo '<pre/>'; print_r($select_agency);die();//debug
        return $select_agency;
    }

    function select_sales(){//FOR SELECT USER
        $select_sales = $this->m_user_clients->table_sales();
        // echo '<pre/>'; print_r($select_agency);die();//debug
        return $select_sales;
    }

    function select_name_sales(){//FOR SELECT USER
        $select_sales = $this->m_user_clients->name_sales();
        // echo '<pre/>'; print_r($select_sales);die();//debug
        return $select_sales;
    }

    function select_user(){
        $select_user = $this->m_user_clients->table_user();
    	// echo '<pre/>'; print_r($select_user);die();//debug
        return $select_user;
    }

    function insert_user(){
        // echo '<pre/>'; print_r($_POST); die();//DEBUG
        $role      = $this->input->post('role');
        $agency    = $this->input->post('agency');
        $sales     = $this->input->post('sales');

        if (!empty($_POST['name_non'])) {
            $name   = $this->input->post('name_non');
        }
        elseif (!empty($_POST['name_pub'])) {
             $name  = $this->input->post('name_pub');
        }
        else{
            $name   = $this->input->post('name_user');
        }

        $usr_name  = $this->input->post('usr_name');
        $pass      = $this->input->post('pass');
        $active    = $this->input->post('active');

        $user = array(
                        'u_id'         => '',
                        'id_agency'    => $agency,
                        'id_sales'     => $sales,
                        'nama'         => $name,
                        'u_name'       => $usr_name,
                        'u_paswd'      => $pass,
                        'active'       => $active,
                        'role'         => $role
                    );

        $cek = $this->m_user_clients->insert_user($user);

        $message = array(
                            'valid'     => $cek['valid'],
                            'message'   => $cek['message']
                        );
        // echo "<pre/>"; print_r($message); die();//debug
        $this->session->set_flashdata('message',$message);
        redirect('admin/user_clients');

    }

    function delete_user(){
        $id_user = array('id_user' => $_POST['id_user']);

        // echo '<pre/>'; print_r($id_user['id_user'][0]); die();//debug
        $id = $id_user['id_user'][0];

        $dlt_agency = $this->m_user_clients->delete_user($id);

        //for message
        $message = array(
                            'valid'     => $dlt_agency['valid'],
                            'message'   => $dlt_agency['message']
                        );
        // echo "<pre/>"; print_r($message); die();//debug
        $this->session->set_flashdata('message',$message);

        redirect('admin/User_clients');
    }

    function edit_user() {
        $id_user   = $this->input->post('id_user');
        $role      = $this->input->post('role');

        if (empty($this->input->post('agency'))) {
            $agency = $this->input->post('agency_old');
        }else{
            $agency = $this->input->post('agency');
        }

        if (empty($this->input->post('sales'))) {
            $sales = $this->input->post('sales_old');
        }else{
            $sales = $this->input->post('sales');
        }

        $name      = $this->input->post('name');
        $usr_name  = $this->input->post('user_name');

        $get_user = call_user_func_array('array_merge', $this->m_user_clients->get_user($id_user, $usr_name));//GO TO Model

        if (empty($this->input->post('password'))) {
            $pass = $get_user['u_paswd'];
        }else{
            $new = $this->input->post('password');
            $pass = md5($new); 
        }

        $active    = $this->input->post('active');

        $user = array(
                        'u_id'         => $id_user,
                        'id_agency'    => $agency,
                        'id_sales'     => $sales,
                        'nama'         => $name,
                        'u_name'       => $usr_name,
                        'u_paswd'      => $pass,
                        'active'       => $active,
                        'role'         => $role
                    );
        // echo '<pre/>'; print_r($user); die();//DEBUG

        $edit_agency =  $this->m_user_clients->edit_user($user);

        $message = array(
                        'valid'     => $edit_agency['valid'],
                        'message'   => $edit_agency['message']
                    );

        // echo "<pre/>"; print_r($message); die();//debug

        $this->session->set_flashdata('message',$message);
        redirect('admin/user_clients');
    }

}

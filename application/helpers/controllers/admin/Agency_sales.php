<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Agency_sales extends CI_Controller {

	function __construct() {
        parent::__construct();
        $this->load->model(array('m_agency_sales'));
        $this->load->model(array('m_user_clients'));

    }

    public function index(){
    	// if ($_SESSION['login'] == true) {
        if (!empty($_SESSION['login'])) {
            $data = array(
													'table_agency' => $this->table_agency(),
													'table_sales'  => $this->table_sales(),
                                                    'select_sales' => $this->select_sales(),
													'select_sgh'   => $this->select_sgh(),
													'view'		   => 'agency_sales',
													'js'		   => 'script_tbl_admin'
                        );
            // echo "<pre/>"; print_r($data); die();//DEBUG
            $this->load->view('admin/template', $data);
        }
        else{
            $this->session->set_flashdata('result_login', '<br>You Have No Session, Please Login !');
            redirect('login');
        }
	}

	function input_agency() {
        $pt 		 = $this->input->post('pt');
        $agency_name = $this->input->post('agency_name');
        $email 		 = $this->input->post('email');
        $sales 		 = $this->input->post('sales');

        $agency = array(
        					'pt' 		  => $pt,
        					'agency_name' => $agency_name,
        					'email' 	  => $email,
        					'id_sales' 	  => $sales
        				);

        // echo "<pre/>"; print_r($agency); die();//debug

        $cek = $this->m_agency_sales->insert_agency($agency);

        $message = array(
        					'valid' 	=> $cek['valid'],
        					'message'	=> $cek['message']
        				);
		$this->session->set_flashdata('message',$message);
		redirect('admin/agency_sales');
    }

	function edit_agency() {
        $agency_id 	= $this->input->post('agency_id');
        $pt 		= $this->input->post('pt');
        $name 		= $this->input->post('name');
        $email 		= $this->input->post('email_edit');

        if (empty($this->input->post('sales_edit'))) {
            $id_sales   = $this->input->post('sales_old');
        }else{
            $id_sales   = $this->input->post('sales_edit');
        }

        // echo '<pre/>'; print_r($id_sales); die();//debug

        $data = array(
                        'id_agency'     => $agency_id,
                        'pt'            => $pt,
                        'agency_name'   => $name,
                        'email'         => $email,
                        'id_sales'      => $id_sales
                    );
        $edit_agency =  $this->m_agency_sales->edit_agency($data);

		$message = array(
						'valid' 	=> $edit_agency['valid'],
						'message'	=> $edit_agency['message']
					);

        // echo "<pre/>"; print_r($message); die();//debug

		$this->session->set_flashdata('message',$message);
		redirect('admin/agency_sales');
    }

    function table_agency(){
    	$table_agency = $this->m_agency_sales->table_agency();
    	return $table_agency;
    }

    function delete_agency(){
    	$id_agency = array('id_agency' => $_POST['id_agency']);

    	// echo '<pre/>'; print_r($id_agency['id_agency'][0]); die();//debug
    	$id = $id_agency['id_agency'][0];

		$dlt_agency = $this->m_agency_sales->delete_agency($id);

		//for message
		$message = array(
        					'valid' 	=> $dlt_agency['valid'],
        					'message'	=> $dlt_agency['message']
        				);
        // echo "<pre/>"; print_r($message); die();//debug
		$this->session->set_flashdata('message',$message);

		redirect('admin/agency_sales');
    }

    function select_sales(){//FOR SELECT USER
        $select_sales = $this->m_user_clients->table_sales();
        // echo '<pre/>'; print_r($select_sales);die();//debug
        return $select_sales;
    }

    // START SALES ======================================================================================================================================>

    function get_position(){
        $id_sales = $_POST['id_sales'];

        $select_sales = $this->m_user_clients->get_position($id_sales);
        echo json_encode($select_sales);
    }

    function select_sgh(){//FOR SELECT USER
        $select_sales = $this->m_user_clients->select_sgh();
        // echo '<pre/>'; print_r($select_sales);die();//debug
        return $select_sales;
    }

     function table_sales(){
    	$table_sales = $this->m_agency_sales->table_sales();
        $sales_data = array();

        foreach ($table_sales as $key => $sales) {
            if ($sales['position'] == 'SGH') {
                $head = 'Febri Ratna';
            }
            elseif ($sales['position'] == 'Manager') {
                $head = 'Tomoaki Ogawa';
            }
            else{
                $head =  $sales['head'];
            }
            array_push($sales_data, array(
                                            $sales['id_sales'],
                                            $sales['id_head'], 
                                            $head, 
                                            $sales['sales_name'], 
                                            $sales['position'], 
                                        )
                    );
        }
        // echo '<pre/>'; print_r($data['sales']);die();

    	return $sales_data;
    }

    function insert_sales(){

    	$data_sales = array(
    						'sales_name'	=> $this->input->post('sales_name'),
    						'email'			=> $this->input->post('email_add'),
    						'position'		=> $this->input->post('position'),
                            'id_head'       => $this->input->post('head')
    						);

        // echo "<pre/>"; print_r($data_sales); die();//debug
    	$sales = $this->m_agency_sales->insert_sales($data_sales);

    	$message = array(
        					'valid' 	=> $sales['valid'],
        					'message'	=> $sales['message']
        				);
		$this->session->set_flashdata('message',$message);
		redirect('admin/agency_sales');
    }

    function edit_sales() {

        if (empty($_POST['head'])) {
            $head = $_POST['edt_id_head'];
        }
        else{
            $head = $_POST['head'];
        }

        if (empty($_POST['position'])) {
            $position = $_POST['edt_sales_position'];
        }
        else{
            $position = $_POST['position'];
        }

        $data = array(
                        'id_sales'      => $this->input->post('edt_id_sales'),
                        'sales_name'    => $this->input->post('edt_sales_name'),
                        'email'         => $this->input->post('edt_email'),
                        'position'      => $position,
                        'id_head'       => $head
                    );
        // echo '<pre/>'; print_r($_POST); echo'<br/>'; print_r($data); die();//debug

        if (!empty($data)) {
        	$edit_sales =  $this->m_agency_sales->edit_sales($data);
			$message = array(
							'valid' 	=> $edit_sales['valid'],
							'message'	=> $edit_sales['message']
						);
        }
        else{
			$message = array(
							'valid' 	=> 'sales_kosong',
							'message'	=> 'Inputan Tidak Boleh Kosong !'
						);
        }

		$this->session->set_flashdata('message',$message);
		redirect('admin/agency_sales');
    }

    function delete_sales(){
    	$id_sales = array('id_sales' => $_POST['id_sales']);

    	// echo '<pre/>'; print_r($id_sales['id_sales'][0]); die();//debug

    	$id = $id_sales['id_sales'][0];

		$dlt_agency = $this->m_agency_sales->delete_sales($id);

		//for message
		$message = array(
        					'valid' 	=> $dlt_agency['valid'],
        					'message'	=> $dlt_agency['message']
        				);
        // echo "<pre/>"; print_r($message); die();//debug
		$this->session->set_flashdata('message',$message);

		redirect('admin/agency_sales');
    }



}

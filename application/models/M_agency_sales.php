<?php

class M_agency_sales extends CI_Model {

    private $table = "user";
    
    function insert_agency($agency) {
        if (empty($agency)) {
            $valid   = "kosong";
            $message = "Form Not Allowed Empty !";
        }
        else{
            $agency = array(
                                    'id_agency'     => '', 
                                    'pt'            => $agency['pt'],
                                    'agency_name'   => $agency['agency_name'],
                                    'email'         => $agency['email'],
                                    'id_sales'      => $agency['id_sales']
                                );

        // echo '<pre/>'; print_r($agency); die();//DEBUG

            $cek = $this->db->get_where('agency', array('agency_name' => $agency['agency_name']));

            if ($cek->num_rows() == 0) { /*Jika data tidak ditemukan*/
                $insert = $this->db->insert('agency', $agency); 
                $valid = "sukses";
                $message = "Input Agency ".$agency." Success !";
            }
            else{
                
                $valid = "gagal";
                $message = "Data Has Availabe, Please Check Agency List !";
                
            }
        }


        return  array(
                        'valid'     => $valid , 
                        'message'   => $message
                    );
        // echo "<pre/>"; print_r($valid.' '.$message); die();//debug

    }

    function table_agency(){
         $table_agency = $this->db->query(" SELECT  `id_agency`, 
                                                    `pt`, 
                                                    `agency_name`, 
                                                    `email`, 
                                                    `id_sales`, 
                                                    (SELECT `sales_name` FROM `sales` WHERE `id_sales` = `agency`.`id_sales`) AS `sales_name` 
                                            FROM `agency` 
                                            ORDER BY `agency_name` ASC 
                                        ");
         return $table_agency->result_array();
    }

    function delete_agency($id){
        $this->db->where('id_agency', $id);
        $this->db->delete('agency');

        //for message 
        $valid = "dihapus";
        $message = "Data Has Deleted !";

        return array('valid' => $valid, 'message' => $message);

    }

    function edit_agency($data){

        $this->db->where('id_agency', $data['id_agency']);
        $query = $this->db->update('agency', $data);
        // echo '<pre/>'; print_r($query); die();//debug
        
        if ($query > 0) {
            $valid = "sukses_edit";
            $message = "Edit Successful !";
        }
        else{
            $valid = "gagal_edit";
            $message = "Edit Failed, ".$this->db->_error_message()."";   
        }

        return array('valid' => $valid, 'message' => $message);

    }

    // START SALES=========================================================================================>
  
    function table_sales(){
        // $table_sales = $this->db->get('sales');

        $table_sales = $this->db->query("  SELECT  a.`id_sales`, 
                                                    a.`id_head`,
                                                    (SELECT sales_name FROM sales WHERE id_sales = a.id_head) AS `head`, 
                                                    a.`sales_name`, 
                                                    a.`position`
                                            FROM `sales` a
                                            ORDER BY a.`position`
                                        ");

        return $table_sales->result_array();
    }

    function insert_sales($data_sales){
        
        if (empty($data_sales['sales_name']) || empty($data_sales['email']) || empty($data_sales['position'])) {
            $valid   = "sales_kosong";
            $message = "Form Not Allowed Empty !";
        }
        elseif ($data_sales['position']=='Account Executive' && empty($data_sales['id_head'])) {
            $valid   = "sales_kosong";
            $message = "SGH Field Not Allowed Empty !";
        }
        else{

            $cek = $this->db->get_where('sales', array('sales_name' => $data_sales['sales_name']));

            // echo '<pre/>'; print_r($data_sales); die();//debug
            if ($cek->num_rows() == 0) { /*Jika data tidak ditemukan*/
                if ($data_sales['id_head'] == NULL) {
                    $head = NULL;
                }
                else{
                    $head = $data_sales['id_head'];
                }

                $insert = $this->db->insert('sales', 
                                            array(
                                                    // 'id_sales'     => '', 
                                                    'id_head'      => $head,
                                                    'sales_name'   => $data_sales['sales_name'],
                                                    'email'        => $data_sales['email'],
                                                    'position'     => $data_sales['position']
                                                    )
                                            ); 
                $valid = "sales_sukses";
                $message = "Input Agency ".$agency." Success !";
                
            }
            else{
                
                $valid = "sales_gagal";
                $message = "Data Has Availabe, Please Check Agency List !";
                
            }
        }

        return  array(
                        'valid'     => $valid , 
                        'message'   => $message
                    );
    }

    function edit_sales($data){

        $this->db->where('id_sales', $data['id_sales']);
        $query = $this->db->update('sales', $data);

        // echo '<pre/>'; print_r($query); die();//debug
        
        if ($query > 0) {
            $valid = "sukses_edit_sales";
            $message = "Edit Successful !";
        }
        else{
            $valid = "gagal_edit_sales";
            $message = "Edit Failed, ".$this->db->_error_message()."";   
        }

        return array('valid' => $valid, 'message' => $message);

    }

    function delete_sales($id){
        $this->db->where('id_sales', $id);
        $this->db->delete('sales');

        //for message 
        $valid = "sales_dihapus";
        $message = "Data Has Deleted !";

        return array('valid' => $valid, 'message' => $message);

    }

}

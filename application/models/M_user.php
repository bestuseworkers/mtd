<?php

class M_user extends CI_Model {

    private $table = "user";

    function cek($username, $password) {
        $this->db->where("u_name", $username);
        $this->db->where("u_paswd", $password);
        return $this->db->get("user");
    }

    function semua() {
        return $this->db->get("user");
    }

    function cekKode($kode) {
        $this->db->where("u_name", $kode);
        return $this->db->get("user");
    }

    function cekId($kode) {
        $this->db->where("u_id", $kode);
        return $this->db->get("user");
    }
    
    function getLoginData($usr, $psw) {
        $u = mysql_real_escape_string($usr);
        $p = md5(mysql_real_escape_string($psw));
        $q_cek_login = $this->db->get_where('user', array('username' => $u, 'password' => $p));
        if (count($q_cek_login->result()) > 0) {
            foreach ($q_cek_login->result() as $qck) {
                foreach ($q_cek_login->result() as $qad) {

                    $sess_data['logged_in']     = 'Saya Login';
                    $sess_data['u_id']          = $qad->u_id;
                    $sess_data['id_agency']     = $qad->id_agency;
                    $sess_data['id_sales']      = $qad->id_sales;
                    $sess_data['nama']          = $qad->nama;
                    $sess_data['u_name']        = $qad->u_name;
                    $sess_data['active']        = $qad->active;
                    $sess_data['role']          = $qad->role;
                    
                    $this->session->set_userdata($sess_data);
                }
                //Autentication Admin
                if ($sess_data['role'] == 'superadmin') //for administrator
                {
                    redirect('admin/dashboard_admin');
                }
                else //for client
                {
                    redirect('dashboard');
                }

            }
        } else {
            $this->session->set_flashdata('result_login', '<br>Username atau Password yang anda masukkan salah.');
            header('location:' . base_url() . 'login');
        }
    }

    function update($id, $info) {
        $this->db->where("u_id", $id);
        $this->db->update("user", $info);
    }

    function simpan($info) {
        $this->db->insert("user", $info);
    }

    function hapus($kode) {
        $this->db->where("u_id", $kode);
        $this->db->delete("user");
    }

}

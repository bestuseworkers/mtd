<?php

class M_dashboard extends CI_Model {

    function name_campaign(){
        $name_campaign = $this->db->query(" SELECT *
            FROM `campaign`
            WHERE id_campaign IN (SELECT id_campaign FROM tbl_dashboard)
        ");
        return $name_campaign->result_array();
    }

    // Model for PUBLISHER
    function campaign_publisher($publisher){
        $brand = $this->db->query(" SELECT u_id
            FROM `user`
            WHERE nama = '$publisher'
        ");

        $u_id = $brand->result_array();
        $id = $u_id[0]['u_id'];

        $get_campagin = $this->db->query("  SELECT *
            FROM campaign
            WHERE u_id = '$id'
        ");

        // echo "<pre/>";print_r($get_campagin->result_array());die;//DEBUG
        return $get_campagin->result_array();
    }
    // End Model for PUBLISHER

    function name_brand(){
        // echo '<pre/>'; print_r($id_campaign); die();//DEBUG
        $name_brand = $this->db->query(" SELECT *
            FROM `brand`
            WHERE id_brand IN (SELECT id_brand FROM tbl_dashboard)
        ");
        return $name_brand->result_array();
    }

    function get_brand($id_brand){
        $name_brand = $this->db->query(" SELECT brand_name
            FROM `brand`
            WHERE id_brand = $id_brand
        ");
        return $name_brand->result_array();
    }

    function name_brand_client($id_user){
        // echo '<pre/>'; print_r($id_campaign); die();//DEBUG
        $name_brand = $this->db->query("SELECT * FROM `brand`
            WHERE id_brand IN (SELECT id_brand FROM tbl_dashboard)
            AND id_brand IN (SELECT id_brand FROM campaign WHERE u_id = $id_user)
        ");
        return $name_brand->result_array();
    }

    function campaign($id_campaign){
        $id_campaign = $this->db->query("SELECT id_campaign FROM `tmp_tbl_dashboard` WHERE id_campaign = $id_campaign");
        return $id_campaign->result_array();
    }

    function type($id_brand, $id_campaign, $id_platform){
        $type = $this->db->query("SELECT DISTINCT `device` FROM `tbl_dashboard` WHERE `device` <> 'NONE' AND `id_brand` = $id_brand AND `id_campaign` = $id_campaign AND `id_platform` = $id_platform ORDER BY `device` ASC");
        return $type->result_array();
    }

    function get_targeting($id_brand, $id_campaign, $id_platform, $type){
        // print_r($id_brand." ".$id_campaign." ".$id_platform." ".$type); die();//DEBUG
        $targeting = $this->db->query("SELECT DISTINCT `targeting` FROM `tbl_dashboard` WHERE `id_brand` = $id_brand AND `id_campaign` = $id_campaign AND `id_platform` = $id_platform AND `device` = '$type'");
        return $targeting->result_array();
    }

    function platform(){
        // echo '<pre/>'; print_r($id_campaign); die();//DEBUG
        $platform = $this->db->query(" SELECT *
            FROM `platform`
            WHERE id_platform IN (SELECT id_platform FROM tbl_dashboard)
        ");
        return $platform->result_array();
    }

    function data_dashboard($id_brand , $id_campaign, $id_platform, $date_from, $date_to){
        $data_dashboard = $this->db->query("SELECT *
            FROM `tbl_dashboard` WHERE `id_brand` = $id_brand AND `id_campaign` = $id_campaign AND `id_platform` = $id_platform AND date >= '$date_from' AND date <= '$date_to' ORDER BY `date` ASC
        ");

        return $data_dashboard->result_array();
    }

    //UNTUK TYPE DAN TARGETING PROGRAMMATIC
    function data_dashboard2($id_brand , $id_campaign, $id_platform, $date_from, $date_to, $id_type, $targeting){
        $data_dashboard2 = $this->db->query("SELECT *
            FROM `tbl_dashboard` WHERE `id_brand` = $id_brand AND `id_campaign` = $id_campaign AND `id_platform` = $id_platform AND date >= '$date_from' AND date <= '$date_to' AND `device` = '$id_type' AND `targeting` = '$targeting' ORDER BY `date` ASC
        ");

        return $data_dashboard2->result_array();
    }

    function get_campagin($id_brand, $u_id, $role){
      // echo '<pre/>'; print_r('id_brand: '.$id_brand.' u_id: '.$u_id.' role: '.$role); //DEBUG
        if($role == 'superadmin' OR $role == 'user') {
            $get_campagin = $this->db->query("SELECT DISTINCT name, id_campaign FROM `campaign` WHERE id_brand = $id_brand AND id_campaign IN (SELECT id_campaign FROM tbl_dashboard)");
        }
        else{
            $get_campagin = $this->db->query("SELECT id_campaign, name FROM `campaign` WHERE id_brand = $id_brand AND u_id = $u_id AND id_campaign IN (SELECT id_campaign FROM tbl_dashboard)");
        }

        return $get_campagin->result_array();
    }

    function get_platform($id_brand, $id_campaign){
        $get_platform = $this->db->query("SELECT id_brand, name FROM `campaign` WHERE id_brand = $id_brand AND id_campaign = $id_campaign");
        // echo '<pre/>'; print_r($data_dashboard->result_array()); die();//DEBUG

        return $get_platform->result_array();
    }

    function get_platform2($id_brand, $name){
        $get_platform2 = $this->db->query("SELECT id_platform FROM `campaign` WHERE id_brand = $id_brand AND name = '$name'");
        // echo '<pre/>'; print_r($data_dashboard->result_array()); die();//DEBUG

        return $get_platform2->result_array();
    }

    function get_platform3($platform){
        // echo '<pre/>'; print_r($platform[0]); die();//DEBUG
        $get_platform = $this->db->query("SELECT * FROM `platform` WHERE id_platform = $platform[0]");

        return $get_platform->result_array();
    }

    //START UNTUK USER / SALES
    function get_id_sales($name){
        //Untuk Dapetin id Sales
        $id         = $this->db->query("SELECT * FROM sales WHERE sales_name = '$name'");
        $sales      = call_user_func_array('array_merge', $id->result_array());

        $id1        = $sales['id_sales'];
        $get_client = $this->db->query("SELECT u_id FROM user WHERE id_sales = $id1");
        $u_id       = $get_client->result_array();

        //Checking Position
        if ($sales['position']=='SGH') {
            $under      = $this->db->query("SELECT * FROM sales WHERE id_head = $id1");
            // $sales_u    = call_user_func_array('array_merge', $under->result_array());//ARRAY MARGE
            $sales_u    = $under->result_array();

            foreach ($sales_u as $key => $id) {
                $id_u         = $id['id_sales'];
                $client_under = $this->db->query("SELECT u_id FROM user WHERE id_sales = $id_u");
                if(!empty($client_under->result_array())){
                    foreach ($client_under->result_array() as $key => $value) {
                        // $id_under     = call_user_func_array('array_merge', $client_under->result_array());
                        array_push($u_id, $value);
                    }
                }
            }
            // echo '<pre/>'; print_r($id_u);die;//DEBUG
        }
        elseif ($sales['position']=='Manager') {
          $get_client = $this->db->query("SELECT u_id FROM user");
          $u_id       = $get_client->result_array();
        }

        $id_brand = array();
        foreach ($u_id as $key => $v) {
            $id2        = $v['u_id'];
            $brand      = $this->db->query("SELECT DISTINCT id_brand FROM campaign WHERE u_id = $id2");
            $brand_id   = $brand->result_array();

            foreach ($brand_id as $key => $v) {
                array_push($id_brand, array(
                                            $v['id_brand']
                                            )
                );
            }
        }


        return $id_brand;
    }

    function name_brand_user($get_id){
        $brand = array();
        foreach ($get_id as $key => $value) {
            foreach ($value as $key => $v) {
                $name_brand = $this->db->query("SELECT * FROM `brand` WHERE id_brand = $v");
                $var        = $name_brand->result_array();
                $var2       = call_user_func_array('array_merge', $var);
                $count      = count($var2);

                array_push($brand, array(
                                            $var2
                                        )
                );
            }
        }
        $brand_user = array();
        foreach ($brand as $key => $value) {
            $id_brand   = $value[0]['id_brand'];
            $filter     = $this->db->query("SELECT id_brand FROM tbl_dashboard WHERE id_brand = $id_brand");
            if ($filter->num_rows() > 0) {
                array_push($brand_user, array(
                                                'id_brand'       => $value[0]['id_brand'],
                                                'brand_name'     => $value[0]['brand_name']
                                              )
                );
            }
        }
        return $brand_user;
    }

// FOR PUBLISHER
    function get_uid($name){
        $get_uid    = $this->db->query("SELECT u_id FROM `user` WHERE role = 'publisher' AND nama = '$name'");
        $u_id       = array();

        if ($get_uid->num_rows() > 0) {
            foreach ($get_uid->result_array() as $key => $v) {
                array_push($u_id, $v['u_id']);
            }
        }
        return $u_id;
    }

    function get_campagin_data($u_id, $id_campaign){
        $campagin_dt    = $this->db->query("SELECT id_campaign, id_brand, id_platform FROM `campaign` WHERE `u_id` = $u_id[0] AND `id_campaign` = $id_campaign");
        $cmpgn_dt       = array();

        if ($campagin_dt->num_rows() > 0) {
            foreach ($campagin_dt->result_array() as $key => $v) {
                array_push($cmpgn_dt, array(
                                            'id_campaign' => $v['id_campaign'],
                                            'id_brand'    => $v['id_brand'],
                                            'id_platform' => $v['id_platform']
                                            )
                                        );
            }
        }
        // echo "<pre/>";print_r($cmpgn_dt); die();//DEBUG
        return $cmpgn_dt;
    }

    function get_dasboard_pub($id_brand, $id_campaign, $id_platform, $date_from, $date_to){
        // echo "<pre/>";print_r($id_brand.' '.$id_campaign.' '.$id_platform.' '.$date_from.' '.$date_to);die;//DEBUG
        $data_publisher = $this->db->query("SELECT *
            FROM `tbl_dashboard` WHERE `id_brand` = $id_brand AND `id_campaign` = $id_campaign AND `id_platform` = $id_platform
            AND date >= '$date_from' AND date <= '$date_to'
            ORDER BY `date` ASC
        ");
        $pub_data = $data_publisher->result_array();
        return $pub_data;
    }

    function data_table_pub($id_brand , $id_campaign, $id_platform, $date_from, $date_to){
        $data_dashboard2 = $this->db->query("SELECT *
            FROM `tbl_dashboard` WHERE `id_brand` = $id_brand AND `id_campaign` = $id_campaign AND `id_platform` = $id_platform
            AND date >= '$date_from' AND date <= '$date_to'
            ORDER BY `date` ASC
        ");

        return $data_dashboard2->result_array();
    }
}

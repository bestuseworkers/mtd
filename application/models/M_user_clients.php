<?php

class M_user_clients extends CI_Model {

    private $table = "user";
    
    function insert_user($user) {
        // echo '<pre/>'; print_r($user); die();//DEBUG
        if (empty($user)) {
            $valid   = "kosong";
            $message = "Form Tidak Boleh Kosong !";
        }
        else{
            $data_user = array(
                                    'u_id'      => '', 
                                    'id_agency' => $user['id_agency'], 
                                    'id_sales'  => $user['id_sales'], 
                                    'nama'      => $user['nama'],
                                    'u_name'    => $user['u_name'],
                                    'u_paswd'   => md5($user['u_paswd']),
                                    'active'    => $user['active'],
                                    'role'      => $user['role']
                                );


            $cek = $this->db->get_where('user', array('u_name' => $data_user['u_name']));

            if ($cek->num_rows() == 0) { /*Jika data tidak ditemukan*/
                $insert = $this->db->insert('user', $data_user); 
                $valid = "sukses";
                $message = "Input User / Clients ".$data_user['nama']." Sebagai ".$data_user['role']." Sukses !";
                
            }
            else{
                
                $valid = "gagal";
                $message = "Data Sudah Ada, Silahkan Cek List User / Clients";
                
            }
        }

        return  array(
                        'valid'     => $valid , 
                        'message'   => $message
                    );
        // echo "<pre/>"; print_r($valid.' '.$message); die();//debug
    }

    function table_agency(){
         $table_agency = $this->db->get('agency');
         return $table_agency->result_array();
    }

    function table_sales(){
        $table_sales = $this->db->get('sales');
        return $table_sales->result_array();
    }

    function get_position($id_sales){
        $table_sales = $this->db->query("SELECT * FROM sales WHERE id_sales = $id_sales");
        return $table_sales->result_array();
    }

    function name_sales(){
        $table_sales = $this->db->get('sales');
        return $table_sales->result_array();
    }

    function select_sgh(){
        $table_sales = $this->db->query("   SELECT  `id_sales`, 
                                                    `sales_name`
                                            FROM `sales`
                                            WHERE position = 'SGH' 
                                            ORDER BY `sales_name`
                                        ");

        return $table_sales->result_array();
    }

    function table_user(){
        $table_user = $this->db->query(" SELECT `u_id`,
                                            `id_agency`,
                                            `id_sales`,
                                            (SELECT `agency_name` FROM agency WHERE id_agency = user.id_agency) AS agency_name,
                                            (SELECT `sales_name` FROM sales WHERE id_sales = user.id_sales) AS sales_name,
                                            `nama`,
                                            `u_name`,
                                            `u_paswd`,
                                            `active`,
                                            `role`
                                    FROM user
                                    ORDER BY `role` DESC
                                ");
        
        return $table_user->result_array();
    }

    function delete_user($id){
        $this->db->where('u_id', $id);
        $this->db->delete('user');

        //for message 
        $valid = "dihapus";
        $message = "Data Sudah Dihapus";

        return array('valid' => $valid, 'message' => $message);

    }

    function edit_user($user){
        // echo '<pre/>'; print_r($user); die();//debug

        $this->db->where('u_id', $user['u_id']);
        $query = $this->db->update('user', $user);
        
        if ($query > 0) {
            $valid = "sukses_edit";
            $message = "Data Berhasil Di Edit";
        }
        else{
            $valid = "gagal_edit";
            $message = "Edit Gagal Karena ".$this->db->_error_message()."";   
        }

        return array('valid' => $valid, 'message' => $message);

    }

    function get_user($id_user, $usr_name){
        // echo '<pre/>'; print_r($id_user); die();//DEBUG
        $user = $this->db->query(" SELECT * FROM user WHERE u_id = $id_user AND u_name = '$usr_name'");
        
        return $user->result_array();
    }

}

<?php

class M_upload extends CI_Model {

    function name_campaign($id_campaign){
        // echo '<pre/>'; print_r($id_campaign); die();//DEBUG
        $name_campaign = $this->db->query(" SELECT  `name`
                                            FROM `campaign`
                                            WHERE id_campaign = $id_campaign
                                        ");
        return $name_campaign->result_array();
    }

    function brand_name($id_brand){
        $brand = $this->db->query(" SELECT  `brand_name`
                                            FROM `brand`
                                            WHERE id_brand = $id_brand
                                        ");
        return $brand->result_array();
    }

    function table_campaign(){
        $table_campaign = $this->db->get('campaign');

        return $table_campaign->result_array();
    }

    function insert_temp($arr, $highestRow, $upload){
      // MAKE SURE TMP_DASHBOARD TABLE EMPTY BEFORE INSERT
      $this->db->truncate('tmp_tbl_dashboard');

      $date_pc        = array();
      $date_sp        = array();
      $date_all       = array();
      
      for ($i = 2; $i < $highestRow + 1; $i++) {
          $data = array(
            // 'id_tmp_dash'   => '',
            'id_brand'      => $upload['id_brand']
            ,'id_campaign'  => $upload['id_campaign']
            ,'id_platform'  => $upload['id_platform']
            ,'date'         => date('Y-m-d', strtotime('1899-12-31+'.($arr[$i][0]-1).' days'))
            ,'device'       => isset($arr[$i][1]) ? $arr[$i][1] : 'NONE'
            ,'targeting'    => isset($arr[$i][2]) ? $arr[$i][2] : 'NONE'
            ,'cost'         => $arr[$i][3]
            ,'impression'   => $arr[$i][4]
            ,'clicks'       => $arr[$i][5]
            ,'ctr'          => $arr[$i][6]
            ,'engagement'   => $arr[$i][7]
            ,'eng_rate'     => $arr[$i][8]
            ,'page_likes'   => $arr[$i][9]
            ,'like_rates'   => $arr[$i][10]
            ,'followers'    => $arr[$i][11]
            ,'follow_rates' => $arr[$i][12]
            ,'views_video'  => $arr[$i][13]
            ,'view_rates'   => $arr[$i][14]
            ,'conversions' => $arr[$i][15]
            ,'cvr'          => $arr[$i][16]
            ,'cpc'          => $arr[$i][17]
            ,'cpm'          => $arr[$i][18]
            ,'cpe'          => $arr[$i][19]
            ,'cpl'          => $arr[$i][20]
            ,'cpf'          => $arr[$i][21]
            ,'cpv'          => $arr[$i][22]
            ,'cpa'          => $arr[$i][23]
            ,'install'      => $arr[$i][24]
            ,'install_rate' => $arr[$i][25]
            ,'cpi'          => $arr[$i][26]
            ,'ad_request'   => $arr[$i][27]
            ,'fill_rate'    => $arr[$i][28]
            ,'leads'        => $arr[$i][29]
            ,'cpleads'      => $arr[$i][30]
            ,'reach'        => $arr[$i][31]
            ,'cpreach'      => $arr[$i][32]
            ,'adRecall'     => $arr[$i][33]
            ,'session'      => $arr[$i][34]
            ,'view_6s'      => $arr[$i][35]
            ,'view_2s'      => $arr[$i][36]
            ,'viewRates_2_6_s'=> $arr[$i][37]
            ,'videoLikes'   => $arr[$i][38]
            ,'comment'      => $arr[$i][39]
            ,'shares'       => $arr[$i][40]
            ,'profileVisits'=> $arr[$i][41]
          );

          //STORE SEMUA DATA KE TEMPORARY TABLE
          //IF DATA NOT NULL
          if ($data['cost'] !== NULL) {
            //INSERT TO DATABASE
            $this->db->insert('tmp_tbl_dashboard', $data);
            array_push($date_all, $data['date']);//For ALL Date
          }

          if ($data['device'] == 'PC'){ //For PC
            array_push($date_pc, $data['date']);
          }
          elseif ($data['device'] == 'Smartphone') { //For Mobile
            array_push($date_sp, $data['date']);
          }
      }

      $id_brand    = $upload['id_brand'];
      $id_campaign = $upload['id_campaign'];
      $id_platform = $upload['id_platform'];

      // Checking banyaknya tergeting
      $targeting = $this->db->query(" SELECT DISTINCT `targeting` FROM `tmp_tbl_dashboard` WHERE `id_brand` = $id_brand AND `id_campaign` = $id_campaign AND `id_platform` = $id_platform");

      foreach ($targeting->result_array() as $key => $value) {
        $list_targeting = array($value['targeting']);
      }

      $tgl_pc     = array_unique($date_pc);
      $tgl_sp     = array_unique($date_sp);
      $tgl_all    = array_unique($date_all);

      //IF Platform Programmatic
      if ($upload['id_platform'] == 1)
      {
          // For Device ALL Targeting ALL
          foreach ($tgl_all as $key => $all) {
              $count_all  = array();

              $all_pc     = $this->db->query("SELECT * FROM `tmp_tbl_dashboard` WHERE `date` = '$all'");
              $all_data   = $all_pc->result_array();

              foreach ($all_data as $key => $value) {
                  $total_all      = array(
                                          'id_brand'      => $value['id_brand']
                                          ,'id_campaign'  => $value['id_campaign']
                                          ,'id_platform'  => $value['id_platform']
                                          ,'date'         => $value['date']
                                          ,'device'       => 'ALL'
                                          ,'targeting'    => 'ALL'
                                      );
              }

              $total_all_2    = array(0,0,0,0,0,0,0,0,0,0,0,0,0);

              foreach ($all_data as $key => $all) {
                array_push($count_all, array(
                                        $all['cost']       //0
                                      ,$all['impression'] //1
                                      ,$all['clicks']     //2
                                      ,$all['ctr']        //3
                                      ,$all['engagement'] //4
                                      ,$all['eng_rate']   //5
                                      ,$all['page_likes'] //6
                                      ,$all['like_rates'] //7
                                      ,$all['followers']  //8
                                      ,$all['follow_rates']//9
                                      ,$all['views_video']//10
                                      ,$all['view_rates'] //11
                                      ,$all['conversions']//12
                                  )
                      );
              }
              foreach ($count_all as $row) {
                  foreach ($row as $key => $value) {
                      $total_all_2[$key] += $value;
                  }
              }

              // FOR CTR (CLICK / IMPRESSION) %
              if ($total_all_2[1] != 0) {
                  $ctr = $total_all_2[2] / $total_all_2[1];
              }
              else{
                  $ctr = 0;
              }
              // FOR CVR (CONVERSIONS / CLICK) %
              if ($total_all_2[12] != 0) {
                  $cvr = $total_all_2[12] / $total_all_2[2] * 100;
              }
              else{
                  $cvr = 0;
              }
              // FOR CPC (COST / CLIK)
              if ($total_all_2[2] != 0) {
                  $cpc = $total_all_2[0] / $total_all_2[2];
              }
              else{
                  $cpc = 0;
              }
              // FOR CPM (COST / IMPRESSIONS) *1000
              if ($total_all_2[1] != 0) {
                  $cpm = $total_all_2[0] / $total_all_2[1] * 1000;
              }
              else{
                  $cpm = 0;
              }
              // FOR CPE (COST / ENGAGEMENTS)
              if ($total_all_2[4] != 0) {
                  $cpe = $total_all_2[0] / $total_all_2[4];
              }
              else{
                  $cpe = 0;
              }
              // FOR CPL (COST / LIKE)
              if ($total_all_2[6] != 0) {
                  $cpl = $total_all_2[0] / $total_all_2[6];
              }
              else{
                  $cpl = 0;
              }
              // FOR CPF (COST / FOLLOWERS)
              if ($total_all_2[8] != 0) {
                  $cpf = $total_all_2[0] / $total_all_2[8];
              }
              else{
                  $cpf = 0;
              }
              // FOR CPV (COST / VIEW)
              if ($total_all_2[10] != 0) {
                  $cpv = $total_all_2[0] / $total_all_2[10];
              }
              else{
                  $cpv = 0;
              }
              // FOR CPA (COST / ACTION)
              if ($total_all_2[12] != 0) {
                  $cpa = $total_all_2[0] / $total_all_2[12];
              }
              else{
                  $cpa = 0;
              }

              $final_all  =   array(
                                  'id_tmp_dash'   => ''
                                  ,'id_brand'     => $total_all['id_brand']
                                  ,'id_campaign'  => $total_all['id_campaign']
                                  ,'id_platform'  => $total_all['id_platform']
                                  ,'date'         => $total_all['date']
                                  ,'device'       => $total_all['device']
                                  ,'targeting'    => $total_all['targeting']
                                  ,'cost'         => $total_all_2[0]
                                  ,'impression'   => $total_all_2[1]
                                  ,'clicks'       => $total_all_2[2]
                                  ,'ctr'          => $ctr
                                  ,'engagement'   => $total_all_2[4]
                                  ,'eng_rate'     => $total_all_2[5]
                                  ,'page_likes'   => $total_all_2[6]
                                  ,'like_rates'   => $total_all_2[7]
                                  ,'followers'    => $total_all_2[8]
                                  ,'follow_rates' => $total_all_2[9]
                                  ,'views_video'  => $total_all_2[10]
                                  ,'view_rates'   => $total_all_2[11]
                                  ,'conversions' => $total_all_2[12]
                                  ,'cvr'          => $cvr
                                  ,'cpc'          => $cpc
                                  ,'cpm'          => $cpm
                                  ,'cpe'          => $cpe
                                  ,'cpl'          => $cpl
                                  ,'cpf'          => $cpf
                                  ,'cpv'          => $cpv
                                  ,'cpa'          => $cpa
                              );

              if ($final_all !== NULL) {//IF DATA NOT NULL
                $this->db->insert('tmp_tbl_dashboard', $final_all);//INSERT TO DATABASE
              }
          }
          // End Device ALL Targeting ALL

          foreach ($list_targeting as $key => $value) {
            // Kalo Targeting Lebih dari 1
            if ($value > 1) {
              // For Device PC Targeting ALL
              foreach ($tgl_pc as $key => $pc) {
                  $count_pc   = array();

                  $data_pc = $this->db->query("SELECT * FROM `tmp_tbl_dashboard` WHERE `date` = '$pc' AND `device` = 'PC'");
                  $pc_data = $data_pc->result_array();

                  foreach ($pc_data as $key => $value) {
                    $total_1 = array(
                      'id_brand'      => $value['id_brand']
                      ,'id_campaign'  => $value['id_campaign']
                      ,'id_platform'  => $value['id_platform']
                      ,'date'         => $value['date']
                      ,'device'       => $value['device']
                      ,'targeting'    => 'ALL'
                    );
                  }

                  $total_2 = array(0,0,0,0,0,0,0,0,0,0,0,0,0);

                  // Untuk Menjumlahkan Data
                  foreach ($pc_data as $key => $value) {
                    array_push($count_pc, array(
                      $value['cost']
                      ,$value['impression']
                      ,$value['clicks']
                      ,$value['ctr']
                      ,$value['engagement']
                      ,$value['eng_rate']
                      ,$value['page_likes']
                      ,$value['like_rates']
                      ,$value['followers']
                      ,$value['follow_rates']
                      ,$value['views_video']
                      ,$value['view_rates']
                      ,$value['conversions']
                    )
                  );
                }

                  foreach ($count_pc as $row) {
                    foreach ($row as $key => $value) {
                      $total_2[$key] += $value;
                    }
                  }

                  // FOR CTR (CLICK / IMPRESSION) %
                  if ($total_2[1] != 0) {
                    $ctr = $total_2[2] / $total_2[1];
                  }
                  else{
                    $ctr = 0;
                  }
                  // FOR CVR (CONVERSIONS / CLICK) %
                  if ($total_2[12] != 0) {
                    $cvr = $total_2[12] / $total_2[2] * 100;
                  }
                  else{
                    $cvr = 0;
                  }
                  // FOR CPC (COST / CLIK)
                  if ($total_2[2] != 0) {
                    $cpc = $total_2[0] / $total_2[2];
                  }
                  else{
                    $cpc = 0;
                  }
                  // FOR CPM (COST / IMPRESSIONS) *1000
                  if ($total_2[1] != 0) {
                    $cpm = $total_2[0] / $total_2[1] * 1000;
                  }
                  else{
                    $cpm = 0;
                  }
                  // FOR CPE (COST / ENGAGEMENTS)
                  if ($total_2[4] != 0) {
                    $cpe = $total_2[0] / $total_2[4];
                  }
                  else{
                    $cpe = 0;
                  }
                  // FOR CPL (COST / LIKE)
                  if ($total_2[6] != 0) {
                    $cpl = $total_2[0] / $total_2[6];
                  }
                  else{
                    $cpl = 0;
                  }
                  // FOR CPF (COST / FOLLOWERS)
                  if ($total_2[8] != 0) {
                    $cpf = $total_2[0] / $total_2[8];
                  }
                  else{
                    $cpf = 0;
                  }
                  // FOR CPV (COST / VIEW)
                  if ($total_2[10] != 0) {
                    $cpv = $total_2[0] / $total_2[10];
                  }
                  else{
                    $cpv = 0;
                  }
                  // FOR CPA (COST / ACTION)
                  if ($total_2[12] != 0) {
                    $cpa = $total_2[0] / $total_2[12];
                  }
                  else{
                    $cpa = 0;
                  }
                  // End Menjumlahkan Data

                  $final  =   array(
                  'id_tmp_dash'   => ''
                  ,'id_brand'     => $total_1['id_brand']
                  ,'id_campaign'  => $total_1['id_campaign']
                  ,'id_platform'  => $total_1['id_platform']
                  ,'date'         => $total_1['date']
                  ,'device'       => $total_1['device']
                  ,'targeting'    => $total_1['targeting']
                  ,'cost'         => $total_2[0]
                  ,'impression'   => $total_2[1]
                  ,'clicks'       => $total_2[2]
                  ,'ctr'          => $ctr
                  ,'engagement'   => $total_2[4]
                  ,'eng_rate'     => $total_2[5]
                  ,'page_likes'   => $total_2[6]
                  ,'like_rates'   => $total_2[7]
                  ,'followers'    => $total_2[8]
                  ,'follow_rates' => $total_2[9]
                  ,'views_video'  => $total_2[10]
                  ,'view_rates'   => $total_2[11]
                  ,'conversions' => $total_2[12]
                  ,'cvr'          => $cvr
                  ,'cpc'          => $cpc
                  ,'cpm'          => $cpm
                  ,'cpe'          => $cpe
                  ,'cpl'          => $cpl
                  ,'cpf'          => $cpf
                  ,'cpv'          => $cpv
                  ,'cpa'          => $cpa
                  );

                  if ($final !== NULL) {//IF DATA NOT NULL
                    $this->db->insert('tmp_tbl_dashboard', $final);//INSERT TO DATABASE
                  }
              }
              // End Device PC Targeting ALL

              // For Device Mobile Targeting ALL
              foreach ($tgl_sp as $key => $sp) {

                $count_sp   = array();

                $data_sp = $this->db->query("SELECT * FROM `tmp_tbl_dashboard` WHERE `date` = '$sp' AND `device` = 'Smartphone'");
                $sp_data = $data_sp->result_array();

                foreach ($sp_data as $key => $value) {
                  $total_sp = array(
                  'id_brand'      => $value['id_brand']
                  ,'id_campaign'  => $value['id_campaign']
                  ,'id_platform'  => $value['id_platform']
                  ,'date'         => $value['date']
                  ,'device'       => $value['device']
                  ,'targeting'    => 'ALL'
                  );
                }

                $total_sp_2 = array(0,0,0,0,0,0,0,0,0,0,0,0,0);

                // Untuk Menjumlahkan Data
                foreach ($sp_data as $key => $value) {
                  array_push($count_sp, array(
                  $value['cost']
                  ,$value['impression']
                  ,$value['clicks']
                  ,$value['ctr']
                  ,$value['engagement']
                  ,$value['eng_rate']
                  ,$value['page_likes']
                  ,$value['like_rates']
                  ,$value['followers']
                  ,$value['follow_rates']
                  ,$value['views_video']
                  ,$value['view_rates']
                  ,$value['conversions']
                  )
                  );
                }

                foreach ($count_sp as $row) {
                  foreach ($row as $key => $value) {
                    $total_sp_2[$key] += $value;
                  }
                }

                // FOR CTR (CLICK / IMPRESSION) %
                if ($total_sp_2[1] != 0) {
                  $ctr = $total_sp_2[2] / $total_sp_2[1];
                }
                else{
                  $ctr = 0;
                }
                // FOR CVR (CONVERSIONS / CLICK) %
                if ($total_sp_2[12] != 0) {
                  $cvr = $total_sp_2[12] / $total_sp_2[2] * 100;
                }
                else{
                  $cvr = 0;
                }
                // FOR CPC (COST / CLIK)
                if ($total_sp_2[2] != 0) {
                  $cpc = $total_sp_2[0] / $total_sp_2[2];
                }
                else{
                  $cpc = 0;
                }
                // FOR CPM (COST / IMPRESSIONS) *1000
                if ($total_sp_2[1] != 0) {
                  $cpm = $total_sp_2[0] / $total_sp_2[1] * 1000;
                }
                else{
                  $cpm = 0;
                }
                // FOR CPE (COST / ENGAGEMENTS)
                if ($total_sp_2[4] != 0) {
                  $cpe = $total_sp_2[0] / $total_sp_2[4];
                }
                else{
                  $cpe = 0;
                }
                // FOR CPL (COST / LIKE)
                if ($total_sp_2[6] != 0) {
                  $cpl = $total_sp_2[0] / $total_sp_2[6];
                }
                else{
                  $cpl = 0;
                }
                // FOR CPF (COST / FOLLOWERS)
                if ($total_sp_2[8] != 0) {
                  $cpf = $total_sp_2[0] / $total_sp_2[8];
                }
                else{
                  $cpf = 0;
                }
                // FOR CPV (COST / VIEW)
                if ($total_sp_2[10] != 0) {
                  $cpv = $total_sp_2[0] / $total_sp_2[10];
                }
                else{
                  $cpv = 0;
                }
                // FOR CPA (COST / ACTION)
                if ($total_sp_2[12] != 0) {
                  $cpa = $total_sp_2[0] / $total_sp_2[12];
                }
                else{
                  $cpa = 0;
                }
                // End Menjumlahkan Data

                $final_sp  =   array(
                'id_tmp_dash'   => ''
                ,'id_brand'     => $total_sp['id_brand']
                ,'id_campaign'  => $total_sp['id_campaign']
                ,'id_platform'  => $total_sp['id_platform']
                ,'date'         => $total_sp['date']
                ,'device'       => $total_sp['device']
                ,'targeting'    => $total_sp['targeting']
                ,'cost'         => $total_sp_2[0]
                ,'impression'   => $total_sp_2[1]
                ,'clicks'       => $total_sp_2[2]
                ,'ctr'          => $ctr
                ,'engagement'   => $total_sp_2[4]
                ,'eng_rate'     => $total_sp_2[5]
                ,'page_likes'   => $total_sp_2[6]
                ,'like_rates'   => $total_sp_2[7]
                ,'followers'    => $total_sp_2[8]
                ,'follow_rates' => $total_sp_2[9]
                ,'views_video'  => $total_sp_2[10]
                ,'view_rates'   => $total_sp_2[11]
                ,'conversions' => $total_sp_2[12]
                ,'cvr'          => $cvr
                ,'cpc'          => $cpc
                ,'cpm'          => $cpm
                ,'cpe'          => $cpe
                ,'cpl'          => $cpl
                ,'cpf'          => $cpf
                ,'cpv'          => $cpv
                ,'cpa'          => $cpa
                );

                if ($final_sp !== NULL) {//IF DATA NOT NULL
                  $this->db->insert('tmp_tbl_dashboard', $final_sp);//INSERT TO DATABASE

                }
              }
              // End Device Mobile Targeting ALL
            }
          }

      }//End IF Programmatic
      // die; //DEBUG
    }

    function id_campaign($id_campaign){
        $id_campaign = $this->db->query("SELECT id_campaign FROM `tmp_tbl_dashboard` WHERE id_campaign = $id_campaign");
        return $id_campaign->result_array();
    }

    function table_temp($id_campaign, $id_brand){
        $table_temp = $this->db->query("SELECT * FROM `tmp_tbl_dashboard` WHERE id_brand = $id_brand AND id_campaign = $id_campaign");
        return $table_temp->result_array();
    }

    function platform($id_platform){
        // echo '<pre/>'; print_r($id_platform); die();//DEBUG
        $platform = $this->db->query("SELECT platform FROM `platform` WHERE id_platform = $id_platform");
        return $platform->result_array();
    }

     function campaign(){
        // echo '<pre/>'; print_r($id_campaign); die();//DEBUG
        $campaign = $this->db->query("SELECT (SELECT name FROM campaign WHERE id_campaign = tmp_tbl_dashboard.id_campaign) AS campaign FROM `tmp_tbl_dashboard`");
        return $campaign->result_array();
    }

    function delete_temp($id){
        foreach ($id as $key => $v) {
          $this->db->where('id_tmp_dash', $v);
          $this->db->delete('tmp_tbl_dashboard');
        }
        //for message
        $valid = "dihapus";
        $message = "Data Not Upload !";

        return array('valid' => $valid, 'message' => $message);
    }

    function input_dashboard($id){
      foreach ($id as $key => $v) {
        $this->db->where('id_tmp_dash', $v);
        $table_dash = $this->db->get('tmp_tbl_dashboard');
        $temp_res = call_user_func_array('array_merge', $table_dash->result_array());
        
        $data = array(
          // 'id_dashboard'   => '',
          'id_brand'      => $temp_res['id_brand']
          ,'id_campaign'  => $temp_res['id_campaign']
          ,'id_platform'  => $temp_res['id_platform']
          ,'date'         => date('Y-m-d',strtotime($temp_res['date']))
          ,'device'       => $temp_res['device']
          ,'targeting'    => $temp_res['targeting']
          ,'cost'         => $temp_res['cost']
          ,'impression'   => $temp_res['impression']
          ,'clicks'       => $temp_res['clicks']
          ,'ctr'          => $temp_res['ctr']
          ,'engagement'   => $temp_res['engagement']
          ,'eng_rate'     => $temp_res['eng_rate']
          ,'page_likes'   => $temp_res['page_likes']
          ,'like_rates'   => $temp_res['like_rates']
          ,'followers'    => $temp_res['followers']
          ,'follow_rates' => $temp_res['follow_rates']
          ,'views_video'  => $temp_res['views_video']
          ,'view_rates'   => $temp_res['view_rates']
          ,'conversions'  => $temp_res['conversions']
          ,'cvr'          => $temp_res['cvr']
          ,'cpc'          => $temp_res['cpc']
          ,'cpm'          => $temp_res['cpm']
          ,'cpe'          => $temp_res['cpe']
          ,'cpl'          => $temp_res['cpl']
          ,'cpf'          => $temp_res['cpf']
          ,'cpv'          => $temp_res['cpv']
          ,'cpa'          => $temp_res['cpa']
          ,'install'      => $temp_res['install']
          ,'install_rate' => $temp_res['install_rate']
          ,'cpi'          => $temp_res['cpi']
          ,'install'      => $temp_res['install']
          ,'ad_request'   => $temp_res['ad_request']
          ,'fill_rate'    => $temp_res['fill_rate']
          ,'leads'        => $temp_res['leads']
          ,'cpleads'      => $temp_res['cpleads']
          ,'reach'        => $temp_res['reach']
          ,'cpreach'      => $temp_res['cpreach']
          ,'adRecall'     => $temp_res['adRecall']
          ,'session'      => $temp_res['session']
          ,'view_6s'      => $temp_res['view_6s']
          ,'view_2s'      => $temp_res['view_2s']
          ,'viewRates_2_6_s'=> $temp_res['viewRates_2_6_s']
          ,'videoLikes'   => $temp_res['videoLikes']
          ,'comment'      => $temp_res['comment']
          ,'shares'       => $temp_res['shares']
          ,'profileVisits'=> $temp_res['profileVisits']
        );
        
        if (!empty($data)) {
          $date           = $data['date'];
          $id_campaign    = $data['id_campaign'];
          $id_brand       = $data['id_brand'];
          $id_platform    = $data['id_platform'];
          $device         = $data['device'];
          $targeting      = $data['targeting'];

          $aviable = $this->db->query("SELECT date FROM tbl_dashboard WHERE date = '$date' AND device = '$device' AND targeting = '$targeting' AND id_brand = $id_brand  AND id_campaign = $id_campaign  AND id_platform = $id_platform");//Cek data
          
          // $date_ava   = call_user_func_array('array_merge', $aviable->result_array());
          $date_ava = $aviable->result_array();
          
          if (!empty($date_ava)) {//Kalo Datanya Tidak Kosong
            $tgl = $date_ava[0]['date'];
            
            $this->db->query("DELETE FROM `tbl_dashboard` WHERE date = '$tgl' AND device = '$device' AND targeting = '$targeting' AND id_brand = $id_brand  AND id_campaign = $id_campaign  AND id_platform = $id_platform");//Delete Data
          }
          
          $this->db->insert('tbl_dashboard', $data);//INPUT DATA TO DASHBOARD DB
        }
      }

      //for message
      $valid = "sukses";
      $message = "Data Uploaded !";
      
      // MAKE SURE TMP_DASHBOARD TABLE EMPTY BEFORE INSERT
      $this->db->truncate('tmp_tbl_dashboard');

      return array('valid' => $valid, 'message' => $message);
    }

    function brand(){
        $brand = $this->db->get('brand');

        return $brand->result_array();
    }

    function get_campagin($id_brand){
        // print_r($id_brand); die();
        $campaign = $this->db->query("SELECT DISTINCT name, id_campaign FROM `campaign` WHERE id_brand = $id_brand");

        return $campaign->result_array();
    }

    function cek_upload($id_brand, $id_campaign, $id_platform){
        // print_r($id_brand); die();
        $this->db->where("id_brand", $id_brand);
        $this->db->where("id_campaign", $id_campaign);
        $this->db->where("id_platform", $id_platform);
        return $this->db->get("campaign");
    }

}

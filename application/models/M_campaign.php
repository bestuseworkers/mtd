<?php

class M_campaign extends CI_Model {
    
    function insert_campaign($campaign) {
        if (empty($campaign)) {
            $valid   = "kosong";
            $message = "Form Tidak Boleh Kosong !";
        }
        else{
            $campaign = array(
                'id_campaign'   => '', 
                'id_brand'      => $campaign['id_brand'], 
                'name'          => $campaign['name'], 
                'id_agency'     => $campaign['id_agency'], 
                'id_platform'   => $campaign['id_platform'],
                'budget'        => $campaign['budget'],
                'u_id'          => $campaign['client']
            );
        // echo '<pre/>'; print_r($campaign); die();//DEBUG

            $cek = $this->db->get_where('campaign', array('name' => $campaign['name'], 'id_platform' => $campaign['id_platform']));
            if ($cek->num_rows() == 0) { /*Jika data tidak ditemukan*/
                $insert = $this->db->insert('campaign', $campaign); 
                $valid = "sukses";
                $message = "Input Campaign ".$campaign['name']." Success !"; 
            }
            else{
                $valid = "gagal";
                $message = "Data Has Availabe, Please Check Campaign List";
            }
        }

        return  array(
            'valid'     => $valid , 
            'message'   => $message
        );
    }

    function table_platform(){
         $table_platform = $this->db->get('platform');
         return $table_platform->result_array();
    }

    function table_client(){
        $table_client = $this->db->query(" SELECT `u_id`,`nama`
            FROM user
            WHERE `role` <> 'superadmin' AND `role` <> 'user'
            ORDER BY `role` DESC
        ");

        return $table_client->result_array();
    }

    function table_campaign(){
        $table_campaign = $this->db->query(" SELECT  
            `id_campaign`, `id_brand`, (SELECT `brand_name` FROM brand WHERE id_brand = campaign.id_brand) AS brand_name, `name`, `id_agency`, (SELECT `agency_name` FROM agency WHERE id_agency = campaign.id_agency) AS agency_name, `id_platform`,(SELECT `platform` FROM platform WHERE id_platform = campaign.id_platform) AS platform, `budget`, `u_id`, (SELECT `nama` FROM user WHERE u_id = campaign.u_id) AS client FROM `campaign` 
            ORDER BY name ASC
        ");
        
        return $table_campaign->result_array();
    }

    function delete_campaign($id){
        $this->db->where('id_campaign', $id);
        $this->db->delete('campaign');

        //for message 
        $valid = "dihapus";
        $message = "Data Has Deleted";

        return array('valid' => $valid, 'message' => $message);
    }

    function edit_campaign($data){
        // echo '<pre/>'; print_r($data); die();//debug

        $this->db->where('id_campaign', $data['id_campaign']);
        $query = $this->db->update('campaign', $data);
        
        if ($query > 0) {
            $valid = "sukses_edit";
            $message = "Edit Successful";
        }
        else{
            $valid = "gagal_edit";
            $message = "Edit Failed, ".$this->db->_error_message()."";   
        }

        return array('valid' => $valid, 'message' => $message);

    }

    function insert_brand($brand){

        if (empty($brand)) {
            $valid   = "kosong";
            $message = "Form Not Allowed Empty !";
        }
        else{            
        // echo '<pre/>'; print_r($brand); die();//DEBUG

            $cek = $this->db->get_where('brand', array('brand_name' => $brand));

            if ($cek->num_rows() == 0) { /*Jika data tidak ditemukan*/
                $insert = $this->db->insert('brand', array('brand_name' => $brand)); 
                $valid = "sukses";
                $message = "Input Brand ".$brand." Success !";
                
            }
            else{
                
                $valid = "gagal";
                $message = "Data Has Availabe, Please Check Campaign List";
                
            }
        }

        return  array(
                        'valid'     => $valid , 
                        'message'   => $message
                    );
    }

    function table_brand(){
        $table_brand = $this->db->get('brand');
        return $table_brand->result_array();
    }

    function edit_brand($brand){
        // echo "<pre/>"; print_r($brand); die();//debug
        $this->db->where('id_brand', $brand['id_brand']);
        $query = $this->db->update('brand', $brand);
        
        if ($query > 0) {
            $valid = "sukses_edit";
            $message = "Edit Successful";
        }
        else{
            $valid = "gagal_edit";
            $message = "Edit Failed, ".$this->db->_error_message()."";   
        }

        return array('valid' => $valid, 'message' => $message);
    }

    function delete_brand($id_brand){
        $this->db->where('id_brand', $id_brand);
        $this->db->delete('brand');

        //for message 
        $valid = "dihapus";
        $message = "Data Has Deleted";

        return array('valid' => $valid, 'message' => $message);
    }

}
